<?php $pageTitle = 'Каталог видео';

require 'header.php' ?>

    <div class="catalog-page file-page">
        <div class="content-wrap">
            <div class="page-title">
                <h1>Каталог видео</h1>
                <a href="#" class="round-btn back-btn">
                    <?= SVG__ARROW_RIGHT ?>
                </a>
            </div>
            <div class="items-list media-list">
                <div class="media-item video">
                    <a href="#" class="img-wrap">
                        <iframe width="560" height="315" src="https://www.youtube.com/embed/zcTMOxIsp7U" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                    </a>
                    <div class="desc-wrap">
                        <h3  class="title">Боровичский район</h3>
                        <div class="desc">
                            <p>Ролик создан в рамках проекта «Услышать голос леса» (2020-2021), на средства Фонда Президентских грантов. Получатель гранта: АНО «Муравейник», Великий Новгород. </p>
                            <p>Дата съемки: 17.01.2022.</p>
                        </div>
                        <div class="btns-wrap">
                            <div class="round-btn age-limit">360°</div>
                            <div class="round-btn age-limit">6+</div>
                            <a href="#" class="round-btn fav-btn">
                                <svg width="22" height="20" viewBox="0 0 22 20" fill="none" xmlns="http://www.w3.org/2000/svg"><path opacity="0.7" d="M10.8053 2.64396L10.806 2.64462C10.8783 2.70853 10.9482 2.7738 11.015 2.84047L11.37 3.19509L11.7233 2.83882C11.7882 2.77345 11.8557 2.71052 11.9257 2.65018L11.9258 2.65022L11.9316 2.64507C14.6506 0.227833 19.2437 1.29637 20.8196 4.46782C21.5911 6.02052 21.6616 8.13764 20.2931 10.6827C18.9424 13.1946 16.193 16.1096 11.3683 19.2281C6.54363 16.1099 3.79415 13.1952 2.4434 10.6836C1.07482 8.13872 1.14516 6.02176 1.91662 4.46907C3.49237 1.2976 8.08564 0.228398 10.8053 2.64396Z" stroke="#181D24"></path></svg>                            </a>
                        </div>
                    </div>
                </div>
                <div class="media-item video">
                    <a href="#" class="img-wrap">
                        <iframe width="560" height="315" src="https://www.youtube.com/embed/V_RXYRwdfUU" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                    </a>
                    <div class="desc-wrap">
                        <a href="#" class="title">Смешные кошки</a>
                        <div class="desc">
                            <p>Смешные, пушистые, добрые котики!</p>
                            <p>Дата съемки: 13.07.2011.</p>
                        </div>
                        <div class="btns-wrap">
                            <div class="round-btn age-limit">6+</div>
                        </div>
                    </div>
                </div>
                <div class="media-item video">
                    <a href="#" class="img-wrap">
                        <iframe width="560" height="315" src="https://www.youtube.com/embed/S5XXsRuMPIU" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                    </a>
                    <div class="desc-wrap">
                        <a href="#" class="title">Sky Driving</a>
                        <div class="desc">
                            <p>This is the first 360 degree SkyDive video in Russia. VR видео, которое позволяет испытать непередаваемые ощущения от прыжка с парашютом.</p>
                            <p >Дата съемки: 13.06.2016.</p>
                        </div>
                        <div class="btns-wrap">
                            <div class="round-btn age-limit">360°</div>
                            <div class="round-btn age-limit">16+</div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="pagination">
                <a href="#" class="arrow prev">
                    <?= SVG__ARROW_RIGHT ?>
                </a>
                <a href="#" class="page">1</a>
                <a href="#" class="page current">2</a>
                <a href="#" class="page">3</a>
                <a href="#" class="page">4</a>
                <a href="#" class="page">5</a>
                <div class="dots">...</div>
                <a href="#" class="page">9</a>
                <a href="#" class="arrow next">
                    <?= SVG__ARROW_RIGHT ?>
                </a>
            </div>
        </div>
    </div>

<?php require 'footer.php' ?>