<?php $pageTitle = 'Главная страница теста';

require 'header.php' ?>

<div class="test-page">
    <div class="content-wrap">
        <a href="#" class="round-btn back-btn">
            <?= SVG__ARROW_RIGHT ?>
        </a>
        <div class="start-screen">
            <div class="page-title">
                <p>Рюрик:</p>
                <p>правда или вымысел</p>
            </div>
            <div class="desc">
                История легендарного правителя Руси обросла мифами, легендами и догадками. Сможешь ли ты отличить их от реальных фактов? История легендарного правителя Руси обросла мифами, легендами и догадками. Сможешь ли ты отличить их от реальных фактов?
            </div>
            <div class="btns-wrap">
                <a href="#" class="btn alt start">Пройти</a>
                <a href="#" class="btn">Следующий тест</a>
            </div>
        </div>
        <div class="test-screen">
            <div class="page-title">Тесты</div>
            <div class="question-wrap">
                <div class="count">
                    <span class="current">1</span>
                    / 3
                </div>
                <div class="outer">
                    <div class="inner current" style="display: block;">
                        <div class="q">
                            Призвать на княжение варягов, в том числе и Рюрика, славянские и прибалтийско-финские племена решили из-за междоусобиц.
                        </div>
                        <label class="a">
                            <input type="radio" name="q1" value="1" checked>
                            <span class="radio"></span>
                            <span class="text">
                                Рельеф-панно «1242 г. Разгром Александром Невским немецких рыцарей на Чудском
                                озере» ответ 1 - пример (2-3 предложения)
                            </span>
                        </label>
                        <label class="a">
                            <input type="radio" name="q1" value="2">
                            <span class="radio"></span>
                            <span class="text">
                                Ответ 2 ( 2-3 предложения)
                            </span>
                        </label>
                    </div>
                    <div class="inner">
                        <div class="q">
                            Вопрос 2
                        </div>
                        <label class="a">
                            <input type="radio" name="q2" value="1" checked>
                            <span class="radio"></span>
                            <span class="text">
                                Вариант 1
                            </span>
                        </label>
                        <label class="a">
                            <input type="radio" name="q2" value="2">
                            <span class="radio"></span>
                            <span class="text">
                                Вариант 2
                            </span>
                        </label>
                    </div>
                    <div class="inner">
                        <div class="q">
                            Вопрос 3<br>
                            Строка<br>
                            И еще строка
                        </div>
                        <label class="a">
                            <input type="radio" name="q3" value="1" checked>
                            <span class="radio"></span>
                            <span class="text">
                                Вариант 1
                            </span>
                        </label>
                        <label class="a">
                            <input type="radio" name="q3" value="2">
                            <span class="radio"></span>
                            <span class="text">
                                Вариант 2
                            </span>
                        </label>
                    </div>
                    <div class="result">
                        <div class="text">Ваш результат:</div>
                        <div class="amount">8 / 10 верных ответов</div>
                        <div class="explain">Текстовое пояснение. С учётом сложившейся международной обстановки, выбранный нами инновационный путь создаёт предпосылки для стандартных подходов!</div>
                    </div>
                </div>
                <div class="msg">
                    Верно! Учитывая ключевые сценарии поведения, выбранный нами инновационный путь однозначно фиксирует необходимость переосмысления внешнеэкономических политик. В частности, постоянное информационно-пропагандистское обеспечение.
                </div>
                <div class="btns-wrap">
                    <a href="#" class="btn alt answer">Ответить</a>
                    <a href="#" class="btn skip">Пропустить</a>
                </div>
                <div class="btns-wrap">
                    <a href="#" class="btn next">Следующий вопрос</a>
                </div>
                <div class="btns-wrap">
                    <a href="#" class="btn alt">Другие тесты</a>
                    <a href="#" class="btn w244">Поделиться результатом</a>
                </div>
            </div>
        </div>
    </div>
</div>

<?php require 'footer.php' ?>