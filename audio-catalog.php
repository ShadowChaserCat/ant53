<?php $pageTitle = 'Каталог аудио';

require 'header.php' ?>

<div class="catalog-page">
    <div class="content-wrap">
        <div class="page-title">
            <p>Каталог аудио</p>
            <a href="#" class="round-btn back-btn">
                <?= SVG__ARROW_RIGHT ?>
            </a>
        </div>
        <div class="content-fetch">
            <ul class="letters-list">
                <li class="letter">а</li>
                <li class="letter">б</li>
                <li class="letter">в</li>
                <li class="letter">г</li>
                <li class="letter">д</li>
                <li class="letter">е</li>
                <li class="letter">ё</li>
                <li class="letter">ж</li>
                <li class="letter">з</li>
                <li class="letter">и</li>
                <li class="letter">й</li>
                <li class="letter">к</li>
                <li class="letter">л</li>
                <li class="letter">м</li>
                <li class="letter">н</li>
                <li class="letter">о</li>
                <li class="letter">п</li>
                <li class="letter">р</li>
                <li class="letter">с</li>
                <li class="letter">т</li>
                <li class="letter">у</li>
                <li class="letter">ф</li>
                <li class="letter">х</li>
                <li class="letter">ц</li>
                <li class="letter">ч</li>
                <li class="letter">ш</li>
                <li class="letter">щ</li>
                <li class="letter">ы</li>
                <li class="letter">э</li>
                <li class="letter">ю</li>
                <li class="letter">я</li>
            </ul>
            <div class="regs-area">
                <div class="swiper">
                    <div class="swiper-wrapper">
                        <ul class="swiper-slide">
                            <li class="swiper-slide__list">Великий Новгород</li>
                            <li class="swiper-slide__list">Батецкий район</li>
                            <li class="swiper-slide__list">Боровичский район </li>
                            <li class="swiper-slide__list">Псковская</li>
                            <li class="swiper-slide__list">Щусева</li>
                            <li class="swiper-slide__list">Батецкий район</li>
                            <li class="swiper-slide__list">Великий Новгород</li>
                            <li class="swiper-slide__list">Батецкий район</li>
                            <li class="swiper-slide__list">Боровичский район </li>
                            <li class="swiper-slide__list">Псковская</li>
                            <li class="swiper-slide__list">Щусева</li>
                            <li class="swiper-slide__list">Батецкий район</li>
                        </ul>
                        <ul class="swiper-slide">
                            <li class="swiper-slide__list">Великий Новгород</li>
                            <li class="swiper-slide__list">Батецкий район</li>
                            <li class="swiper-slide__list">Боровичский район </li>
                            <li class="swiper-slide__list">Псковская</li>
                            <li class="swiper-slide__list">Щусева</li>
                            <li class="swiper-slide__list">Батецкий район</li>
                            <li class="swiper-slide__list">Великий Новгород</li>
                            <li class="swiper-slide__list">Батецкий район</li>
                            <li class="swiper-slide__list">Боровичский район </li>
                            <li class="swiper-slide__list">Псковская</li>
                            <li class="swiper-slide__list">Щусева</li>
                            <li class="swiper-slide__list">Батецкий район</li>
                        </ul>
                        <ul class="swiper-slide">
                            <li class="swiper-slide__list">Великий Новгород</li>
                            <li class="swiper-slide__list">Батецкий район</li>
                            <li class="swiper-slide__list">Боровичский район </li>
                            <li class="swiper-slide__list">Псковская</li>
                            <li class="swiper-slide__list">Щусева</li>
                            <li class="swiper-slide__list">Батецкий район</li>
                            <li class="swiper-slide__list">Великий Новгород</li>
                            <li class="swiper-slide__list">Батецкий район</li>
                            <li class="swiper-slide__list">Боровичский район </li>
                            <li class="swiper-slide__list">Псковская</li>
                            <li class="swiper-slide__list">Щусева</li>
                            <li class="swiper-slide__list">Батецкий район</li>
                        </ul>
                    </div>
                </div>
                <div class="slider-btns">
                    <a href="#" class="slider-btn reg-btn-prev">
                        <?= SVG__ARROW_RIGHT ?>
                    </a>
                    <a href="#" class="slider-btn reg-btn-next">
                        <?= SVG__ARROW_RIGHT ?>
                    </a>
                    <div class="reg-swiper swiper-scrollbar"></div>
                </div>
            </div>
        </div>
        <div class="items-list media-list">
            <div class="media-item">
                <div class="img-audio">
                    <div class="img-container">
                        <img src="img/svg/headphones.svg" alt>
                    </div>
                </div>
                <div class="desc-wrap">
                    <a href="#" class="title">Куличный луг</a>
                    <div class="desc">
                        Памятник природы регионального значения. Расположен в Старорусском районе Новгородской
                        области. Фото предоставлено краеведом г. Старая Русса Павлом Володиным, через социальную
                        сеть VK.<br>
                        Дата съемки: 13.07.2011.
                    </div>
                    <div class="player-wrap">
                        <div class="time-wrap">
                            <div class="current">00:00</div>
                            <div class="total">04:30</div>
                        </div>
                        <div class="progress-bar"></div>
                    </div>
                    <div class="btns-wrap">
                        <a href="#" class="btn alt">Слушать</a>
                        <a href="#" class="round-btn fav-btn">
                            <?= SVG__HEART ?>
                        </a>
                        <div class="round-btn age-limit">12+</div>
                    </div>
                </div>
            </div>
            <div class="media-item">
                <div class="img-audio">
                    <div class="img-container">
                        <img src="img/svg/headphones.svg" alt>
                    </div>
                </div>
                <div class="desc-wrap">
                    <a href="#" class="title">Куличный луг</a>
                    <div class="desc">
                        Памятник природы регионального значения. Расположен в Старорусском районе Новгородской
                        области. Фото предоставлено краеведом г. Старая Русса Павлом Володиным, через социальную
                        сеть VK.<br>
                        Дата съемки: 13.07.2011.
                    </div>
                    <div class="player-wrap">
                        <div class="time-wrap">
                            <div class="current">00:00</div>
                            <div class="total">04:30</div>
                        </div>
                        <div class="progress-bar"></div>
                    </div>
                    <div class="btns-wrap">
                        <a href="#" class="btn alt">Слушать</a>
                        <a href="#" class="round-btn fav-btn">
                            <?= SVG__HEART ?>
                        </a>
                        <div class="round-btn age-limit">12+</div>
                    </div>
                </div>
            </div>
            <div class="media-item">
                <div class="img-audio">
                    <div class="img-container">
                        <img src="img/svg/headphones.svg" alt>
                    </div>
                </div>
                <div class="desc-wrap">
                    <a href="#" class="title">Куличный луг</a>
                    <div class="desc">
                        Памятник природы регионального значения. Расположен в Старорусском районе Новгородской
                        области. Фото предоставлено краеведом г. Старая Русса Павлом Володиным, через социальную
                        сеть VK.<br>
                        Дата съемки: 13.07.2011.
                    </div>
                    <div class="player-wrap">
                        <div class="time-wrap">
                            <div class="current">00:00</div>
                            <div class="total">04:30</div>
                        </div>
                        <div class="progress-bar"></div>
                    </div>
                    <div class="btns-wrap">
                        <a href="#" class="btn alt">Слушать</a>
                        <a href="#" class="round-btn fav-btn">
                            <?= SVG__HEART ?>
                        </a>
                        <div class="round-btn age-limit">12+</div>
                    </div>
                </div>
            </div>
            <div class="media-item">
                <div class="img-audio">
                    <div class="img-container">
                        <img src="img/svg/headphones.svg" alt>
                    </div>
                </div>
                <div class="desc-wrap">
                    <a href="#" class="title">Куличный луг</a>
                    <div class="desc">
                        Памятник природы регионального значения. Расположен в Старорусском районе Новгородской
                        области. Фото предоставлено краеведом г. Старая Русса Павлом Володиным, через социальную
                        сеть VK.<br>
                        Дата съемки: 13.07.2011.
                    </div>
                    <div class="player-wrap">
                        <div class="time-wrap">
                            <div class="current">00:00</div>
                            <div class="total">04:30</div>
                        </div>
                        <div class="progress-bar"></div>
                    </div>
                    <div class="btns-wrap">
                        <a href="#" class="btn alt">Слушать</a>
                        <a href="#" class="round-btn fav-btn">
                            <?= SVG__HEART ?>
                        </a>
                        <div class="round-btn age-limit">12+</div>
                    </div>
                </div>
            </div>
            <div class="media-item">
                <div class="img-audio">
                    <div class="img-container">
                        <img src="img/svg/headphones.svg" alt>
                    </div>
                </div>
                <div class="desc-wrap">
                    <a href="#" class="title">Куличный луг</a>
                    <div class="desc">
                        Памятник природы регионального значения. Расположен в Старорусском районе Новгородской
                        области. Фото предоставлено краеведом г. Старая Русса Павлом Володиным, через социальную
                        сеть VK.<br>
                        Дата съемки: 13.07.2011.
                    </div>
                    <div class="player-wrap">
                        <div class="time-wrap">
                            <div class="current">00:00</div>
                            <div class="total">04:30</div>
                        </div>
                        <div class="progress-bar"></div>
                    </div>
                    <div class="btns-wrap">
                        <a href="#" class="btn alt">Слушать</a>
                        <a href="#" class="round-btn fav-btn">
                            <?= SVG__HEART ?>
                        </a>
                        <div class="round-btn age-limit">12+</div>
                    </div>
                </div>
            </div>
            <div class="media-item">
                <div class="img-audio">
                    <div class="img-container">
                        <img src="img/svg/headphones.svg" alt>
                    </div>
                </div>
                <div class="desc-wrap">
                    <a href="#" class="title">Куличный луг</a>
                    <div class="desc">
                        Памятник природы регионального значения. Расположен в Старорусском районе Новгородской
                        области. Фото предоставлено краеведом г. Старая Русса Павлом Володиным, через социальную
                        сеть VK.<br>
                        Дата съемки: 13.07.2011.
                    </div>
                    <div class="player-wrap">
                        <div class="time-wrap">
                            <div class="current">00:00</div>
                            <div class="total">04:30</div>
                        </div>
                        <div class="progress-bar"></div>
                    </div>
                    <div class="btns-wrap">
                        <a href="#" class="btn alt">Слушать</a>
                        <a href="#" class="round-btn fav-btn">
                            <?= SVG__HEART ?>
                        </a>
                        <div class="round-btn age-limit">12+</div>
                    </div>
                </div>
            </div>
        </div>
        <div class="pagination">
            <a href="#" class="arrow prev">
                <?= SVG__ARROW_RIGHT ?>
            </a>
            <a href="#" class="page">1</a>
            <a href="#" class="page current">2</a>
            <a href="#" class="page">3</a>
            <a href="#" class="page">4</a>
            <a href="#" class="page">5</a>
            <div class="dots">...</div>
            <a href="#" class="page">9</a>
            <a href="#" class="arrow next">
                <?= SVG__ARROW_RIGHT ?>
            </a>
        </div>
    </div>
</div>

<?php require 'footer.php' ?>