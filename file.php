<?php $pageTitle = 'Блок с файлами';

require 'header.php' ?>

    <div class=" catalog-page articles-page">
        <div class="content-wrap">
            <div class="page-title">
                <a href="#" class="round-btn back-btn">
                    <svg width="30" height="15" viewBox="0 0 30 15" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path fill-rule="evenodd" clip-rule="evenodd"
                              d="M29.7505 7.92063L-8.74228e-08 7.92063L0 6.9635L29.7505 6.9635L29.7505 7.92063Z"
                              fill="#181D24"></path>
                        <path fill-rule="evenodd" clip-rule="evenodd"
                              d="M21.812 0C21.9976 1.91877 22.9406 3.70301 24.4557 5.00226C25.9707 6.30152 27.9483 7.02189 30.0001 7.02189V7.97902C27.694 7.97902 25.4712 7.16934 23.7683 5.70902C22.0655 4.24869 21.0056 2.24326 20.7969 0.0866218L21.812 0Z"
                              fill="#181D24"></path>
                        <path fill-rule="evenodd" clip-rule="evenodd"
                              d="M21.812 15C21.9976 13.0812 22.9406 11.297 24.4557 9.99774C25.9707 8.69848 27.9483 7.97811 30.0001 7.97811V7.02098C27.694 7.02098 25.4712 7.83066 23.7683 9.29098C22.0655 10.7513 21.0056 12.7567 20.7969 14.9134L21.812 15Z"
                              fill="#181D24"></path>
                    </svg>
                </a>
                <h1>Библиографические материалы</h1>
            </div>
            <div class="items-grid">
                <a href="#" class="item-link">
                    <div class="item-link__img-container">
                        <img src="img/placeholder.png" alt>
                    </div>
                    <h2 class="title">Новгород</h2>
                    <p class="desc">Обзор книги: Кушнир И.И. Новгород. – Л.: Стройиздат, 1967.</p>
                </a>
                <a href="#" class="item-link">
                    <div class="item-link__img-container">
                        <img src="img/placeholder.png" alt>
                    </div>
                    <h2 class="title">Новгород</h2>
                    <p class="desc">Обзор книги: Кушнир И.И. Новгород. – Л.: Стройиздат, 1967.</p>
                </a>
                <a href="#" class="item-link">
                    <div class="item-link__img-container">
                        <img src="img/placeholder.png" alt>
                    </div>
                    <h2 class="title">Новгород</h2>
                    <p class="desc">Обзор книги: Кушнир И.И. Новгород. – Л.: Стройиздат, 1967.</p>
                </a>
                <a href="#" class="item-link">
                    <div class="item-link__img-container">
                        <img src="img/placeholder.png" alt>
                    </div>
                    <h2 class="title">Новгород</h2>
                    <p class="desc">Обзор книги: Кушнир И.И. Новгород. – Л.: Стройиздат, 1967.</p>
                </a>
                <a href="#" class="item-link">
                    <div class="item-link__img-container">
                        <img src="img/placeholder.png" alt>
                    </div>
                    <h2 class="title">Новгород</h2>
                    <p class="desc">Обзор книги: Кушнир И.И. Новгород. – Л.: Стройиздат, 1967.</p>
                </a>
                <a href="#" class="item-link">
                    <div class="item-link__img-container">
                        <img src="img/placeholder.png" alt>
                    </div>
                    <h2 class="title">Новгород</h2>
                    <p class="desc">Обзор книги: Кушнир И.И. Новгород. – Л.: Стройиздат, 1967.</p>
                </a>
                <a href="#" class="item-link">
                    <div class="item-link__img-container">
                        <img src="img/placeholder.png" alt>
                    </div>
                    <h2 class="title">Новгород</h2>
                    <p class="desc">Обзор книги: Кушнир И.И. Новгород. – Л.: Стройиздат, 1967.</p>
                </a>
                <a href="#" class="item-link">
                    <div class="item-link__img-container">
                        <img src="img/placeholder.png" alt>
                    </div>
                    <h2 class="title">Новгород</h2>
                    <p class="desc">Обзор книги: Кушнир И.И. Новгород. – Л.: Стройиздат, 1967.</p>
                </a>

            </div>
            <div class="pagination">
                <a href="#" class="arrow prev">
                    <?= SVG__ARROW_RIGHT ?>
                </a>
                <a href="#" class="page">1</a>
                <a href="#" class="page current">2</a>
                <a href="#" class="page">3</a>
                <a href="#" class="page">4</a>
                <a href="#" class="page">5</a>
                <div class="dots">...</div>
                <a href="#" class="page">9</a>
                <a href="#" class="arrow next">
                    <?= SVG__ARROW_RIGHT ?>
                </a>
            </div>
        </div>
    </div>

<?php require 'footer.php' ?>