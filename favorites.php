<?php $pageTitle = 'Избранное';

require 'header.php' ?>

<div class="favorites-page">
    <div class="content-wrap">
        <div class="page-title">
            Избранное
            <div class="count">3000</div>
        </div>
        <div class="list-filters-wrap">
            <div class="title">Фильтры:</div>
            <div class="list-wrap">
                <div class="list">
                    <a href="#" class="plus-btn">
                        <?= SVG__PLUS ?>
                    </a>
                </div>
                <div class="btns-wrap">
                    <a href="#" class="btn alt apply-btn">Применить</a>
                    <a href="#" class="btn gray reset-btn">Сбросить</a>
                </div>
            </div>
        </div>
        <div class="items-grid">
            <div class="item">
                <a href="#" class="img-wrap">
                    <img src="img/placeholder.png" alt>
                </a>
                <a href="#" class="title">Новгород</a>
                <div class="desc">Обзор книги: Кушнир И.И. Новгород. – Л.: Стройиздат, 1967.</div>
            </div>
            <div class="item">
                <a href="#" class="img-wrap">
                    <img src="img/placeholder.png" alt>
                </a>
                <a href="#" class="title">Новгород</a>
                <div class="desc">Обзор книги: Кушнир И.И. Новгород. – Л.: Стройиздат, 1967.</div>
            </div>
            <div class="item">
                <a href="#" class="img-wrap">
                    <img src="img/placeholder.png" alt>
                </a>
                <a href="#" class="title">Новгород</a>
                <div class="desc">Обзор книги: Кушнир И.И. Новгород. – Л.: Стройиздат, 1967.</div>
            </div>
            <div class="item">
                <a href="#" class="img-wrap">
                    <img src="img/placeholder.png" alt>
                </a>
                <a href="#" class="title">Новгород</a>
                <div class="desc">Обзор книги: Кушнир И.И. Новгород. – Л.: Стройиздат, 1967.</div>
            </div>
            <div class="item">
                <a href="#" class="img-wrap">
                    <img src="img/placeholder.png" alt>
                </a>
                <a href="#" class="title">Новгород</a>
                <div class="desc">Обзор книги: Кушнир И.И. Новгород. – Л.: Стройиздат, 1967.</div>
            </div>
            <div class="item">
                <a href="#" class="img-wrap">
                    <img src="img/placeholder.png" alt>
                </a>
                <a href="#" class="title">Новгород</a>
                <div class="desc">Обзор книги: Кушнир И.И. Новгород. – Л.: Стройиздат, 1967.</div>
            </div>
            <div class="item">
                <a href="#" class="img-wrap">
                    <img src="img/placeholder.png" alt>
                </a>
                <a href="#" class="title">Новгород</a>
                <div class="desc">Обзор книги: Кушнир И.И. Новгород. – Л.: Стройиздат, 1967.</div>
            </div>
            <div class="item">
                <a href="#" class="img-wrap">
                    <img src="img/placeholder.png" alt>
                </a>
                <a href="#" class="title">Новгород</a>
                <div class="desc">Обзор книги: Кушнир И.И. Новгород. – Л.: Стройиздат, 1967.</div>
            </div>
            <div class="item">
                <a href="#" class="img-wrap">
                    <img src="img/placeholder.png" alt>
                </a>
                <a href="#" class="title">Новгород</a>
                <div class="desc">Обзор книги: Кушнир И.И. Новгород. – Л.: Стройиздат, 1967.</div>
            </div>
            <div class="item">
                <a href="#" class="img-wrap">
                    <img src="img/placeholder.png" alt>
                </a>
                <a href="#" class="title">Новгород</a>
                <div class="desc">Обзор книги: Кушнир И.И. Новгород. – Л.: Стройиздат, 1967.</div>
            </div>
            <div class="item">
                <a href="#" class="img-wrap">
                    <img src="img/placeholder.png" alt>
                </a>
                <a href="#" class="title">Новгород</a>
                <div class="desc">Обзор книги: Кушнир И.И. Новгород. – Л.: Стройиздат, 1967.</div>
            </div>
            <div class="item">
                <a href="#" class="img-wrap">
                    <img src="img/placeholder.png" alt>
                </a>
                <a href="#" class="title">Новгород</a>
                <div class="desc">Обзор книги: Кушнир И.И. Новгород. – Л.: Стройиздат, 1967.</div>
            </div>
            <div class="item">
                <a href="#" class="img-wrap">
                    <img src="img/placeholder.png" alt>
                </a>
                <a href="#" class="title">Новгород</a>
                <div class="desc">Обзор книги: Кушнир И.И. Новгород. – Л.: Стройиздат, 1967.</div>
            </div>
            <div class="item">
                <a href="#" class="img-wrap">
                    <img src="img/placeholder.png" alt>
                </a>
                <a href="#" class="title">Новгород</a>
                <div class="desc">Обзор книги: Кушнир И.И. Новгород. – Л.: Стройиздат, 1967.</div>
            </div>
            <div class="item">
                <a href="#" class="img-wrap">
                    <img src="img/placeholder.png" alt>
                </a>
                <a href="#" class="title">Новгород</a>
                <div class="desc">Обзор книги: Кушнир И.И. Новгород. – Л.: Стройиздат, 1967.</div>
            </div>
            <div class="item">
                <a href="#" class="img-wrap">
                    <img src="img/placeholder.png" alt>
                </a>
                <a href="#" class="title">Новгород</a>
                <div class="desc">Обзор книги: Кушнир И.И. Новгород. – Л.: Стройиздат, 1967.</div>
            </div>
        </div>
        <div class="pagination">
            <a href="#" class="arrow prev">
                <?= SVG__ARROW_RIGHT ?>
            </a>
            <a href="#" class="page">1</a>
            <a href="#" class="page current">2</a>
            <a href="#" class="page">3</a>
            <a href="#" class="page">4</a>
            <a href="#" class="page">5</a>
            <div class="dots">...</div>
            <a href="#" class="page">9</a>
            <a href="#" class="arrow next">
                <?= SVG__ARROW_RIGHT ?>
            </a>
        </div>
    </div>
</div>

<?php require 'footer.php' ?>