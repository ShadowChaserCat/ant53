<?php $pageTitle = 'Тематические рубрики - подрубрики';

require 'header.php' ?>

<div class="category-page">
    <div class="content-wrap">
        <div class="page-title">
            <p>Тематические</p>
            <p>рубрики</p>
            <a href="#" class="round-btn back-btn">
                <?= SVG__ARROW_RIGHT ?>
            </a>
            <div class="round-btn age-limit">12+</div>
        </div>
    </div>
    <div class="img-block"></div>
    <div class="category-block">
        <div class="content-wrap">
            <div class="breadcrumbs">
                <div class="item"></div>
                <div class="item">Рубрика</div>
                <div class="separator">&nbsp;&nbsp;/&nbsp;&nbsp;</div>
                <div class="item">Подрубрика</div>
                <div class="separator">&nbsp;&nbsp;/&nbsp;&nbsp;</div>
                <div class="item">Подрубрика</div>
                <div class="separator">&nbsp;&nbsp;/&nbsp;&nbsp;</div>
                <a href="#" class="item">Подрубрика</a>
            </div>
            <div class="subcategories-wrap">
                <div class="title">Выберите рубрику:</div>
                <div class="list">
                    <a href="#" class="item">Съемки в Новгорое</a>
                    <a href="#" class="item">Съемки в области</a>
                    <a href="#" class="item">Актеры-новгородцы</a>
                    <a href="#" class="item">Фильмы о Новгороде</a>
                    <a href="#" class="item">Фильмы о новгородских землях</a>
                    <a href="#" class="item">Разное</a>
                </div>
            </div>
            <div class="items-grid">
                <div class="item">
                    <a href="#" class="img-wrap">
                        <img src="img/placeholder.png" alt>
                    </a>
                    <a href="#" class="title">Новгород</a>
                    <div class="desc">Обзор книги: Кушнир И.И. Новгород. – Л.: Стройиздат, 1967.</div>
                </div>
                <div class="item">
                    <a href="#" class="img-wrap">
                        <img src="img/placeholder.png" alt>
                    </a>
                    <a href="#" class="title">Новгород</a>
                    <div class="desc">Обзор книги: Кушнир И.И. Новгород. – Л.: Стройиздат, 1967.</div>
                </div>
                <div class="item">
                    <a href="#" class="img-wrap">
                        <img src="img/placeholder.png" alt>
                    </a>
                    <a href="#" class="title">Новгород</a>
                    <div class="desc">Обзор книги: Кушнир И.И. Новгород. – Л.: Стройиздат, 1967.</div>
                </div>
                <div class="item">
                    <a href="#" class="img-wrap">
                        <img src="img/placeholder.png" alt>
                    </a>
                    <a href="#" class="title">Новгород</a>
                    <div class="desc">Обзор книги: Кушнир И.И. Новгород. – Л.: Стройиздат, 1967.</div>
                </div>
                <div class="item">
                    <a href="#" class="img-wrap">
                        <img src="img/placeholder.png" alt>
                    </a>
                    <a href="#" class="title">Новгород</a>
                    <div class="desc">Обзор книги: Кушнир И.И. Новгород. – Л.: Стройиздат, 1967.</div>
                </div>
                <div class="item">
                    <a href="#" class="img-wrap">
                        <img src="img/placeholder.png" alt>
                    </a>
                    <a href="#" class="title">Новгород</a>
                    <div class="desc">Обзор книги: Кушнир И.И. Новгород. – Л.: Стройиздат, 1967.</div>
                </div>
                <div class="item">
                    <a href="#" class="img-wrap">
                        <img src="img/placeholder.png" alt>
                    </a>
                    <a href="#" class="title">Новгород</a>
                    <div class="desc">Обзор книги: Кушнир И.И. Новгород. – Л.: Стройиздат, 1967.</div>
                </div>
                <div class="item">
                    <a href="#" class="img-wrap">
                        <img src="img/placeholder.png" alt>
                    </a>
                    <a href="#" class="title">Новгород</a>
                    <div class="desc">Обзор книги: Кушнир И.И. Новгород. – Л.: Стройиздат, 1967.</div>
                </div>
                <div class="item">
                    <a href="#" class="img-wrap">
                        <img src="img/placeholder.png" alt>
                    </a>
                    <a href="#" class="title">Новгород</a>
                    <div class="desc">Обзор книги: Кушнир И.И. Новгород. – Л.: Стройиздат, 1967.</div>
                </div>
                <div class="item">
                    <a href="#" class="img-wrap">
                        <img src="img/placeholder.png" alt>
                    </a>
                    <a href="#" class="title">Новгород</a>
                    <div class="desc">Обзор книги: Кушнир И.И. Новгород. – Л.: Стройиздат, 1967.</div>
                </div>
                <div class="item">
                    <a href="#" class="img-wrap">
                        <img src="img/placeholder.png" alt>
                    </a>
                    <a href="#" class="title">Новгород</a>
                    <div class="desc">Обзор книги: Кушнир И.И. Новгород. – Л.: Стройиздат, 1967.</div>
                </div>
                <div class="item">
                    <a href="#" class="img-wrap">
                        <img src="img/placeholder.png" alt>
                    </a>
                    <a href="#" class="title">Новгород</a>
                    <div class="desc">Обзор книги: Кушнир И.И. Новгород. – Л.: Стройиздат, 1967.</div>
                </div>
                <div class="item">
                    <a href="#" class="img-wrap">
                        <img src="img/placeholder.png" alt>
                    </a>
                    <a href="#" class="title">Новгород</a>
                    <div class="desc">Обзор книги: Кушнир И.И. Новгород. – Л.: Стройиздат, 1967.</div>
                </div>
                <div class="item">
                    <a href="#" class="img-wrap">
                        <img src="img/placeholder.png" alt>
                    </a>
                    <a href="#" class="title">Новгород</a>
                    <div class="desc">Обзор книги: Кушнир И.И. Новгород. – Л.: Стройиздат, 1967.</div>
                </div>
                <div class="item">
                    <a href="#" class="img-wrap">
                        <img src="img/placeholder.png" alt>
                    </a>
                    <a href="#" class="title">Новгород</a>
                    <div class="desc">Обзор книги: Кушнир И.И. Новгород. – Л.: Стройиздат, 1967.</div>
                </div>
                <div class="item">
                    <a href="#" class="img-wrap">
                        <img src="img/placeholder.png" alt>
                    </a>
                    <a href="#" class="title">Новгород</a>
                    <div class="desc">Обзор книги: Кушнир И.И. Новгород. – Л.: Стройиздат, 1967.</div>
                </div>
            </div>
            <div class="pagination">
                <a href="#" class="arrow prev">
                    <?= SVG__ARROW_RIGHT ?>
                </a>
                <a href="#" class="page">1</a>
                <a href="#" class="page current">2</a>
                <a href="#" class="page">3</a>
                <a href="#" class="page">4</a>
                <a href="#" class="page">5</a>
                <div class="dots">...</div>
                <a href="#" class="page">9</a>
                <a href="#" class="arrow next">
                    <?= SVG__ARROW_RIGHT ?>
                </a>
            </div>
        </div>
    </div>
</div>

<?php require 'footer.php' ?>