
        <footer class="footer maps">
            <div class="content-wrap">
                <div class="addresses-row">
                    <div class="column">
                        <img src="img/footer-logo-1.png" alt>
                        <div class="text">АНО «Муравейник»</div>
                    </div>
                    <div class="column">
                        <div class="text">
                            Великий Новгород,<br>
                            ул. Б. Московская, 37/9
                        </div>
                        <a href="tel:+78162631710" class="text">8 (816 2) 63-17-10</a>
                        <a href="mailto:info@ant53.ru" class="text">info@ant53.ru</a>
                        <a href="https://vk.com/53ant" class="text" target="_blank">https://vk.com/53ant</a>
                    </div>
                    <div class="separator hide-lg show-sm"></div>
                    <div class="column">
                        <img src="img/footer-logo-2.png" alt>
                        <div class="text">МБУК БЦ «Читай Город»</div>
                    </div>
                    <div class="column">
                        <div class="text">
                            Великий Новгород,<br>
                            проспект Мира, 1
                        </div>
                        <a href="tel:+78162620361" class="text">8 (816 2) 62-03-61</a>
                        <a href="mailto:director@chitajka53.ru" class="text">director@chitajka53.ru</a>
                        <a href="chitajka53.ru" class="text" target="_blank">chitajka53.ru</a>
                    </div>
                </div>
                <div class="copy-row">
                    <div class="copy">© МБУК «БЦ «Читай-город», АНО<br class="hide-lg show-sm-inline"> «МУРАВЕЙНИК», 2016–2022</div>
                    <a href="#" class="link">Политика конфиденциальности</a>
                    <a href="#" class="link">Авторское право</a>
                    <a  class="artgorka" href="#">
                        Дизайн:
                        <?= SVG__ARTGORKA ?>
                    </a>
                </div>
            </div>
        </footer>
        <div class="popup-wrap" data-popup="hide">
            <div class="popup category-select">
                <div class="title">Выберите рубрику:</div>
                <div class="categories-list">
                    <a href="#" class="item">Географическая</a>
                    <a href="#" class="item">Тематическая</a>
                </div>
                <a href="#" class="btn alt" data-popup="subcategory-select">Применить</a>
                <a href="#" class="x" data-popup="hide"><?= SVG__X ?></a>
            </div>
            <div class="popup subcategory-select">
                <div class="title">Тематические рубрики:</div>
                <div class="categories-list">
                    <a href="#" class="item">Города и села</a>
                    <a href="#" class="item">Великий Новгород</a>
                    <a href="#" class="item">Уличная сеть</a>
                    <a href="#" class="item">Шоссе</a>
                    <a href="#" class="item">Уличная сеть</a>
                    <a href="#" class="item">Города и села</a>
                    <a href="#" class="item">Великий Новгород</a>
                    <a href="#" class="item">Уличная сеть</a>
                    <a href="#" class="item">Шоссе</a>
                    <a href="#" class="item">Уличная сеть</a>
                    <a href="#" class="item">Города и села</a>
                    <a href="#" class="item">Великий Новгород</a>
                    <a href="#" class="item">Уличная сеть</a>
                    <a href="#" class="item">Шоссе</a>
                    <a href="#" class="item">Уличная сеть</a>
                    <a href="#" class="item">Города и села</a>
                    <a href="#" class="item">Великий Новгород</a>
                    <a href="#" class="item">Уличная сеть</a>
                    <a href="#" class="item">Шоссе</a>
                    <a href="#" class="item">Уличная сеть</a>
                </div>
                <a href="#" class="btn alt">Применить</a>
                <a href="#" class="x" data-popup="hide"><?= SVG__X ?></a>
            </div>
            <div class="popup edit">
                <div class="title">
                    Чтобы внести изменения, Вам нужно<br>
                    отправить сообщение на почту <a href="mailto:info@ant53.ru">info@ant53.ru</a>
                </div>
                <div class="required-title">В сообщении необходимо указать:</div>
                <ul class="required-list">
                    <li>ссылку на материал</li>
                    <li>ссылку на Ваш профиль</li>
                    <li>тип изменений (исправить ошибку, внести дополнения в текст, добавить файлы, удалить файлы, другая причина – написать, какая именно)</li>
                </ul>
                <div class="text">
                    <p>После этого с Вами свяжется администратор сайта, который откроет Вам доступ к редактированию материала.</p>
                    <p>Обращаем Ваше внимание, что материал на время редактирования будет недоступен пользователям сайта. Поэтому огромная просьба: вносить правки оперативно и сразу же сообщить администратору об окончании работ (так же, по почте) – чтобы Ваш материал как можно быстрее вернулся «в строй». Но, конечно – не в ущерб качеству!</p>
                </div>
                <a href="#" class="x" data-popup="hide"><?= SVG__X ?></a>
            </div>
            <div class="popup author">
                <div class="title">
                    Прекрасное решение, за которое мы Вам<br>
                    очень благодарны!
                </div>
                <div class="required-title">Вам нужно:</div>
                <ul class="required-list">
                    <li>пройти регистрацию;</li>
                    <li>выслать нам все, что вы хотите разместить, на почту info@ant53.ru</li>
                </ul>
                <div class="text">
                    <p>Просим в письме указать ссылку на Ваш профиль – скопируйте из адресной строки в своем личном кабинете, который будет доступен Вам после регистрации.</p>
                    <p>Обращаем Ваше внимание, что присланные материалы должны принадлежать лично Вам – мы не размещаем файлы, скачанные из сети.</p>
                    <p>Желательно прокомментировать присланные материалы: что это, когда и кем создано, чему посвящено – пишите все, что считаете нужным. Это существенно облегчит работу модераторов по проверке присланных данных.</p>
                    <p class="bold">Будем рады видеть Вас в рядах наших авторов!</p>
                </div>
                <a href="#" class="x" data-popup="hide"><?= SVG__X ?></a>
            </div>
            <div class="popup winner">
                <img src="img/placeholder-513x586.png" alt>
                <div class="name">Андреев Александр Алексеевич</div>
                <div class="years">1921 - 1979</div>
                <div class="desc">
                    Уроженец Новгородской губернии (д. Филиппково). До и после Великой Отечественной войны жил на Новгородчине. Родственники героя живут в Великом Новгороде.
                </div>
                <a href="#" class="x" data-popup="hide"><?= SVG__X ?></a>
            </div>
            <div class="popup success">
                <div class="title">Ваше обращение успешно отправлено!</div>
                <div class="subtitle">Благодарим за внимание к нашему ресурсу.</div>
                <a href="#" class="x" data-popup="hide"><?= SVG__X ?></a>
            </div>
            <form method="post" class="popup signup">
                <div class="title">Заполните профиль:</div>
                <div class="input-wrap">
                    <input type="text" name="firstname" placeholder="Имя" required>
                </div>
                <div class="input-wrap">
                    <input type="text" name="patronymic" placeholder="Отчество *">
                </div>
                <div class="input-wrap">
                    <input type="text" name="lastname" placeholder="Фамилия" required>
                </div>
                <div class="input-wrap">
                    <input type="text" name="birthday" placeholder="Дата рождения" required>
                </div>
                <div class="input-wrap">
                    <input type="text" name="city" placeholder="Населённый пункт" required>
                </div>
                <div class="input-wrap">
                    <input type="text" name="job" placeholder="Место работы *">
                </div>
                <div class="input-wrap">
                    <input type="email" name="email" placeholder="Почта" required>
                </div>
                <div class="input-wrap">
                    <input type="text" name="phone" placeholder="Телефон" required>
                </div>
                <div class="input-wrap">
                    <input type="password" name="password" placeholder="Пароль" required>
                </div>
                <div class="input-wrap">
                    <input type="password" name="password-2" placeholder="Подтвердите пароль" required>
                </div>
                <div class="input-wrap captcha">
                    <div class="captcha-wrap"></div>
                    <input type="text" name="captcha" placeholder="Введите текст с картинки" required>
                </div>
                <label class="check-wrap">
                    <input type="checkbox" name="policy">
                    <span class="check">
                        <img src="img/svg/check.svg" alt>
                    </span>
                    <span class="text">Я принимаю условия <a href="#">Политики конфиденциальности</a></span>
                </label>
                <label class="check-wrap">
                    <input type="checkbox" name="policy">
                    <span class="check">
                        <img src="img/svg/check.svg" alt>
                    </span>
                    <span class="text">Я ознакомлен с <a href="#">Правилами размещения</a></span>
                </label>
                <button type="submit" class="btn alt">Зарегистрироваться</button>
                <a href="#" class="x" data-popup="hide"><?= SVG__X ?></a>
            </form>
            <div class="popup signup-finish">
                <div class="title">
                    Спасибо за регистрацию на сайте<br>
                    Новгородской цифровой библиотеки!
                </div>
                <div class="library-card">1-2021</div>
                <div class="text">
                    Номер Вашего читательского билета<br>
                    на текущий год
                </div>
                <a href="#" class="btn">Перейти в Личный кабинет</a>
            </div>
        </div>
        <script src="js/ajax.js"></script>
    </body>
</html>