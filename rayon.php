<?php $pageTitle = 'Все материалы';

require 'header.php' ?>

<div class="rayon-page articles-page">
    <div class="content-wrap">
        <div class="page-title">
            <div class="container">
                <p>Отбор по районам <br> <span style="color: #ca9d75;">НОВГОРОДСКОЙ ОБЛАСТИ</span></p>
                <div class="count">3000</div>
            </div>
        </div>
        <nav class="content-fetch">
            <ul class="content-fetch-items content-wrap">
                <li class="item">Батецкий район</li>
                <li class="item">Боровичский район</li>
                <li class="item">Валдайский район</li>
                <li class="item">Волотовский район</li>
                <li class="item">Демянский район</li>
                <li class="item">Крестецкий район</li>
                <li class="item">Любытинский район</li>
                <li class="item">Маловишерский район</li>
            </ul>
        </nav>
        <div class="items-grid">
            <a href="#" class="item-link">
                <div class="item-link__img-container">
                    <img src="img/placeholder.png" alt>
                </div>
                <h2  class="title">Новгород</h2>
                <p class="desc">Обзор книги: Кушнир И.И. Новгород. – Л.: Стройиздат, 1967.</p>
            </a>
            <a href="#" class="item-link">
                <div class="item-link__img-container">
                    <img src="img/placeholder.png" alt>
                </div>
                <h2  class="title">Новгород</h2>
                <p class="desc">Обзор книги: Кушнир И.И. Новгород. – Л.: Стройиздат, 1967.</p>
            </a>
            <a href="#" class="item-link">
                <div class="item-link__img-container">
                    <img src="img/placeholder.png" alt>
                </div>
                <h2  class="title">Новгород</h2>
                <p class="desc">Обзор книги: Кушнир И.И. Новгород. – Л.: Стройиздат, 1967.</p>
            </a>
            <a href="#" class="item-link">
                <div class="item-link__img-container">
                    <img src="img/placeholder.png" alt>
                </div>
                <h2  class="title">Новгород</h2>
                <p class="desc">Обзор книги: Кушнир И.И. Новгород. – Л.: Стройиздат, 1967.</p>
            </a>
            <a href="#" class="item-link">
                <div class="item-link__img-container">
                    <img src="img/placeholder.png" alt>
                </div>
                <h2  class="title">Новгород</h2>
                <p class="desc">Обзор книги: Кушнир И.И. Новгород. – Л.: Стройиздат, 1967.</p>
            </a>
            <a href="#" class="item-link">
                <div class="item-link__img-container">
                    <img src="img/placeholder.png" alt>
                </div>
                <h2  class="title">Новгород</h2>
                <p class="desc">Обзор книги: Кушнир И.И. Новгород. – Л.: Стройиздат, 1967.</p>
            </a>
            <a href="#" class="item-link">
                <div class="item-link__img-container">
                    <img src="img/placeholder.png" alt>
                </div>
                <h2  class="title">Новгород</h2>
                <p class="desc">Обзор книги: Кушнир И.И. Новгород. – Л.: Стройиздат, 1967.</p>
            </a>
            <a href="#" class="item-link">
                <div class="item-link__img-container">
                    <img src="img/placeholder.png" alt>
                </div>
                <h2  class="title">Новгород</h2>
                <p class="desc">Обзор книги: Кушнир И.И. Новгород. – Л.: Стройиздат, 1967.</p>
            </a>

        </div>
        <div class="pagination">
            <a href="#" class="arrow prev">
                <?= SVG__ARROW_RIGHT ?>
            </a>
            <a href="#" class="page">1</a>
            <a href="#" class="page current">2</a>
            <a href="#" class="page">3</a>
            <a href="#" class="page">4</a>
            <a href="#" class="page">5</a>
            <div class="dots">...</div>
            <a href="#" class="page">9</a>
            <a href="#" class="arrow next">
                <?= SVG__ARROW_RIGHT ?>
            </a>
        </div>
    </div>
</div>

<?php require 'footer.php' ?>