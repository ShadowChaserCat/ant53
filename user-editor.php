<?php $pageTitle = 'ЛК Редактор';

require 'header.php' ?>

<div class="user-page">
    <div class="content-wrap">
        <div class="user-wrap">
            <div class="avatar">
                <img src="img/user-page/avatar.png" alt>
            </div>
            <div class="name">Антон Петров</div>
            <div class="role">Редактор</div>
            <div class="status">Online</div>
            <a href="#" class="btn gray edit-btn">Редактировать профиль</a>
            <div class="info-wrap">
                <div class="column">
                    <div class="title">Место работы:</div>
                    <div class="text">название</div>
                </div>
                <div class="column">
                    <div class="title">Населенный пункт:</div>
                    <div class="text">Великий Новгород</div>
                </div>
            </div>
            <a href="#" class="round-btn back-btn"><?= SVG__ARROW_RIGHT ?></a>
            <a href="#" class="round-btn fav-btn"><?= SVG__HEART ?></a>
        </div>
    </div>
    <div class="bg-wrap">
        <div class="content-wrap">
            <div class="links-wrap">
                <div class="item">
                    <a href="#" class="title">
                        Автор материалов
                        <?= SVG__ARROW_TOP_RIGHT ?>
                    </a>
                    <div class="list">
                        <a href="#" class="link">Пункт 1</a>
                        <a href="#" class="link">Пункт 2</a>
                        <a href="#" class="link">Пункт 3</a>
                    </div>
                </div>
                <div class="item">
                    <a href="#" class="title">
                        Участие в материалах других пользователей
                        <?= SVG__ARROW_TOP_RIGHT ?>
                    </a>
                    <div class="list">
                        <div class="columns-wrap">
                            <div class="column">
                                <div>
                                    <p>Мои информационные коллекции: 0</p>
                                    <p>Добавлено файлов в мои коллекции: 0</p>
                                </div>
                                <div>
                                    <p>Мои обзоры книг: 0</p>
                                    <p>Мои ссылки: 0</p>
                                </div>
                            </div>
                            <div class="column">
                                <div>
                                    <p>Мои библиографические списки: 0</p>
                                    <p>Добавлено записей в мои списки: 0</p>
                                </div>
                                <div>
                                    <p>Мои записи в календаре событий: 0</p>
                                    <p>Мои тесты: 0</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="item">
                    <a href="#" class="title">
                        Темы материалов
                        <?= SVG__ARROW_TOP_RIGHT ?>
                    </a>
                    <div class="list">
                        <a href="#" class="link">Пункт 1</a>
                        <a href="#" class="link">Пункт 2</a>
                        <a href="#" class="link">Пункт 3</a>
                    </div>
                </div>
                <div class="item">
                    <a href="#" class="title">
                        География материалов
                        <?= SVG__ARROW_TOP_RIGHT ?>
                    </a>
                    <div class="list">
                        <a href="#" class="link">Пункт 1</a>
                        <a href="#" class="link">Пункт 2</a>
                        <a href="#" class="link">Пункт 3</a>
                    </div>
                </div>
            </div>
            <div class="btns-wrap">
                <a href="#" class="btn alt">Добавить материалы</a>
                <a href="#" class="btn gray">Редактировать свой материал</a>
            </div>
            <div class="updates-block my-materials">
                <div class="block-title">
                    <p>Мои</p>
                    <p>материалы</p>
                </div>
                <div class="items-grid">
                    <div class="item">
                        <a href="#" class="img-wrap">
                            <img src="img/placeholder.png" alt>
                        </a>
                        <a href="#" class="title">Новгород</a>
                        <div class="desc">Обзор книги: Кушнир И.И. Новгород. – Л.: Стройиздат, 1967.</div>
                    </div>
                    <div class="item">
                        <a href="#" class="img-wrap">
                            <img src="img/placeholder.png" alt>
                        </a>
                        <a href="#" class="title">Новгород</a>
                        <div class="desc">Обзор книги: Кушнир И.И. Новгород. – Л.: Стройиздат, 1967.</div>
                    </div>
                    <div class="item">
                        <a href="#" class="img-wrap">
                            <img src="img/placeholder.png" alt>
                        </a>
                        <a href="#" class="title">Новгород</a>
                        <div class="desc">Обзор книги: Кушнир И.И. Новгород. – Л.: Стройиздат, 1967.</div>
                    </div>
                    <div class="item">
                        <a href="#" class="img-wrap">
                            <img src="img/placeholder.png" alt>
                        </a>
                        <a href="#" class="title">Новгород</a>
                        <div class="desc">Обзор книги: Кушнир И.И. Новгород. – Л.: Стройиздат, 1967.</div>
                    </div>
                    <div class="item">
                        <a href="#" class="img-wrap">
                            <img src="img/placeholder.png" alt>
                        </a>
                        <a href="#" class="title">Новгород</a>
                        <div class="desc">Обзор книги: Кушнир И.И. Новгород. – Л.: Стройиздат, 1967.</div>
                    </div>
                    <div class="item">
                        <a href="#" class="img-wrap">
                            <img src="img/placeholder.png" alt>
                        </a>
                        <a href="#" class="title">Новгород</a>
                        <div class="desc">Обзор книги: Кушнир И.И. Новгород. – Л.: Стройиздат, 1967.</div>
                    </div>
                    <div class="item">
                        <a href="#" class="img-wrap">
                            <img src="img/placeholder.png" alt>
                        </a>
                        <a href="#" class="title">Новгород</a>
                        <div class="desc">Обзор книги: Кушнир И.И. Новгород. – Л.: Стройиздат, 1967.</div>
                    </div>
                    <div class="item">
                        <a href="#" class="img-wrap">
                            <img src="img/placeholder.png" alt>
                        </a>
                        <a href="#" class="title">Новгород</a>
                        <div class="desc">Обзор книги: Кушнир И.И. Новгород. – Л.: Стройиздат, 1967.</div>
                    </div>
                </div>
                <div class="pagination">
                    <a href="#" class="arrow prev">
                        <?= SVG__ARROW_RIGHT ?>
                    </a>
                    <a href="#" class="page">1</a>
                    <a href="#" class="page current">2</a>
                    <a href="#" class="page">3</a>
                    <a href="#" class="page">4</a>
                    <a href="#" class="page">5</a>
                    <div class="dots">...</div>
                    <a href="#" class="page">9</a>
                    <a href="#" class="arrow next">
                        <?= SVG__ARROW_RIGHT ?>
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>

<?php require 'footer.php' ?>