<?php $pageTitle = 'Карточка - уникальная';

require 'header.php' ?>

<div class="card-page unique">
    <div class="info-block" style="background-image: url(img/card-unique-page/info-block-bg.png);">
        <div class="content-wrap">
            <div class="title">Андреев</div>
            <div class="subtitle">Александр Алексеевич</div>
            <div class="years">1921 - 1979</div>
            <div class="desc-wrap">
                <div class="img-wrap">
                    <img src="img/card-page/info-block-img.png" alt>
                </div>
                <div class="desc">
                    <p>
                        Уроженец Новгородской губернии (д. Филиппково).<br>
                        До и после Великой Отечественной войны жил<br>
                        на Новгородчине. Родственники героя живут в Великом Новгороде.
                    </p>
                    <p>Родился 16 марта 1911 года в деревне Филиппково Новгородской губернии (на тот момент деревня предположительно относилась к Любынскому сельсовету Старорусского уезда (сейчас с/с Шимского р-на). Ныне деревни не существует). До войны познакомился с будущей женой Ниной (уроженкой своей деревни).</p>
                    <p>Родился 16 марта 1911 года в деревне Филиппково Новгородской губернии (на тот момент деревня предположительно относилась к Любынскому сельсовету Старорусского уезда (сейчас с/с Шимского р-на). Ныне деревни не существует).</p>
                </div>
            </div>
            <a href="#" class="round-btn back-btn">
                <?= SVG__ARROW_RIGHT ?>
            </a>
            <a href="#" class="round-btn fav-btn">
                <?= SVG__HEART ?>
            </a>
            <div class="round-btn age-limit">12+</div>
        </div>
    </div>
    <div class="about-block">
        <div class="content-wrap">
            <div class="block-title">О герое</div>
            <div class="text">
                <p>Уроженец Новгородской губернии (д. Филиппково). До и после Великой Отечественной войны жил на Новгородчине. Родственники героя живут в Великом Новгороде.</p>
                <p>Родился 16 марта 1911 года в деревне Филиппково Новгородской губернии (на тот момент деревня предположительно относилась к Любынскому сельсовету Старорусского уезда (сейчас с/с Шимского р-на). Ныне деревни не существует). До войны познакомился с будущей женой Ниной (уроженкой своей деревни).</p>
                <p>Воевал подо Ржевом. В октябре 1944 года был тяжело ранен, лежал в госпитале в Торжке Тверской области. Был признан инвалидом 2 группы. Вернулся в родную деревню, сразу женился. В 1945 году у Александра Алексеевича и Нины Михайловны родилась первая дочь (всего в семье было восемь детей). Позже семья перебралась в деревню Учно Волотовского района Новгородской области. Повзрослев, дети разьехались, а супруги остались в ставшей родной дервне, где прожили до конца жизни. Александр Алексеевич работал кузнецом, был мастером своего дела.</p>
                <p>Умер 27 декабря 1979 года.</p>
                <p>Дети и внуки живут в Великом Новгороде.</p>
                <p>***</p>
                <p>Сведения для «Новгородского альбома победителей» предоставила дочь героя, Надежда Александровна Панфилова. Прилагаемые материалы переданы в новгородскую электронную библиотеку для архивного хранения, с правом публикации на сайте.</p>
            </div>
        </div>
    </div>
    <div class="tags-block">
        <div class="content-wrap">
            <div class="block-title">
                <p>Фронты</p>
            </div>
            <div class="tags-list">
                <div class="tag">Волховский</div>
                <div class="tag">Белорусский</div>
                <div class="tag">Волховский</div>
                <div class="tag">Белорусский</div>
                <div class="tag">Волховский</div>
                <div class="tag">Белорусский</div>
                <div class="wrap ml"></div>
                <div class="tag">Волховский</div>
                <div class="tag">Белорусский</div>
                <div class="tag">Волховский</div>
                <div class="tag">Белорусский</div>
                <div class="tag">Волховский</div>
                <div class="tag">Белорусский</div>
            </div>
        </div>
    </div>
    <div class="tags-block">
        <div class="content-wrap">
            <div class="block-title">
                <p>Военные</p>
                <p>операции</p>
            </div>
            <div class="tags-list">
                <div class="tag">Волховский</div>
                <div class="tag">Белорусский</div>
                <div class="tag">Волховский</div>
                <div class="tag">Белорусский</div>
                <div class="tag">Волховский</div>
                <div class="tag">Белорусский</div>
                <div class="wrap ml"></div>
                <div class="tag">Волховский</div>
                <div class="tag">Белорусский</div>
                <div class="tag">Волховский</div>
                <div class="tag">Белорусский</div>
                <div class="tag">Волховский</div>
                <div class="tag">Белорусский</div>
            </div>
        </div>
    </div>
    <div class="photo-block">
        <div class="content-wrap">
            <div class="block-title">Фотоальбом</div>
            <div class="swiper">
                <div class="swiper-wrapper">
                    <div class="swiper-slide">
                        <img src="img/mainpage/block-5-img-1.png" alt>
                        <div class="title">Заголовок</div>
                        <div class="desc">Краткое описание текст примерно на одно-два предложения</div>
                    </div>
                    <div class="swiper-slide">
                        <img src="img/mainpage/block-5-img-2.png" alt>
                        <div class="title">Заголовок</div>
                        <div class="desc">Краткое описание текст примерно на одно-два предложения</div>
                    </div>
                    <div class="swiper-slide">
                        <img src="img/mainpage/block-5-img-3.png" alt>
                        <div class="title">Заголовок</div>
                        <div class="desc">Краткое описание текст примерно на одно-два предложения</div>
                    </div>
                    <div class="swiper-slide">
                        <img src="img/mainpage/block-5-img-1.png" alt>
                        <div class="title">Заголовок</div>
                        <div class="desc">Краткое описание текст примерно на одно-два предложения</div>
                    </div>
                    <div class="swiper-slide">
                        <img src="img/mainpage/block-5-img-2.png" alt>
                        <div class="title">Заголовок</div>
                        <div class="desc">Краткое описание текст примерно на одно-два предложения</div>
                    </div>
                    <div class="swiper-slide">
                        <img src="img/mainpage/block-5-img-3.png" alt>
                        <div class="title">Заголовок</div>
                        <div class="desc">Краткое описание текст примерно на одно-два предложения</div>
                    </div>
                </div>
                <div class="swiper-scrollbar"></div>
            </div>
            <div class="btns-wrap">
                <a href="#" class="round-btn prev">
                    <?= SVG__ARROW_RIGHT ?>
                </a>
                <a href="#" class="round-btn next">
                    <?= SVG__ARROW_RIGHT ?>
                </a>
            </div>
            <a href="#" class="btn alt more-btn">Смотреть все фото</a>
        </div>
    </div>
</div>

<?php require 'footer.php' ?>