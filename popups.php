<?php $pageTitle = 'Попапы';

require 'header.php' ?>

<div class="content-wrap" style="padding-top: 100px;">
    <a href="#" class="btn" data-popup="category-select">Выбор категории (category-select)</a>
    <a href="#" class="btn" data-popup="subcategory-select" style="margin-top: 50px;">Выбор подкатегории (subcategory-select)</a>
    <a href="#" class="btn" data-popup="edit" style="margin-top: 50px;">Редактировать материал (edit)</a>
    <a href="#" class="btn" data-popup="author" style="margin-top: 50px;">Стать автором (author)</a>
    <a href="#" class="btn" data-popup="winner" style="margin-top: 50px;">Попап победителя, попап к альбому (winner)</a>
    <a href="#" class="btn" data-popup="success" style="margin-top: 50px;">Сообщение об отправке сообщения (success)</a>
    <a href="#" class="btn" data-popup="signup" style="margin-top: 50px;">Регистрация (signup)</a>
    <a href="#" class="btn" data-popup="signup-finish" style="margin-top: 50px;">Спасибо за регистрацию (signup-finish)</a>
</div>

<?php require 'footer.php' ?>