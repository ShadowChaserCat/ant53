<?php $pageTitle = 'Карточка - универсальная';

require 'header.php' ?>

<div class="card-page">
    <div class="info-block" >
        <div class="content-wrap">
            <div class="title">Андреев</div>
            <div class="subtitle">Александр Алексеевич</div>
            <div class="years">1921 - 1979</div>
            <div class="desc-wrap">
                <div class="img-wrap">
                    <img src="img/card-page/info-block-img.png" alt>
                </div>
                <div class="desc">
                    <p>
                        Уроженец Новгородской губернии (д. Филиппково).<br>
                        До и после Великой Отечественной войны жил<br>
                        на Новгородчине. Родственники героя живут в Великом Новгороде.
                    </p>
                    <p>Родился 16 марта 1911 года в деревне Филиппково Новгородской губернии (на тот момент деревня предположительно относилась к Любынскому сельсовету</p>
                    <p>Родился 16 марта 1911 года в деревне Филиппково Новгородской губернии (на тот момент деревня предположительно относилась к Любынскому сельсовету</p>
                    <p>Родился 16 марта 1911 года в деревне Филиппково Новгородской губернии (на тот момент деревня предположительно относилась к Любынскому сельсовету</p>
                </div>
            </div>
            <a href="#" class="more-wrap">
                Смотреть сведения об авторах
                <?= SVG__ARROW_TOP_RIGHT ?>
            </a>
            <a href="#" class="round-btn back-btn">
                <?= SVG__ARROW_RIGHT ?>
            </a>
            <a href="#" class="round-btn fav-btn">
                <?= SVG__HEART ?>
            </a>
            <div class="round-btn age-limit">12+</div>
            <div class="hint fav">
                Вы сохраняете этот материал в раздел «Избранное». Раздел доступен в Личном кабинете читателя –<br>
                если Вы зарегистрированы на сайте и вошли под своим логином/паролем.
            </div>
            <div class="hint age">
                Обратите также внимание на возрастную маркировку материала. Она показывает, есть ли возрастные ограничения при его просмотре, в соответствии с Федеральным законом РФ от 29.12.2010 г. № 436-ФЗ «О защите детей от информации, причиняющей вред их здоровью и развитию».
            </div>
        </div>
    </div>
    <div class="more-block">
        <div class="content-wrap">
            <div class="columns-wrap">
                <div class="column">
                    <div class="title">Материал создал:</div>
                    <div class="author">
                        <div class="img-wrap">
                            <img src="img/placeholder-darken.png" alt>
                        </div>
                        <div class="name">Татьяна Карпова</div>
                    </div>
                    <div class="text">МБУК "БЦ "Читай-город"</div>
                    <div class="text">2021-08-10 19:52:43 (Offline)</div>
                    <a href="#" class="profile-link">Смотреть профиль</a>
                </div>
                <div class="column">
                    <div class="title">Модератор:</div>
                    <div class="author">
                        <div class="img-wrap">
                            <img src="img/placeholder-darken.png" alt>
                        </div>
                        <div class="name">Татьяна Карпова</div>
                    </div>
                    <div class="text">МБУК "БЦ "Читай-город"</div>
                    <div class="text">2021-08-10 19:52:43 (Offline)</div>
                    <a href="#" class="profile-link">Смотреть профиль</a>
                </div>
                <div class="column">
                    <div class="title">Дата публикации:</div>
                    <div class="date">06.08.2021</div>
                </div>
                <div class="column">
                    <div class="title">Последнее обновление:</div>
                    <div class="date">06.08.2021</div>
                </div>
                <div class="column full-width">
                    <div class="title">В создании материала принимали участие:</div>
                    <div class="text">
                        София Петровна Дудко, мама старшего помощника командира АПРК «Курск» (Санкт-Петербург); администрация гимназии «Исток» Великого Новгорода.
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="categories-block">
        <div class="content-wrap">
            <div class="block-title">Рубрики</div>
            <div class="types-wrap">
                <div class="item purple">Тематические</div>
                <div class="item brown">Географические по районам</div>
                <div class="item red">Географические по регионам РФ</div>
                <div class="item cyan">Географические по странам</div>
            </div>
            <div class="tags-slider">
                <div class="swiper">
                    <div class="swiper-wrapper">
                        <div class="swiper-slide">
                            <a href="#" class="tag red">Астрономия</a>
                            <a href="#" class="tag brown">Великий Новгород</a>
                            <a href="#" class="tag cyan">Операция «Искра»</a>
                            <a href="#" class="tag purple">Волховский</a>
                            <a href="#" class="tag red">Военные памятники и мемориалы</a>
                            <a href="#" class="tag brown">Памятные знаки</a>
                            <div class="wrap"></div>
                            <a href="#" class="tag purple">Операция «Искра»</a>
                            <a href="#" class="tag red">Астрономия</a>
                            <a href="#" class="tag brown">Великий Новгород</a>
                            <a href="#" class="tag purple">Операция «Искра»</a>
                            <a href="#" class="tag brown">Волховский</a>
                            <a href="#" class="tag purple">Военные памятники и мемориалы</a>
                            <div class="wrap"></div>
                            <a href="#" class="tag brown">Волховский</a>
                            <a href="#" class="tag cyan">Военные памятники и мемориалы</a>
                            <a href="#" class="tag purple">Астрономия</a>
                            <a href="#" class="tag brown">Великий Новгород</a>
                            <a href="#" class="tag cyan">Операция «Искра»</a>
                            <a href="#" class="tag purple">Операция «Искра»</a>
                            <div class="wrap"></div>
                            <a href="#" class="tag purple">Операция «Искра»</a>
                            <a href="#" class="tag purple">Астрономия</a>
                            <a href="#" class="tag brown">Великий Новгород</a>
                            <a href="#" class="tag brown">Операция «Искра»</a>
                            <a href="#" class="tag brown">Волховский</a>
                            <a href="#" class="tag purple">Военные памятники и мемориалы</a>
                        </div>
                        <div class="swiper-slide">
                            <a href="#" class="tag red">Астрономия</a>
                            <a href="#" class="tag brown">Великий Новгород</a>
                            <a href="#" class="tag cyan">Операция «Искра»</a>
                            <a href="#" class="tag purple">Волховский</a>
                            <a href="#" class="tag red">Военные памятники и мемориалы</a>
                            <a href="#" class="tag brown">Памятные знаки</a>
                            <div class="wrap"></div>
                            <a href="#" class="tag purple">Операция «Искра»</a>
                            <a href="#" class="tag red">Астрономия</a>
                            <a href="#" class="tag brown">Великий Новгород</a>
                            <a href="#" class="tag purple">Операция «Искра»</a>
                            <a href="#" class="tag brown">Волховский</a>
                            <a href="#" class="tag purple">Военные памятники и мемориалы</a>
                            <div class="wrap"></div>
                            <a href="#" class="tag brown">Волховский</a>
                            <a href="#" class="tag cyan">Военные памятники и мемориалы</a>
                            <a href="#" class="tag purple">Астрономия</a>
                            <a href="#" class="tag brown">Великий Новгород</a>
                            <a href="#" class="tag cyan">Операция «Искра»</a>
                            <a href="#" class="tag purple">Операция «Искра»</a>
                            <div class="wrap"></div>
                            <a href="#" class="tag purple">Операция «Искра»</a>
                            <a href="#" class="tag purple">Астрономия</a>
                            <a href="#" class="tag brown">Великий Новгород</a>
                            <a href="#" class="tag brown">Операция «Искра»</a>
                            <a href="#" class="tag brown">Волховский</a>
                            <a href="#" class="tag purple">Военные памятники и мемориалы</a>
                        </div>
                        <div class="swiper-slide">
                            <a href="#" class="tag red">Астрономия</a>
                            <a href="#" class="tag brown">Великий Новгород</a>
                            <a href="#" class="tag cyan">Операция «Искра»</a>
                            <a href="#" class="tag purple">Волховский</a>
                            <a href="#" class="tag red">Военные памятники и мемориалы</a>
                            <a href="#" class="tag brown">Памятные знаки</a>
                            <div class="wrap"></div>
                            <a href="#" class="tag purple">Операция «Искра»</a>
                            <a href="#" class="tag red">Астрономия</a>
                            <a href="#" class="tag brown">Великий Новгород</a>
                            <a href="#" class="tag purple">Операция «Искра»</a>
                            <a href="#" class="tag brown">Волховский</a>
                            <a href="#" class="tag purple">Военные памятники и мемориалы</a>
                            <div class="wrap"></div>
                            <a href="#" class="tag brown">Волховский</a>
                            <a href="#" class="tag cyan">Военные памятники и мемориалы</a>
                            <a href="#" class="tag purple">Астрономия</a>
                            <a href="#" class="tag brown">Великий Новгород</a>
                            <a href="#" class="tag cyan">Операция «Искра»</a>
                            <a href="#" class="tag purple">Операция «Искра»</a>
                            <div class="wrap"></div>
                            <a href="#" class="tag purple">Операция «Искра»</a>
                            <a href="#" class="tag purple">Астрономия</a>
                            <a href="#" class="tag brown">Великий Новгород</a>
                            <a href="#" class="tag brown">Операция «Искра»</a>
                            <a href="#" class="tag brown">Волховский</a>
                            <a href="#" class="tag purple">Военные памятники и мемориалы</a>
                        </div>
                    </div>
                    <div class="swiper-scrollbar"></div>
                </div>
                <div class="slider-btns">
                    <a href="#" class="round-btn prev">
                        <?= SVG__ARROW_RIGHT ?>
                    </a>
                    <a href="#" class="round-btn next">
                        <?= SVG__ARROW_RIGHT ?>
                    </a>
                </div>
            </div>
        </div>
    </div>
    <div class="collections-block">
        <div class="content-wrap">
            <div class="block-title">
                <p>Информационные</p>
                <p>блоки коллекции</p>
            </div>
            <div class="items-wrap">
                <a href="#" class="item">
                    Библиографические материалы
                    <?= SVG__ARROW_RIGHT ?>
                    <img src="img/card-page/collections-block-img.png" alt>
                </a>
                <a href="#" class="item">
                    Статья
                    <?= SVG__ARROW_RIGHT ?>
                    <img src="img/card-page/collections-block-img.png" alt>
                </a>
                <a href="#" class="item">
                    Альбом
                    <?= SVG__ARROW_RIGHT ?>
                    <img src="img/card-page/collections-block-img.png" alt>
                </a>
                <a href="#" class="item">
                    Великий новгород
                    <?= SVG__ARROW_RIGHT ?>
                    <img src="img/card-page/collections-block-img.png" alt>
                </a>
                <div class="pagination">
                    <a href="#" class="arrow prev">
                        <?= SVG__ARROW_RIGHT ?>
                    </a>
                    <a href="#" class="page">1</a>
                    <a href="#" class="page current">2</a>
                    <a href="#" class="page">3</a>
                    <a href="#" class="page">4</a>
                    <a href="#" class="page">5</a>
                    <div class="dots">...</div>
                    <a href="#" class="page">9</a>
                    <a href="#" class="arrow next">
                        <?= SVG__ARROW_RIGHT ?>
                    </a>
                </div>
            </div>
        </div>
    </div>
    <div class="photo-block">
        <div class="content-wrap">
            <div class="block-title">Фотоальбом</div>
            <div class="swiper last-update-swiper ">
                <div class="swiper-wrapper">
                    <div class="swiper-slide">
                        <div class="swiper-img-container">
                            <img src="img/mainpage/block-5-img-1.png" alt>
                        </div>
                        <div class="desc">Краткое описание текст примерно на одно-два предложения</div>
                    </div>
                    <div class="swiper-slide">
                        <div class="swiper-img-container">
                            <img src="img/mainpage/block-5-img-2.png" alt>
                        </div>
                        <div class="desc">Краткое описание текст примерно на одно-два предложения</div>
                    </div>
                    <div class="swiper-slide">
                        <div class="swiper-img-container">
                            <img src="img/mainpage/block-5-img-3.png" alt>
                        </div>
                        <div class="desc">Краткое описание текст примерно на одно-два предложения</div>
                    </div>
                    <div class="swiper-slide">
                        <div class="swiper-img-container">
                            <img src="img/mainpage/block-5-img-1.png" alt>
                        </div>
                        <div class="desc">Краткое описание текст примерно на одно-два предложения</div>
                    </div>
                    <div class="swiper-slide">
                        <div class="swiper-img-container">
                            <img src="img/mainpage/block-5-img-2.png" alt>
                        </div>
                        <div class="desc">Краткое описание текст примерно на одно-два предложения</div>
                    </div>
                    <div class="swiper-slide">
                        <div class="swiper-img-container">
                            <img src="img/mainpage/block-5-img-3.png" alt>
                        </div>
                        <div class="desc">Краткое описание текст примерно на одно-два предложения</div>
                    </div>
                </div>
                <div class="swiper-scrollbar"></div>
            </div>
            <div class="btns-wrap">
                <a href="#" class="round-btn prev">
                    <?= SVG__ARROW_RIGHT ?>
                </a>
                <a href="#" class="round-btn next">
                    <?= SVG__ARROW_RIGHT ?>
                </a>
            </div>
            <a href="#" class="btn alt more-btn">Смотреть все фото</a>
        </div>
    </div>
</div>

<?php require 'footer.php' ?>