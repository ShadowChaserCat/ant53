<?php $pageTitle = 'Документы';

require 'header.php' ?>

    <div class="documents-page">
        <div class="content-wrap">
            <div class="page-title">
                <p>Документы</p>
                <a href="#" class="round-btn back-btn"><?= SVG__ARROW_RIGHT ?></a>
            </div>
            <div class="items-list">
                <a href="#" class="document-item">
                    <div class="icon"><?= SVG__PAPER ?></div>
                    <div class="title">Курс на социально-ориентированный национальный проект представляет собой интересный эксперимент проверки благоприятных перспектив.</div>
                </a>
                <a href="#" class="document-item">
                    <div class="icon"><?= SVG__PAPER ?></div>
                    <div class="title">Курс на социально-ориентированный национальный проект представляет собой интересный эксперимент проверки благоприятных перспектив.</div>
                </a>
                <a href="#" class="document-item">
                    <div class="icon"><?= SVG__PAPER ?></div>
                    <div class="title">Курс на социально-ориентированный национальный проект представляет собой интересный эксперимент проверки благоприятных перспектив.</div>
                </a>
                <a href="#" class="document-item">
                    <div class="icon"><?= SVG__PAPER ?></div>
                    <div class="title">Курс на социально-ориентированный национальный проект представляет собой интересный эксперимент проверки благоприятных перспектив.</div>
                </a>
                <a href="#" class="document-item">
                    <div class="icon"><?= SVG__PAPER ?></div>
                    <div class="title">Курс на социально-ориентированный национальный проект представляет собой интересный эксперимент проверки благоприятных перспектив.</div>
                </a>
                <a href="#" class="document-item">
                    <div class="icon"><?= SVG__PAPER ?></div>
                    <div class="title">Курс на социально-ориентированный национальный проект представляет собой интересный эксперимент проверки благоприятных перспектив.</div>
                </a>
                <a href="#" class="document-item">
                    <div class="icon"><?= SVG__PAPER ?></div>
                    <div class="title">Курс на социально-ориентированный национальный проект представляет собой интересный эксперимент проверки благоприятных перспектив.</div>
                </a>
                <a href="#" class="document-item">
                    <div class="icon"><?= SVG__PAPER ?></div>
                    <div class="title">Курс на социально-ориентированный национальный проект представляет собой интересный эксперимент проверки благоприятных перспектив.</div>
                </a>
                <a href="#" class="document-item">
                    <div class="icon"><?= SVG__PAPER ?></div>
                    <div class="title">Курс на социально-ориентированный национальный проект представляет собой интересный эксперимент проверки благоприятных перспектив.</div>
                </a>
                <a href="#" class="document-item">
                    <div class="icon"><?= SVG__PAPER ?></div>
                    <div class="title">Курс на социально-ориентированный национальный проект представляет собой интересный эксперимент проверки благоприятных перспектив.</div>
                </a>
                <a href="#" class="document-item">
                    <div class="icon"><?= SVG__PAPER ?></div>
                    <div class="title">Курс на социально-ориентированный национальный проект представляет собой интересный эксперимент проверки благоприятных перспектив.</div>
                </a>
                <a href="#" class="document-item">
                    <div class="icon"><?= SVG__PAPER ?></div>
                    <div class="title">Курс на социально-ориентированный национальный проект представляет собой интересный эксперимент проверки благоприятных перспектив.</div>
                </a>
            </div>
            <div class="pagination">
                <a href="#" class="arrow prev">
                    <?= SVG__ARROW_RIGHT ?>
                </a>
                <a href="#" class="page">1</a>
                <a href="#" class="page current">2</a>
                <a href="#" class="page">3</a>
                <a href="#" class="page">4</a>
                <a href="#" class="page">5</a>
                <div class="dots">...</div>
                <a href="#" class="page">9</a>
                <a href="#" class="arrow next">
                    <?= SVG__ARROW_RIGHT ?>
                </a>
            </div>
        </div>
    </div>

<?php require 'footer.php' ?>