<?php $pageTitle = 'Спецпроекты - типовой';

require 'header.php' ?>

<div class="spec-project-single-page">
    <div class="content-wrap">
        <div class="page-title">
            <p>Новгород</p>
            <p>в кадре и за кадром</p>
            <a href="#" class="round-btn back-btn">
                <?= SVG__ARROW_RIGHT ?>
            </a>
            <div class="round-btn age-limit">12+</div>
        </div>
    </div>
    <div class="img-block">
        <img src="img/spec-project-single-page/img-block.png" alt>
    </div>
    <div class="list-block">
        <div class="content-wrap">
            <div class="block-title">Фильтры:</div>
            <div class="filters-wrap">
                <a href="#" class="item current">Съемки в Новгорое</a>
                <a href="#" class="item">Съемки в области</a>
                <a href="#" class="item">Актеры-новгородцы</a>
                <a href="#" class="item">Фильмы о Новгороде</a>
                <a href="#" class="item">Фильмы о новгородских землях</a>
                <a href="#" class="item">Разное</a>
            </div>
            <div class="list-wrap">
                <div class="item">
                    <a href="#" class="img-wrap">
                        <img src="img/placeholder-295x205.png" alt>
                    </a>
                    <div class="desc-wrap">
                        <a href="#" class="title">Курс на социально-ориентированнный проект</a>
                        <div class="tags-wrap">
                            <div class="tag">Съемки в Новгорое</div>
                            <div class="tag">Съемки в области</div>
                            <div class="tag">Актеры-новгородцы</div>
                        </div>
                        <div class="desc">
                            Курс на социально-ориентированный национальный проект обеспечивает актуальность поэтапного и последовательного развития общества. Сложно сказать, почему интерактивные прототипы лишь добавляют фракционных разногласий и заблокированы в рамках своих собственных рациональных ограничений. С другой стороны, повышение уровня гражданского сознания требует анализа.
                        </div>
                        <a href="#" class="btn alt more-btn">Узнать больше</a>
                    </div>
                </div>
                <div class="item">
                    <a href="#" class="img-wrap">
                        <img src="img/placeholder-295x205.png" alt>
                    </a>
                    <div class="desc-wrap">
                        <a href="#" class="title">Курс на социально-ориентированнный проект</a>
                        <div class="tags-wrap">
                            <div class="tag">Съемки в Новгорое</div>
                            <div class="tag">Съемки в области</div>
                            <div class="tag">Актеры-новгородцы</div>
                        </div>
                        <div class="desc">
                            Курс на социально-ориентированный национальный проект обеспечивает актуальность поэтапного и последовательного развития общества. Сложно сказать, почему интерактивные прототипы лишь добавляют фракционных разногласий и заблокированы в рамках своих собственных рациональных ограничений. С другой стороны, повышение уровня гражданского сознания требует анализа.
                        </div>
                        <a href="#" class="btn alt more-btn">Узнать больше</a>
                    </div>
                </div>
                <div class="item">
                    <a href="#" class="img-wrap">
                        <img src="img/placeholder-295x205.png" alt>
                    </a>
                    <div class="desc-wrap">
                        <a href="#" class="title">Курс на социально-ориентированнный проект</a>
                        <div class="tags-wrap">
                            <div class="tag">Съемки в Новгорое</div>
                            <div class="tag">Съемки в области</div>
                            <div class="tag">Актеры-новгородцы</div>
                        </div>
                        <div class="desc">
                            Курс на социально-ориентированный национальный проект обеспечивает актуальность поэтапного и последовательного развития общества. Сложно сказать, почему интерактивные прототипы лишь добавляют фракционных разногласий и заблокированы в рамках своих собственных рациональных ограничений. С другой стороны, повышение уровня гражданского сознания требует анализа.
                        </div>
                        <a href="#" class="btn alt more-btn">Узнать больше</a>
                    </div>
                </div>
                <div class="item">
                    <a href="#" class="img-wrap">
                        <img src="img/placeholder-295x205.png" alt>
                    </a>
                    <div class="desc-wrap">
                        <a href="#" class="title">Курс на социально-ориентированнный проект</a>
                        <div class="tags-wrap">
                            <div class="tag">Съемки в Новгорое</div>
                            <div class="tag">Съемки в области</div>
                            <div class="tag">Актеры-новгородцы</div>
                        </div>
                        <div class="desc">
                            Курс на социально-ориентированный национальный проект обеспечивает актуальность поэтапного и последовательного развития общества. Сложно сказать, почему интерактивные прототипы лишь добавляют фракционных разногласий и заблокированы в рамках своих собственных рациональных ограничений. С другой стороны, повышение уровня гражданского сознания требует анализа.
                        </div>
                        <a href="#" class="btn alt more-btn">Узнать больше</a>
                    </div>
                </div>
            </div>
            <div class="pagination">
                <a href="#" class="arrow prev">
                    <?= SVG__ARROW_RIGHT ?>
                </a>
                <a href="#" class="page">1</a>
                <a href="#" class="page current">2</a>
                <a href="#" class="page">3</a>
                <a href="#" class="page">4</a>
                <a href="#" class="page">5</a>
                <div class="dots">...</div>
                <a href="#" class="page">9</a>
                <a href="#" class="arrow next">
                    <?= SVG__ARROW_RIGHT ?>
                </a>
            </div>
        </div>
    </div>
</div>

<?php require 'footer.php' ?>