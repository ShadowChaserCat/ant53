<?php $pageTitle = 'Все материалы';

require 'header.php' ?>

<div class="articles-page region-page">
    <div class="content-wrap">
        <div class="page-title">
            <div class="container">
                <p>Отбор по регионам <br><span style="color: #ca9d75;">Российской Федерации </span></p>
                <span class="count">2000</span>
            </div>
        </div>
        <nav class="content-fetch">
            <ul class="content-fetch-items content-wrap">
                <li class="item">Новгородская область</li>
                <li class="item">Псковская область</li>
                <li class="item">Ханты-Мансийский автономный округ</li>
                <li class="item">Ленинградская область</li>
                <li class="item">Московская область</li>
                <li class="item">Архангельская область</li>
            </ul>
        </nav>
        <div class="letters-wrap">
            <a href="#" class="letter">а</a>
            <a href="#" class="letter">б</a>
            <a href="#" class="letter">в</a>
            <a href="#" class="letter">г</a>
            <a href="#" class="letter">д</a>
            <a href="#" class="letter">е</a>
            <a href="#" class="letter">ж</a>
            <a href="#" class="letter">з</a>
            <a href="#" class="letter">и</a>
            <a href="#" class="letter">й</a>
            <a href="#" class="letter">к</a>
            <a href="#" class="letter">л</a>
            <a href="#" class="letter">м</a>
            <a href="#" class="letter">н</a>
            <a href="#" class="letter">о</a>
            <a href="#" class="letter">п</a>
            <a href="#" class="letter">р</a>
            <a href="#" class="letter">с</a>
            <a href="#" class="letter">т</a>
            <a href="#" class="letter">у</a>
            <a href="#" class="letter">ф</a>
            <a href="#" class="letter">х</a>
            <a href="#" class="letter">ц</a>
            <a href="#" class="letter">ч</a>
            <a href="#" class="letter">ш</a>
            <a href="#" class="letter">щ</a>
            <a href="#" class="letter">ы</a>
            <a href="#" class="letter">э</a>
            <a href="#" class="letter">ю</a>
            <a href="#" class="letter">я</a>
        </div>
        <div class="items-grid">
            <a href="#" class="item-link">
                <div class="item-link__img-container">
                    <img src="img/placeholder.png" alt>
                </div>
                <h2  class="title">Новгород</h2>
                <p class="desc">Обзор книги: Кушнир И.И. Новгород. – Л.: Стройиздат, 1967.</p>
            </a>
            <a href="#" class="item-link">
                <div class="item-link__img-container">
                    <img src="img/placeholder.png" alt>
                </div>
                <h2  class="title">Новгород</h2>
                <p class="desc">Обзор книги: Кушнир И.И. Новгород. – Л.: Стройиздат, 1967.</p>
            </a>
            <a href="#" class="item-link">
                <div class="item-link__img-container">
                    <img src="img/placeholder.png" alt>
                </div>
                <h2  class="title">Новгород</h2>
                <p class="desc">Обзор книги: Кушнир И.И. Новгород. – Л.: Стройиздат, 1967.</p>
            </a>
            <a href="#" class="item-link">
                <div class="item-link__img-container">
                    <img src="img/placeholder.png" alt>
                </div>
                <h2  class="title">Новгород</h2>
                <p class="desc">Обзор книги: Кушнир И.И. Новгород. – Л.: Стройиздат, 1967.</p>
            </a>
            <a href="#" class="item-link">
                <div class="item-link__img-container">
                    <img src="img/placeholder.png" alt>
                </div>
                <h2  class="title">Новгород</h2>
                <p class="desc">Обзор книги: Кушнир И.И. Новгород. – Л.: Стройиздат, 1967.</p>
            </a>
            <a href="#" class="item-link">
                <div class="item-link__img-container">
                    <img src="img/placeholder.png" alt>
                </div>
                <h2  class="title">Новгород</h2>
                <p class="desc">Обзор книги: Кушнир И.И. Новгород. – Л.: Стройиздат, 1967.</p>
            </a>
            <a href="#" class="item-link">
                <div class="item-link__img-container">
                    <img src="img/placeholder.png" alt>
                </div>
                <h2  class="title">Новгород</h2>
                <p class="desc">Обзор книги: Кушнир И.И. Новгород. – Л.: Стройиздат, 1967.</p>
            </a>
            <a href="#" class="item-link">
                <div class="item-link__img-container">
                    <img src="img/placeholder.png" alt>
                </div>
                <h2  class="title">Новгород</h2>
                <p class="desc">Обзор книги: Кушнир И.И. Новгород. – Л.: Стройиздат, 1967.</p>
            </a>

        </div>
        <div class="pagination">
            <a href="#" class="arrow prev">
                <?= SVG__ARROW_RIGHT ?>
            </a>
            <a href="#" class="page">1</a>
            <a href="#" class="page current">2</a>
            <a href="#" class="page">3</a>
            <a href="#" class="page">4</a>
            <a href="#" class="page">5</a>
            <div class="dots">...</div>
            <a href="#" class="page">9</a>
            <a href="#" class="arrow next">
                <?= SVG__ARROW_RIGHT ?>
            </a>
        </div>
    </div>
</div>

<?php require 'footer.php' ?>