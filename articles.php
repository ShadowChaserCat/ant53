<?php $pageTitle = 'Все материалы';

require 'header.php' ?>

<div class="articles-page">
    <div class="content-wrap">
        <div class="page-title">
            <div class="container">
                <p>Все <span class="page-title__text__brown">материалы</span></p>
                <div class="count">3000</div>
            </div>
        </div>
        <section class="filters">
            <article>
                <a href="#" class="filters__link filters__link_brown">Отбор по темам</a>
            </article>
            <article>
                <a href="#" class="filters__link filters__link__white-black">Отбор по районам Новгородской области</a>
            </article>
            <article>
                <a href="#" class="filters__link filters__link__white">Отбор по регионам Российской Федерации</a>
            </article>
            <article>
                <a href="#" class="filters__link filters__link__black ">Отбор по странам мира</a>
            </article>
        </section>
        <div class="items-grid">
            <a href="#" class="item-link">
                <div class="item-link__img-container">
                    <img src="img/placeholder.png" alt>
                </div>
                <h2  class="title">Новгород</h2>
                <p class="desc">Обзор книги: Кушнир И.И. Новгород. – Л.: Стройиздат, 1967.</p>
            </a>
            <a href="#" class="item-link">
                <div class="item-link__img-container">
                    <img src="img/placeholder.png" alt>
                </div>
                <h2  class="title">Новгород</h2>
                <p class="desc">Обзор книги: Кушнир И.И. Новгород. – Л.: Стройиздат, 1967.</p>
            </a>
            <a href="#" class="item-link">
                <div class="item-link__img-container">
                    <img src="img/placeholder.png" alt>
                </div>
                <h2  class="title">Новгород</h2>
                <p class="desc">Обзор книги: Кушнир И.И. Новгород. – Л.: Стройиздат, 1967.</p>
            </a>
            <a href="#" class="item-link">
                <div class="item-link__img-container">
                    <img src="img/placeholder.png" alt>
                </div>
                <h2  class="title">Новгород</h2>
                <p class="desc">Обзор книги: Кушнир И.И. Новгород. – Л.: Стройиздат, 1967.</p>
            </a>
            <a href="#" class="item-link">
                <div class="item-link__img-container">
                    <img src="img/placeholder.png" alt>
                </div>
                <h2  class="title">Новгород</h2>
                <p class="desc">Обзор книги: Кушнир И.И. Новгород. – Л.: Стройиздат, 1967.</p>
            </a>
            <a href="#" class="item-link">
                <div class="item-link__img-container">
                    <img src="img/placeholder.png" alt>
                </div>
                <h2  class="title">Новгород</h2>
                <p class="desc">Обзор книги: Кушнир И.И. Новгород. – Л.: Стройиздат, 1967.</p>
            </a>
            <a href="#" class="item-link">
                <div class="item-link__img-container">
                    <img src="img/placeholder.png" alt>
                </div>
                <h2  class="title">Новгород</h2>
                <p class="desc">Обзор книги: Кушнир И.И. Новгород. – Л.: Стройиздат, 1967.</p>
            </a>
            <a href="#" class="item-link">
                <div class="item-link__img-container">
                    <img src="img/placeholder.png" alt>
                </div>
                <h2  class="title">Новгород</h2>
                <p class="desc">Обзор книги: Кушнир И.И. Новгород. – Л.: Стройиздат, 1967.</p>
            </a>

        </div>
        <div class="pagination">
            <a href="#" class="arrow prev">
                <?= SVG__ARROW_RIGHT ?>
            </a>
            <a href="#" class="page">1</a>
            <a href="#" class="page current">2</a>
            <a href="#" class="page">3</a>
            <a href="#" class="page">4</a>
            <a href="#" class="page">5</a>
            <div class="dots">...</div>
            <a href="#" class="page">9</a>
            <a href="#" class="arrow next">
                <?= SVG__ARROW_RIGHT ?>
            </a>
        </div>
    </div>
</div>

<?php require 'footer.php' ?>