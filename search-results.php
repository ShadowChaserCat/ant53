<?php $pageTitle = 'Результаты поиска';

require 'header.php' ?>

<div class="search-page">
    <div class="content-wrap">
        <div class="page-title">Результаты поиска</div>
        <form method="post" class="search-wrap">
            <input type="text" name="search" placeholder="Ввести новый запрос">
            <button type="submit" class="btn alt">Найти</button>
        </form>
        <div class="results-title">Результаты по запросу: «...»</div>
        <div class="items-grid">
            <a href="#" class="item-link">
                <div class="item-link__img-container">
                    <img src="img/placeholder.png" alt>
                </div>
                <h2  class="title">Новгород</h2>
                <p class="desc">Обзор книги: Кушнир И.И. Новгород. – Л.: Стройиздат, 1967.</p>
            </a>
            <a href="#" class="item-link">
                <div class="item-link__img-container">
                    <img src="img/placeholder.png" alt>
                </div>
                <h2  class="title">Новгород</h2>
                <p class="desc">Обзор книги: Кушнир И.И. Новгород. – Л.: Стройиздат, 1967.</p>
            </a>
            <a href="#" class="item-link">
                <div class="item-link__img-container">
                    <img src="img/placeholder.png" alt>
                </div>
                <h2  class="title">Новгород</h2>
                <p class="desc">Обзор книги: Кушнир И.И. Новгород. – Л.: Стройиздат, 1967.</p>
            </a>
            <a href="#" class="item-link">
                <div class="item-link__img-container">
                    <img src="img/placeholder.png" alt>
                </div>
                <h2  class="title">Новгород</h2>
                <p class="desc">Обзор книги: Кушнир И.И. Новгород. – Л.: Стройиздат, 1967.</p>
            </a>
            <a href="#" class="item-link">
                <div class="item-link__img-container">
                    <img src="img/placeholder.png" alt>
                </div>
                <h2  class="title">Новгород</h2>
                <p class="desc">Обзор книги: Кушнир И.И. Новгород. – Л.: Стройиздат, 1967.</p>
            </a>
            <a href="#" class="item-link">
                <div class="item-link__img-container">
                    <img src="img/placeholder.png" alt>
                </div>
                <h2  class="title">Новгород</h2>
                <p class="desc">Обзор книги: Кушнир И.И. Новгород. – Л.: Стройиздат, 1967.</p>
            </a>
            <a href="#" class="item-link">
                <div class="item-link__img-container">
                    <img src="img/placeholder.png" alt>
                </div>
                <h2  class="title">Новгород</h2>
                <p class="desc">Обзор книги: Кушнир И.И. Новгород. – Л.: Стройиздат, 1967.</p>
            </a>
            <a href="#" class="item-link">
                <div class="item-link__img-container">
                    <img src="img/placeholder.png" alt>
                </div>
                <h2  class="title">Новгород</h2>
                <p class="desc">Обзор книги: Кушнир И.И. Новгород. – Л.: Стройиздат, 1967.</p>
            </a>

        </div>
        <div class="pagination">
            <a href="#" class="arrow prev">
                <?= SVG__ARROW_RIGHT ?>
            </a>
            <a href="#" class="page">1</a>
            <a href="#" class="page current">2</a>
            <a href="#" class="page">3</a>
            <a href="#" class="page">4</a>
            <a href="#" class="page">5</a>
            <div class="dots">...</div>
            <a href="#" class="page">9</a>
            <a href="#" class="arrow next">
                <?= SVG__ARROW_RIGHT ?>
            </a>
        </div>
    </div>
</div>

<?php require 'footer.php' ?>