<?php $pageTitle = 'Главная';

require 'header.php' ?>

<div class="mainpage">
    <div class="block-1">
        <div class="content-wrap">
            <div class="title">
                Цифровая<br>
                библиотека
            </div>
            <img src="img/mainpage/svg/block-1-subtitle.svg" alt class="subtitle">
            <img src="img/mainpage/block-1-img.png" alt class="block-image">
            <form method="post" class="mainpage-search content-wrap">
                <input type="text" class="mainpage-search__input" name="search" placeholder="Введите свой запрос">
                <button type="submit" class="mainpage-search__button">Найти</button>
            </form>
        </div>
    </div>

    <div class="running-text first"></div>
    <div class="running-text second"></div>

    <div class="block-3">
        <div class="content-wrap">
            <div class="block-title">
                <p>Форматы</p>
                <p>материалов</p>
            </div>
            <div class="items-wrap">
                <a href="#" class="item">
                    Места, события, люди
                    <span class="count">62872</span>
                </a>
                <a href="#" class="item">
                    Газеты
                    <span class="count">159</span>
                </a>
                <a href="#" class="item">
                    Фотографии
                    <span class="count">25367</span>
                </a>
                <a href="#" class="item">
                    Аудиозаписи
                    <span class="count">19276</span>
                </a>
                <a href="#" class="item">
                    Видеофайлы
                    <span class="count">83</span>
                </a>
                <a href="#" class="item">
                    Библиография
                    <span class="count">45</span>
                </a>
                <a href="#" class="item">
                    Обзоры книг
                    <span class="count">4366</span>
                </a>
                <a href="#" class="item">
                    Каталог ссылок
                    <span class="count">4782</span>
                </a>
            </div>
            <img src="img/mainpage/block-3-img-1.png" alt class="hover-image i1">
            <img src="img/mainpage/block-3-img-2.png" alt class="hover-image i2">
            <img src="img/mainpage/block-3-img-3.png" alt class="hover-image i3">
            <img src="img/mainpage/block-3-img-4.png" alt class="hover-image i4">
            <img src="img/mainpage/block-3-img-5.png" alt class="hover-image i5">
            <img src="img/mainpage/block-3-img-6.png" alt class="hover-image i6">
            <img src="img/mainpage/block-3-img-7.png" alt class="hover-image i7">
            <img src="img/mainpage/block-3-img-8.png" alt class="hover-image i8">
        </div>
    </div>

    <div class="block-4">
        <div class="content-wrap">
            <div class="block-title">
                <span>Все</span>
                материалы
            </div>
            <div class="items-wrap">
                <a href="#" class="item">
                    <div class="title">
                        <img src="img/mainpage/svg/block-4-icon-1.svg" alt>
                        По темам
                    </div>
                </a>
                <a href="#" class="item">
                    <div class="title">
                        <img src="img/mainpage/svg/block-4-icon-2.svg" alt>
                        По районам
                    </div>
                </a>
                <a href="#" class="item">
                    <div class="title">
                        <img src="img/mainpage/svg/block-4-icon-3.svg" alt>
                        Свой поиск
                    </div>
                </a>
            </div>
        </div>
    </div>

    <div class="block-5">
        <div class="content-wrap">
            <div class="block-title">
                <p>Последние</p>
                <p>обновления</p>
            </div>
            <div class="swiper last-update-swiper">
                <div class="swiper-wrapper">
                    <a href="#" class="swiper-slide">
                        <div class="swiper-img-container">
                            <img src="img/mainpage/block-5-img-1.png" alt>
                        </div>
                        <div class="title">Заголовок</div>
                        <div class="desc">Краткое описание текст примерно на одно-два предложения</div>
                    </a>
                    <a href="#" class="swiper-slide">
                        <div class="swiper-img-container">
                            <img src="img/mainpage/block-5-img-2.png" alt>
                        </div>
                        <div class="title">Заголовок</div>
                        <div class="desc">Краткое описание текст примерно на одно-два предложения</div>
                    </a>
                    <a href="#" class="swiper-slide">

                        <div class="swiper-img-container">
                            <img src="img/mainpage/block-5-img-3.png" alt>
                        </div>
                        <div class="title">Заголовок</div>
                        <div class="desc">Краткое описание текст примерно на одно-два предложения</div>
                    </a>
                    <a href="#" class="swiper-slide">
                        <div class="swiper-img-container">
                            <img src="img/mainpage/block-5-img-1.png" alt>
                        </div>
                        <div class="title">Заголовок</div>
                        <div class="desc">Краткое описание текст примерно на одно-два предложения</div>
                    </a>
                    <a href="#" class="swiper-slide">
                        <div class="swiper-img-container">
                            <img src="img/mainpage/block-5-img-2.png" alt>
                        </div>
                        <div class="title">Заголовок</div>
                        <div class="desc">Краткое описание текст примерно на одно-два предложения</div>
                    </a>
                    <a href="#" class="swiper-slide">
                        <div class="swiper-img-container">
                            <img src="img/mainpage/block-5-img-3.png" alt>
                        </div>
                        <div class="title">Заголовок</div>
                        <div class="desc">Краткое описание текст примерно на одно-два предложения</div>
                    </a>
                </div>
                <div class="swiper-scrollbar"></div>
            </div>
            <div class="btns-wrap">
                <a href="#" class="round-btn prev">
                    <?= SVG__ARROW_RIGHT ?>
                </a>
                <a href="#" class="round-btn next">
                    <?= SVG__ARROW_RIGHT ?>
                </a>
            </div>
        </div>
        <img src="img/mainpage/block-5-bg.png" alt class="bg">
    </div>

    <div class="block-6">
        <div class="swiper">
            <div class="swiper-wrapper">
                <a href="#" class="swiper-slide swiper-slide__second">
                    <div class="content-wrap">
                        <div class="title">Спецпроекты</div>
                        <div class="project-name">
                            <p>Впервые в Новгороде</p>
                            <p class="italic _48px">Впервые в россии</p>
                        </div>
                    </div>
                </a>
                <a href="#" class="swiper-slide swiper-slide__third">
                    <div class="content-wrap">
                        <div class="title">Спецпроекты</div>
                        <div class="project-name">
                            <p>Три имени</p>
                            <p class="italic _48px">изменивших мир</p>
                        </div>
                    </div>
                </a>
                <a href="#" class="swiper-slide swiper-slide__fourth">
                    <div class="content-wrap">
                        <div class="title">Спецпроекты</div>
                        <div class="project-name">
                            <p>Memorial</p>
                            <p class="italic _48px">Великий Новгород</p>
                        </div>
                    </div>
                </a>
                <a href="#" class="swiper-slide swiper-slide__fifth">
                    <div class="content-wrap">
                        <div class="title">Спецпроекты</div>
                        <div class="project-name">
                            <p>Новгород</p>
                            <p class="italic _48px">литературный</p>
                        </div>
                    </div>
                </a>
                <a href='#' class="swiper-slide swiper-slide__sixth">
                    <div class="content-wrap">
                        <div class="title">Спецпроекты</div>
                        <div class="project-name">
                            <p>Новгород</p>
                            <p class="italic _48px">в кадре и за кадром</p>
                        </div>
                    </div>
                </a>
                <a href='#' class="swiper-slide swiper-slide__seventh">
                    <div class="content-wrap">
                        <div class="title">Спецпроекты</div>
                        <div class="project-name">
                            <p>Новгородские имена</p>
                            <p class="italic _48px">на карте мира</p>
                        </div>
                    </div>
                </a>
                <a href='#' class="swiper-slide swiper-slide__eighth">
                    <div class="content-wrap">
                        <div class="title">Спецпроекты</div>
                        <div class="project-name">
                            <p>Новгород</p>
                            <p class="italic _48px">в цифрах и фактах</p>
                        </div>
                    </div>
                </a>
            </div>
            <div class="swiper-pagination"></div>
            <a href="#" class="round-btn prev">
                <?= SVG__ARROW_RIGHT ?>
            </a>
            <a href="#" class="round-btn next">
                <?= SVG__ARROW_RIGHT ?>
            </a>
        </div>
    </div>

    <div class="block-7">
        <div class="content-wrap">
            <div class="block-title">
                <p>Информация</p>
                <p>о сайте</p>
            </div>
            <div class="items-wrap">
                <div class="item">
                    <div class="title">
                        Кто создает материалы сайта?
                        <?= SVG__ARROW_TOP_RIGHT ?>
                    </div>
                    <div class="desc-wrap">
                        <div class="desc">
                            <p>Основными контент-менеджерами выступают библиотекари из различных библиотечных систем Новгородской области. Ответственные за ресурс: библиотечная система «Читай-город» из Великого Новгорода и новгородская некоммерческая организация «Муравейник» (также созданная библиотекарями).</p>
                            <p>Активное участие в пополнении ресурса принимают наши информационные партнеры – отраслевые организации различной направленности, размещающие свои служебные архивы; новгородские СМИ; отдельные краеведы-энтузиасты из самых разных уголков нашего региона и многие другие хорошие люди и структуры.</p>
                            <p>В создании многих материалов сайта участвуют жители других регионов и даже стран – в том случае, если материал связан и с нашей новгородской, и с их историей.</p>
                        </div>
                        <div class="links-wrap">
                            <a href="#">Авторы материалов</a>
                            <a href="#">Организации-партнеры</a>
                        </div>
                    </div>
                </div>
                <div class="item">
                    <div class="title">
                        Насколько достоверен этот ресурс?
                        <?= SVG__ARROW_TOP_RIGHT ?>
                    </div>
                    <div class="desc-wrap">
                        <div class="desc">
                            <p>Материалы создаются на основе печатных первоисточников различной степени давности и тщательно выверяются. «Всех подряд» мы в контент-менеджеры не берем – даже из среды библиотекарей с опытом, которые являются профессионалами по работе с информацией по умолчанию.</p>
                            <p>Все материалы проходят модерацию. «При сайте» эффективно работает группа «Новгородский край в эпизодах и лицах» в социальной сети «ВКонтакте» – своего рода общественный контроль. Аудитория группы активная, требовательная и сведущая в краеведческих темах – ошибки, если таковые случаются, выявляются моментально!</p>
                        </div>
                        <div class="links-wrap">
                            <a href="https://vk.com/53ant" class="brown">Вступить в группу во «ВКонтакте»</a>
                        </div>
                    </div>
                </div>
                <div class="item">
                    <div class="title">
                        Есть ли возрастные ограничения?
                        <?= SVG__ARROW_TOP_RIGHT ?>
                    </div>
                    <div class="desc-wrap">
                        <div class="desc">
                            <p>Согласно положениям Федерального закона Российской Федерации от 29 декабря 2010 г. № 436-ФЗ «О защите детей от информации, причиняющей вред их здоровью и развитию», материалы сайта имеют возрастные метки 0+, 6+, 12+, 16+ и 18+. Просим обращать внимание на указанное возрастное ограничение в каждом конкретном материале!</p>
                        </div>
                    </div>
                </div>
                <div class="item">
                    <div class="title">
                        Мне обязательно регистрироваться?
                        <?= SVG__ARROW_TOP_RIGHT ?>
                    </div>
                    <div class="desc-wrap">
                        <div class="desc">
                            <p>Нет. Все материалы сайта доступны без регистрации – за исключением познавательно-развлекательного раздела «Тесты».</p>
                            <p>Привилегия зарегистрированных пользователей – проходить тесты и иметь личный кабинет, в котором можно:</p>
                            <ul>
                                <li>сохранять понравившиеся материалы (чтобы не искать их заново);</li>
                                <li>получать уведомления о новых материалах сайта, появившихся за время их отсутствия.</li>
                            </ul>
                            <p>P.S. от админов сайта: мы безмерно благодарны тем, кто нашел время и зарегистрировался – тем самым вы помогли сайту, увеличив его статистику (которая идет в официальные отчеты новгородских библиотек). Вы – восхитительны!</p>
                        </div>
                    </div>
                </div>
                <div class="item">
                    <div class="title">
                        Могу я копировать ваши материалы?
                        <?= SVG__ARROW_TOP_RIGHT ?>
                    </div>
                    <div class="desc-wrap">
                        <div class="desc">
                            <p>Без проблем. Все, что здесь размещается – делается для людей и является общественным достоянием. Однако душевно просим – при использовании ссылаться на наш сайт и указывать, что материал Вы нашли именно здесь.</p>
                            <p>Это будет знаком уважения не только к нашему труду и вкладу наших информационных партнеров – но и к тем, кто писал книги и статьи, послужившие первоисточниками для наших материалов. Мы, в свою очередь, тщательно указываем первоисточники в своих статьях. Будем взаимно вежливы – мир станет лучше и добрее!</p>
                        </div>
                    </div>
                </div>
                <div class="item">
                    <div class="title">
                        Хочу добавить на сайт свои файлы
                        <?= SVG__ARROW_TOP_RIGHT ?>
                    </div>
                    <div class="desc-wrap">
                        <div class="desc">
                            <p>Прекрасное решение, за которое мы Вам очень благодарны! Вам нужно:</p>
                            <ul>
                                <li> <p>пройти <a href="#" class="desc-reg__a">регистрацию</a>;</p></li>
                                <li> <p>выслать нам все, что вы хотите разместить, на почту <a  href="mailto:info@ant53.ru" class="desc-reg__a">info@ant53.ru</a></p></li>
                            </ul>
                            <p>Просим в письме указать ссылку на Ваш профиль – скопируйте из адресной строки в своем личном кабинете, который будет доступен Вам после регистрации.</p>
                            <p>Обращаем Ваше внимание, что присланные материалы должны принадлежать лично Вам – мы не размещаем файлы, скачанные из сети.</p>
                            <p>Желательно прокомментировать присланные материалы: что это, когда и кем создано, чему посвящено – пишите все, что считаете нужным. Это существенно облегчит работу модераторов по проверке присланных данных. Будем рады видеть Вас в рядах наших авторов!</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="block-8">
        <div class="content-wrap">
            <div class="block-title">Партнеры</div>
            <div class="items-wrap">
                <a href="#" class="item">
                    <div class="title">СМИ</div>
                    <div class="bg-icon">
                        <img src="img/mainpage/svg/block-8-icon-1.svg" alt>
                    </div>
                </a>
                <a href="#" class="item">
                    <div class="title">Культура</div>
                    <div class="bg-icon">
                        <img src="img/mainpage/svg/block-8-icon-2.svg" alt>
                    </div>
                </a>
                <a href="#" class="item">
                    <div class="title">Образование</div>
                    <div class="bg-icon">
                        <img src="img/mainpage/svg/block-8-icon-3.svg" alt>
                    </div>
                </a>
                <a href="#" class="item">
                    <div class="title">Некоммерческий сектор</div>
                    <div class="bg-icon">
                        <img src="img/mainpage/svg/block-8-icon-4.svg" alt>
                    </div>
                </a>
                <a href="#" class="item">
                    <div class="title">Отраслевые организации</div>
                    <div class="bg-icon">
                        <img src="img/mainpage/svg/block-8-icon-5.svg" alt>
                    </div>
                </a>
                <a href="#" class="item">
                    <div class="title">Органы власти</div>
                    <div class="bg-icon">
                        <img src="img/mainpage/svg/block-8-icon-6.svg" alt>
                    </div>
                </a>
            </div>
        </div>
    </div>

    <div class="block-9">
        <div class="content-wrap">
            <div class="block-title">Связаться <span>с нами:</span></div>
            <div class="subtitle">Среднее время ответа – 5 рабочих дней, ввиду загруженности администраторов. Спасибо за понимание!</div>
            <form method="post" class="form-wrap">
                <div class="input-wrap">
                    <input type="text" name="firstname" placeholder="Имя">
                </div>
                <div class="input-wrap">
                    <input type="text" name="phone" placeholder="Телефон/email">
                </div>
                <div class="input-wrap">
                    <input type="text" name="question" placeholder="Вопрос">
                </div>
                <div class="input-wrap captcha">
                    <div class="captcha-wrap"></div>
                    <input type="text" name="captcha" placeholder="Введите текст с картинки">
                </div>
                <label class="check-wrap">
                    <input type="checkbox" name="personal" required>
                    <span class="check">
                        <img src="img/svg/check.svg" alt>
                    </span>
                    <span class="text">
                        Я принимаю условия <a href="#">политики конфиденциальности</a>
                    </span>
                </label>
                <button type="submit" class="btn">Отправить</button>
                <div class="contact-us">
                    Если вы не получили ответ в указанные сроки - пожалуйста, напишите нам на почту <a  href="mailto:info@ant53.ru" class="contact-us__a">info@ant53.ru</a> или позвоните по номеру <a href="tel:+78162631710 " class="contact-us__a">8 (816 2)63 17 10</a>. Возможно, мы не получили Ваше обращение по техническим причинам.
                </div>
            </form>
        </div>
    </div>

    <div class="block-10">
        <div class="content-wrap">
            <div class="p1">Мы</div>
            <div class="p2">вконтакте</div>
            <a href="https://vk.com/53ant" class="round-btn">
                <?= SVG__VK_ICON ?>
            </a>
        </div>
    </div>
</div>

<?php require 'footer.php' ?>