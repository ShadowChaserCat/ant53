<?php $pageTitle = 'Карточка библиографического списка';

require 'header.php' ?>

<div class="biblio-single-page">
    <div class="info-block" style="background-image: url(img/card-page/info-block-bg.png);">
        <div class="content-wrap">
            <div class="title">Андреев</div>
            <div class="subtitle">Александр Алексеевич</div>
            <div class="years">1921 - 1979</div>
            <div class="desc-wrap">
                <div class="img-wrap">
                    <img src="img/card-page/info-block-img.png" alt>
                </div>
                <div class="desc">
                    <p>
                        Уроженец Новгородской губернии (д. Филиппково).<br>
                        До и после Великой Отечественной войны жил<br>
                        на Новгородчине. Родственники героя живут в Великом Новгороде.
                    </p>
                    <p>Родился 16 марта 1911 года в деревне Филиппково Новгородской губернии (на тот момент деревня предположительно относилась к Любынскому сельсовету</p>
                    <p>Родился 16 марта 1911 года в деревне Филиппково Новгородской губернии (на тот момент деревня предположительно относилась к Любынскому сельсовету</p>
                    <p>Родился 16 марта 1911 года в деревне Филиппково Новгородской губернии (на тот момент деревня предположительно относилась к Любынскому сельсовету</p>
                </div>
            </div>
            <a href="#" class="round-btn back-btn">
                <?= SVG__ARROW_RIGHT ?>
            </a>
            <a href="#" class="round-btn fav-btn">
                <?= SVG__HEART ?>
            </a>
            <div class="round-btn age-limit">12+</div>
        </div>
    </div>
    <div class="content-wrap">
        <div class="books-wrap">
            <div class="block-title">Книги по теме</div>
            <div class="items-grid">
                <div class="item">
                    <a href="#" class="img-wrap">
                        <img src="img/placeholder.png" alt>
                    </a>
                    <a href="#" class="title">Новгород</a>
                    <div class="desc">Обзор книги: Кушнир И.И. Новгород. – Л.: Стройиздат, 1967.</div>
                </div>
                <div class="item">
                    <a href="#" class="img-wrap">
                        <img src="img/placeholder.png" alt>
                    </a>
                    <a href="#" class="title">Новгород</a>
                    <div class="desc">Обзор книги: Кушнир И.И. Новгород. – Л.: Стройиздат, 1967.</div>
                </div>
                <div class="item">
                    <a href="#" class="img-wrap">
                        <img src="img/placeholder.png" alt>
                    </a>
                    <a href="#" class="title">Новгород</a>
                    <div class="desc">Обзор книги: Кушнир И.И. Новгород. – Л.: Стройиздат, 1967.</div>
                </div>
                <div class="item">
                    <a href="#" class="img-wrap">
                        <img src="img/placeholder.png" alt>
                    </a>
                    <a href="#" class="title">Новгород</a>
                    <div class="desc">Обзор книги: Кушнир И.И. Новгород. – Л.: Стройиздат, 1967.</div>
                </div>
            </div>
            <div class="pagination">
                <a href="#" class="arrow prev">
                    <?= SVG__ARROW_RIGHT ?>
                </a>
                <a href="#" class="page">1</a>
                <a href="#" class="page current">2</a>
                <a href="#" class="page">3</a>
                <a href="#" class="page">4</a>
                <a href="#" class="page">5</a>
                <div class="dots">...</div>
                <a href="#" class="page">9</a>
                <a href="#" class="arrow next">
                    <?= SVG__ARROW_RIGHT ?>
                </a>
            </div>
        </div>
        <div class="articles-wrap">
            <div class="block-title">Все публикации автора</div>
            <div class="tags-list">
                <a href="#" class="tag current">История и культура</a>
                <a href="#" class="tag">Художественная самодеятельность</a>
                <a href="#" class="tag">Музыка</a>
                <a href="#" class="tag">Танец(хореография)</a>
                <a href="#" class="tag">Промышленные предприятия</a>
                <a href="#" class="tag">Промышленность</a>
                <a href="#" class="tag">Промышленность советского периода</a>
                <a href="#" class="tag">Культурно-досуговая деятельность</a>
            </div>
            <div class="articles-list">
                <div class="item">
                    <?= SVG__ARROW_TOP_RIGHT ?>
                    <a href="#" class="title">
                        Текст-рыба - пример вывода публикайи - Предприниматели в сети интернет в равной степени предоставлены сами себе. А также многие известные личности объективно рассмотрены соответствующими инстанциями
                    </a>
                    <div class="desc">
                        Аннотация - раскрытие - Предприниматели в сети интернет в равной степени предоставлены сами себе. А также многие известные личности объективно рассмотрены соответствующими инстанциями
                    </div>
                </div>
                <div class="item">
                    <?= SVG__ARROW_TOP_RIGHT ?>
                    <a href="#" class="title">
                        Текст-рыба - пример вывода публикайи - Предприниматели в сети интернет в равной степени предоставлены сами себе. А также многие известные личности объективно рассмотрены соответствующими инстанциями
                    </a>
                    <div class="desc">
                        Аннотация - раскрытие - Предприниматели в сети интернет в равной степени предоставлены сами себе. А также многие известные личности объективно рассмотрены соответствующими инстанциями
                    </div>
                </div>
                <div class="item">
                    <a href="#" class="title">
                        Текст-рыба - пример вывода публикайи - Предприниматели в сети интернет в равной степени предоставлены сами себе. А также многие известные личности объективно рассмотрены соответствующими инстанциями
                    </a>
                </div>
                <div class="item">
                    <a href="#" class="title">
                        Текст-рыба - пример вывода публикайи - Предприниматели в сети интернет в равной степени предоставлены сами себе. А также многие известные личности объективно рассмотрены соответствующими инстанциями
                    </a>
                </div>
                <div class="item">
                    <a href="#" class="title">
                        Текст-рыба - пример вывода публикайи - Предприниматели в сети интернет в равной степени предоставлены сами себе. А также многие известные личности объективно рассмотрены соответствующими инстанциями
                    </a>
                </div>
                <div class="item">
                    <a href="#" class="title">
                        Текст-рыба - пример вывода публикайи - Предприниматели в сети интернет в равной степени предоставлены сами себе. А также многие известные личности объективно рассмотрены соответствующими инстанциями
                    </a>
                </div>
                <div class="item">
                    <a href="#" class="title">
                        Текст-рыба - пример вывода публикайи - Предприниматели в сети интернет в равной степени предоставлены сами себе. А также многие известные личности объективно рассмотрены соответствующими инстанциями
                    </a>
                </div>
                <div class="item">
                    <a href="#" class="title">
                        Текст-рыба - пример вывода публикайи - Предприниматели в сети интернет в равной степени предоставлены сами себе. А также многие известные личности объективно рассмотрены соответствующими инстанциями
                    </a>
                </div>
                <div class="item">
                    <a href="#" class="title">
                        Текст-рыба - пример вывода публикайи - Предприниматели в сети интернет в равной степени предоставлены сами себе. А также многие известные личности объективно рассмотрены соответствующими инстанциями
                    </a>
                </div>
                <div class="item">
                    <a href="#" class="title">
                        Текст-рыба - пример вывода публикайи - Предприниматели в сети интернет в равной степени предоставлены сами себе. А также многие известные личности объективно рассмотрены соответствующими инстанциями
                    </a>
                </div>
            </div>
            <div class="pagination">
                <a href="#" class="arrow prev">
                    <?= SVG__ARROW_RIGHT ?>
                </a>
                <a href="#" class="page">1</a>
                <a href="#" class="page current">2</a>
                <a href="#" class="page">3</a>
                <a href="#" class="page">4</a>
                <a href="#" class="page">5</a>
                <div class="dots">...</div>
                <a href="#" class="page">9</a>
                <a href="#" class="arrow next">
                    <?= SVG__ARROW_RIGHT ?>
                </a>
            </div>
            <a href="#" class="btn alt">Скачать весь список</a>
        </div>
    </div>
</div>

<?php require 'footer.php' ?>