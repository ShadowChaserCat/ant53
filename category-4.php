<?php $pageTitle = 'Географические рубрики - по районам';

require 'header.php' ?>

<div class="category-page">
    <div class="content-wrap">
        <div class="page-title">
            <p>Географические рубрики</p>
            <p>по районам</p>
            <a href="#" class="round-btn back-btn">
                <?= SVG__ARROW_RIGHT ?>
            </a>
            <div class="round-btn age-limit">12+</div>
        </div>
    </div>
    <div class="img-block"></div>
    <div class="category-block">
        <div class="content-wrap">
            <div class="subcategories-wrap">
                <div class="title">Выберите рубрику:</div>
                <div class="list">
                    <a href="#" class="item">Новгородская область</a>
                    <a href="#" class="item">Великий Новгород и Новгородский р-н</a>
                    <a href="#" class="item">Батецкий и Батецкий р-н</a>
                    <a href="#" class="item">Боровичи и Боровичский р-н</a>
                    <a href="#" class="item">Валдай и Валдайский р-н</a>
                    <a href="#" class="item">Волот и Волотовский р-н</a>
                    <a href="#" class="item">Демянск и Демянский р-н</a>
                    <a href="#" class="item">Крестцы и Крестецкий р-н</a>
                    <a href="#" class="item">Любытино и Любытинский р-н</a>
                    <a href="#" class="item">Малая Вишера и Маловишерский р-н</a>
                    <a href="#" class="item">Марёво и Маревский р-н</a>
                    <a href="#" class="item">Мошенское и Мошенской р-н</a>
                    <a href="#" class="item">Окуловка и Окуловский р-н</a>
                    <a href="#" class="item">Парфино и Парфинский р-н</a>
                    <a href="#" class="item">Пестово и Пестовский р-н</a>
                    <a href="#" class="item">Поддорье и Поддорский р-н</a>
                    <a href="#" class="item">Сольцы и Солецкий р-н</a>
                    <a href="#" class="item">Старая Русса и Старорусский р-н</a>
                    <a href="#" class="item">Хвойная и Хвойнинский р-н</a>
                    <a href="#" class="item">Холм и Холмский р-н</a>
                    <a href="#" class="item">Чудово и Чудовский р-н</a>
                    <a href="#" class="item">Шимск и Шимский р-н</a>
                </div>
            </div>
            <div class="block-title">
                <p>Последние</p>
                <p>обновления</p>
            </div>
            <div class="swiper">
                <div class="swiper-wrapper">
                    <div class="swiper-slide">
                        <img src="img/mainpage/block-5-img-1.png" alt>
                        <div class="title">Заголовок</div>
                        <div class="desc">Краткое описание текст примерно на одно-два предложения</div>
                    </div>
                    <div class="swiper-slide">
                        <img src="img/mainpage/block-5-img-2.png" alt>
                        <div class="title">Заголовок</div>
                        <div class="desc">Краткое описание текст примерно на одно-два предложения</div>
                    </div>
                    <div class="swiper-slide">
                        <img src="img/mainpage/block-5-img-3.png" alt>
                        <div class="title">Заголовок</div>
                        <div class="desc">Краткое описание текст примерно на одно-два предложения</div>
                    </div>
                    <div class="swiper-slide">
                        <img src="img/mainpage/block-5-img-1.png" alt>
                        <div class="title">Заголовок</div>
                        <div class="desc">Краткое описание текст примерно на одно-два предложения</div>
                    </div>
                    <div class="swiper-slide">
                        <img src="img/mainpage/block-5-img-2.png" alt>
                        <div class="title">Заголовок</div>
                        <div class="desc">Краткое описание текст примерно на одно-два предложения</div>
                    </div>
                    <div class="swiper-slide">
                        <img src="img/mainpage/block-5-img-3.png" alt>
                        <div class="title">Заголовок</div>
                        <div class="desc">Краткое описание текст примерно на одно-два предложения</div>
                    </div>
                </div>
                <div class="swiper-scrollbar"></div>
            </div>
            <div class="btns-wrap">
                <a href="#" class="round-btn prev">
                    <?= SVG__ARROW_RIGHT ?>
                </a>
                <a href="#" class="round-btn next">
                    <?= SVG__ARROW_RIGHT ?>
                </a>
            </div>
        </div>
    </div>
</div>

<?php require 'footer.php' ?>