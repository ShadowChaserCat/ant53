const itemLink = (link, src, title, description) => {
    return `<a href="${link}" class="item-link">
                <div class="item-link__img-container">
                    <img src="${src}" alt="Проблемы с интернетом">
                </div>
                <h2  class="title">${title}</h2>
                <p class="desc">${description}</p>
            </a>`
}

const fetchPage = async () => {

    let Filter = 'Rayon';
    let pageFilter = new FormData();
    pageFilter.append('Filter', Filter);

    try {
        let response = await fetch('https://fakestoreapi.com/products?limit=12', {
            method: "GET",
        });
        let result = await response.json();

        console.log(result);

        await document.querySelectorAll('.item-link').forEach(el => {
            el.remove();
        })


        result.forEach(item => {
            let itemHTML = itemLink( item.id , item.image, item.price, item.title);
            document.querySelector('.items-grid').insertAdjacentHTML('beforeend', itemHTML)
        })

    } catch (err) {
        console.log(err);
        console.log(err.code);
        console.log('Error')
    }
}

$('.articles-page').ready( () => {
    if (['/articles.php', '/country.php' , '/theme.php' , '/region.php' , '/rayon.php' ].includes(location.pathname)) {
        $(document).bind('load', fetchPage())
    }
})
