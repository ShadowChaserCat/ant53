let map,
    mapCenter = [58.490554, 32.498183],
    mapZoom = window.innerWidth > 1199 ? 8 : 6,
    mapType,
    iconX = '<svg width="10" height="10" viewBox="0 0 10 10" fill="none" xmlns="http://www.w3.org/2000/svg"><path opacity="0.5" d="M6.17387 4.99929L9.75374 1.42723C9.91051 1.27044 9.99858 1.05779 9.99858 0.836054C9.99858 0.614319 9.91051 0.401665 9.75374 0.244875C9.59697 0.0880841 9.38435 0 9.16264 0C8.94094 0 8.72832 0.0880841 8.57155 0.244875L5 3.82526L1.42845 0.244875C1.27168 0.0880841 1.05906 -1.65206e-09 0.837356 0C0.615652 1.65206e-09 0.403029 0.0880841 0.246261 0.244875C0.0894923 0.401665 0.0014208 0.614319 0.00142079 0.836054C0.00142079 1.05779 0.0894923 1.27044 0.246261 1.42723L3.82613 4.99929L0.246261 8.57135C0.168229 8.64875 0.106294 8.74084 0.0640274 8.84231C0.021761 8.94377 0 9.05261 0 9.16253C0 9.27244 0.021761 9.38128 0.0640274 9.48274C0.106294 9.58421 0.168229 9.6763 0.246261 9.7537C0.323655 9.83175 0.415733 9.89369 0.517185 9.93596C0.618636 9.97824 0.727452 10 0.837356 10C0.94726 10 1.05608 9.97824 1.15753 9.93596C1.25898 9.89369 1.35106 9.83175 1.42845 9.7537L5 6.17332L8.57155 9.7537C8.64894 9.83175 8.74102 9.89369 8.84247 9.93596C8.94392 9.97824 9.05274 10 9.16264 10C9.27255 10 9.38136 9.97824 9.48281 9.93596C9.58427 9.89369 9.67634 9.83175 9.75374 9.7537C9.83177 9.6763 9.89371 9.58421 9.93597 9.48274C9.97824 9.38128 10 9.27244 10 9.16253C10 9.05261 9.97824 8.94377 9.93597 8.84231C9.89371 8.74084 9.83177 8.64875 9.75374 8.57135L6.17387 4.99929Z" fill="black"/></svg>';

$(() => {
    mapType = $('.spec-projects-maps-page').length ? 'persons' : 'objects';

    ymaps.ready(() => {
        map = new ymaps.Map('map', {
            center: mapType === 'persons' ? mapCenter : [58.522869, 31.269793], // Если находимся на странице личности, то за координаты центра лучше взять центр города
            zoom: mapType === 'persons' ? mapZoom : (window.innerWidth > 1199 ? 11 : 10),
            controls: [
                'zoomControl'
            ]
        }, {
            suppressMapOpenBlock: true
        });

        let marks;
        if ( mapType === 'persons' )
            marks = [
                [[58.522869, 31.269793], 'img/spec-projects-maps-page/map-marker-1.png', 'Великий Новгород'],
                [[57.990736, 31.355294], 'img/spec-projects-maps-page/map-marker-2.png', 'Старая Русса'],
                [[58.390155, 33.910294], 'img/spec-projects-maps-page/map-marker-3.png', 'Боровичи']
            ];
        else
            marks = [
                [[58.561849, 31.223098], 'Название объекта 1', 1],
                [[58.541366, 31.376652], 'Объект второй', 2],
                [[58.495758, 31.230296], 'Ну и объект номер три', 3]
            ];

        marks.forEach((item, i) => {
            let markContent;
            if ( mapType === 'persons' )
                markContent = ymaps.templateLayoutFactory.createClass(
                    `<div class="map-marker" id="mark${i}">` +
                        '<div class="img-wrap">' +
                            `<img src="${item[1]}" alt>` +
                        '</div>' +
                        `<div class="title">${item[2]}</div>` +
                        '<div class="dot"></div>' +
                    '</div>'
                );
            else {
                markContent = ymaps.templateLayoutFactory.createClass(
                    `<div class="map-marker object" id="mark${i}">` +
                        '<div class="dot-2">23</div>' +
                    '</div>'
                );
                $(`.objects-list .item[data-object-id="${item[2]}"]`).click(() => {
                    mark.events.fire('click');
                    $('.map .objects-list').removeClass('opened');
                    return false;
                });
            }

            let balloonContent = ymaps.templateLayoutFactory.createClass(
                `<div class="map-balloon${mapType === 'persons' ? '' : ' object'}">` +
                    '<div class="img-wrap" style="background-image: url(img/spec-projects-maps-page/map-balloon-image.png);"></div>' +
                    '<div class="title">Александр, <span>князь Новгородский</span></div>' +
                    '<div class="desc hide-sm">' +
                        'Интерактивная карта Великого Новгорода - административного центра Новгородской области. Путешествие по точкам города, связанным с именем Александра Невского' +
                    '</div>' +
                    '<div class="desc hide-lg show-sm">' +
                        'Великий Новгород,<br> Вокзальная площадь' +
                    '</div>' +
                    `<a href="#" class="more-link">${mapType === 'persons' ? 'Начать путешествие' : 'Посмотреть'}</a>` +
                    '<div class="dot"></div>' +
                    '<div class="dot-2">23</div>' +
                    `<a href="#" class="x">${iconX}</a>` +
                '</div>',
                {
                    build: function() {
                        this.constructor.superclass.build.call(this);
                        this._$element = $('.map-balloon', this.getParentElement());
                        this._$element.find('.x')
                            .on('click', $.proxy(this.onCloseClick, this));
                    },
                    onCloseClick: function (e) {
                        e.preventDefault();
                        this.events.fire('userclose');
                    }
                }
            ),
            mark = new ymaps.Placemark(item[0], {
                hintContent: mapType === 'persons' ? '' : item[1]
            }, {
                iconLayout: 'default#imageWithContent',
                iconImageHref: '',
                iconContentLayout: markContent,
                balloonLayout: balloonContent,
                balloonPanelMaxMapArea: 0,
                markID: i
            });

            mark.events
                .add('balloonopen', e => {
                    let target = e.get('target'),
                        balloonHeight = $('.map-balloon').outerHeight(),
                        pixelCenter = target.geometry.getPixelGeometry().getCoordinates(),
                        coords = map.options.get('projection').fromGlobalPixels([pixelCenter[0], pixelCenter[1] - balloonHeight / 2 - (window.innerWidth < 1200 && mapType === 'persons' ? 0 : 36)], map.getZoom());
                    map.panTo(coords);
                })
                .add('mouseenter', e => {
                    let target = e.get('target'),
                        id = target.options.get('markID');
                    $(`#mark${id}`).addClass('hover');
                })
                .add('mouseleave', e => {
                    let target = e.get('target'),
                        id = target.options.get('markID');
                    $(`#mark${id}`).removeClass('hover');
                });

            map.geoObjects.add(mark);
            //map.geoObjects.add(new ymaps.Placemark(item[0])); // for testing
        });

        let timer = setInterval(() => {
            map.geoObjects.each(el => {
                let text = el.options.get('iconContentLayout').getTemplate()._text,
                    id = $(text).attr('id'),
                    mark = $(`#${id}`),
                    isObject = mark.hasClass('object'),
                    width = mark.outerWidth(),
                    height = mark.outerHeight();
                el.options.set('iconImageSize', [width, height]);
                if ( window.innerWidth > 1199 )
                    el.options.set('iconImageOffset', [-width / 2, -height + (isObject ? 0 : 10)]);
                else
                    el.options.set('iconImageOffset', [-width / 2, -height - (isObject ? 0 : 9 / 2 + 6)]);
                if ( el.options.get('iconImageSize') )
                    clearInterval(timer);
            });
        }, 100);

        if ( $('.spec-projects-object-page').length ) { // Если находимся на странице объекта, то центрируем карту на этом объекте и приближаем
            map.setCenter([58.541366, 31.376652]);
            map.setZoom(12);
        }
    });

    $('.map .objects-btn, .map .objects-list .x').click(() => {
        $('.map .objects-list').toggleClass('opened');
        return false;
    });
});