function TogglePopup(type) {
    let html = $('html');
    type == 'hide'
        ? html.removeClass().find('.popup-wrap .selected').removeClass('selected')
        : $('.popup-wrap .popup').hide().filter(`.${type}`).show().parents('html').removeClass().addClass('popup ' + type);
}
let a = 0;


$(() => {
    $('.header .links-wrap .link').on('mouseenter mouseleave', e =>
        $(e.currentTarget).parents('.links-wrap').toggleClass('hover', e.type === 'mouseenter')
    );
    let a = 0;

    $('#mobileMenu').click(function() {
        $('.mobile-menu').addClass('mobile-menu-active');
    });

    $('#mobileNavigationButton').click(() => {
        $('.mobile-menu').removeClass('mobile-menu-active')
    })

    let hoverTimer;
    $('.header .header-btn.user, .header .auth-submenu').on('mouseenter mouseleave', e => {
        if ( e.type === 'mouseenter' ) {
            clearTimeout(hoverTimer);
            $('.header .auth-submenu').addClass('visible');
        } else
            hoverTimer = setTimeout(() => $('.header .auth-submenu').removeClass('visible'), 500)
    });

    $('.mainpage .block-3 .items-wrap .item').on('mouseenter mouseleave', e => {
        let $this = $(e.currentTarget),
            index = $this.index();
        $('.mainpage .block-3 .hover-image').eq(index).toggleClass('visible', e.type === 'mouseenter')
    });

    let mainpageBlock5Slider = new Swiper('.mainpage .block-5 .swiper', {
        speed: 600,
        slidesPerView: 'auto',
        spaceBetween: 30,
        mousewheel: {
            forceToAxis: true
        },
        scrollbar: {
            el: '.mainpage .block-5 .swiper-scrollbar'
        },
        navigation: {
            nextEl: '.mainpage .block-5 .round-btn.next',
            prevEl: '.mainpage .block-5 .round-btn.prev'
        }
    });

    let mainpageBlock6Slider = new Swiper('.mainpage .block-6 .swiper', {
        speed: 600,
        mousewheel: {
            forceToAxis: true
        },
        pagination: {
            el: '.mainpage .block-6 .swiper-pagination'
        },
        navigation: {
            nextEl: '.mainpage .block-6 .round-btn.next',
            prevEl: '.mainpage .block-6 .round-btn.prev'
        }
    });

    $('.mainpage .block-7 .items-wrap .item .title').click(e =>
        $(e.currentTarget).toggleClass('opened').next().slideToggle()
    );

    $('.fav-btn').click(e => {
        let $this = $(e.currentTarget);
        e.preventDefault();
        favorID = $this.data('id');

        if($this.hasClass('liked'))
            doAction = 'delete';
        else
            doAction = 'add';
        addFavorite(favorID, doAction);
    });

    function addFavorite(id, action)
    {
        param = 'id='+id+"&action="+action;
        $.ajax({
            url: '/include/ajax/favorites.php',
            type: "GET",
            dataType: "html",
            data: param,
            success: function(response) {
                status = $.parseJSON(response);
                if(status == 1){
                    $('.fav-btn[data-id="'+id+'"]').addClass('liked');
                }
                if(status == 2){
                    $('.fav-btn[data-id="'+id+'"]').removeClass('liked');
                }
            },
            error: function(jqXHR, textStatus, errorThrown){
                console.log('Error: '+ errorThrown);
            }
        });
    }

    let cardPageTagsSlider = new Swiper('.card-page .categories-block .tags-slider .swiper', {
        width: 1062,
        speed: 600,
        spaceBetween: 15,
        freeMode: true,
        mousewheel: {
            forceToAxis: true
        },
        navigation: {
            nextEl: '.card-page .slider-btns .round-btn.next',
            prevEl: '.card-page .slider-btns .round-btn.prev'
        },
        scrollbar: {
            el: '.card-page .tags-slider .swiper-scrollbar'
        }
    });

    let cardpagePhotoSlider = new Swiper('.card-page .photo-block .swiper', {
        speed: 600,
        slidesPerView: 'auto',
        spaceBetween: 30,
        mousewheel: {
            forceToAxis: true
        },
        scrollbar: {
            el: '.card-page .photo-block .swiper .swiper-scrollbar'
        },
        navigation: {
            nextEl: '.card-page .photo-block .round-btn.next',
            prevEl: '.card-page .photo-block .round-btn.prev'
        }
    });

    let albumPageDescSlider = new Swiper('.album-page .desc-block .swiper', {
        speed: 600,
        slidesPerView: 'auto',
        spaceBetween: 40,
        mousewheel: {
            forceToAxis: true
        },
        scrollbar: {
            el: '.album-page .desc-block .swiper-scrollbar'
        },
        navigation: {
            nextEl: '.album-page .desc-block .round-btn.next',
            prevEl: '.album-page .desc-block .round-btn.prev'
        }
    });

    let albumPageGallerySlider = new Swiper('.album-page .gallery-block .swiper', {
        speed: 600,
        slidesPerView: 'auto',
        spaceBetween: 40,
        mousewheel: {
            forceToAxis: true
        },
        scrollbar: {
            el: '.album-page .gallery-block .swiper-scrollbar'
        },
        navigation: {
            nextEl: '.album-page .gallery-block .round-btn.next',
            prevEl: '.album-page .gallery-block .round-btn.prev'
        }
    });

    $('.spec-project-single .list-block .filters-wrap .item').click(e => {
        let $this = $(e.currentTarget);
        $this.addClass('current').siblings().removeClass('current');
        return false;
    });

    $('.spec-project-single-page-2 .list-block .tabs-wrap .tab').click(e => {
        let $this = $(e.currentTarget);
        $this.addClass('current').siblings().removeClass('current');
        return false;
    });

    $('.spec-project-single-page-2 .list-block .tabs-wrap .reset-btn').click(e => {
        let $this = $(e.currentTarget);
        $this.siblings('.tab').eq(0).click();
        return false;
    });

    let bookSinglePageSlider = new Swiper('.book-single-page .swiper', {
        spaceBetween: 35,
        speed: 600,
        slidesPerView: 'auto',
        mousewheel: {
            forceToAxis: true
        },
        scrollbar: {
            el: '.book-single-page .swiper .swiper-scrollbar'
        },
        navigation: {
            nextEl: '.book-single-page .slider-btn.next',
            prevEl: '.book-single-page .slider-btn.prev'
        }
    });

    $('.book-single-page .more-wrap .dd-head').click(e => {
        let $this = $(e.currentTarget);
        $this.toggleClass('opened').next().slideToggle();
        return false;
    });

    $('.biblio-single-page .articles-wrap .tags-list .tag').click(e => {
        let $this = $(e.currentTarget);
        $this.addClass('current').siblings().removeClass('current');
        return false;
    });

    $('.biblio-single-page .articles-wrap .articles-list .item svg').click(e => {
        let $this = $(e.currentTarget);
        $this.parent().toggleClass('opened').find('.desc').slideToggle();
    });

    $('.partners-page .tabs-wrap .tab').click(e => {
        let $this = $(e.currentTarget),
            index = $this.index();
        $this.addClass('current').siblings().removeClass('current');
        $('.tab-content').hide().eq(index).fadeIn();
        return false;
    });

    $('.test-page .start-screen .btns-wrap .btn.start').click(e => {
        $('.start-screen').fadeOut(
            () => $('.test-screen').fadeIn(
                () => $('.outer').height($('.inner').eq(0).outerHeight())
            )
        );
        return false;
    });

    $('.test-page .test-screen .question-wrap .btns-wrap .btn.answer').click(e => {
        let $this = $(e.currentTarget),
            wrap = $this.parents('.question-wrap'),
            q = wrap.find('.inner.current'),
            btns = wrap.find('.btns-wrap');
        q.addClass('answered').find('.a').eq(0).addClass('green');
        wrap.find('.msg').slideDown();
        if ( q.index() + 1 === wrap.find('.inner').length )
            btns.eq(1).find('.btn').text('Узнать результат');
        btns.eq(0).fadeOut(
            () => btns.eq(1).fadeIn({
                start: function() { $(this).css('display', 'flex'); }
            })
        );
        return false;
    });

    $('.test-page .test-screen .question-wrap .btns-wrap .btn.next, .test-page .test-screen .question-wrap .btns-wrap .btn.skip').click(e => {
        let $this = $(e.currentTarget),
            outer = $('.outer'),
            innerCurrent = $('.inner.current'),
            innerCurrentIndex = innerCurrent.index(),
            next = innerCurrent.next(),
            msg = outer.siblings('.msg'),
            btns = outer.siblings('.btns-wrap'),
            total = outer.find('.inner').length;
        innerCurrent.removeClass('current').fadeOut(
            () => next.addClass('current').fadeIn({
                start: () => outer.height(next.outerHeight()),
                complete: () =>
                    innerCurrentIndex + 1 < total ?
                        outer.siblings('.count').find('.current').text(innerCurrent.index() + 2)
                    : null
            })
        );
        msg.slideUp();
        btns.eq($this.hasClass('skip') && innerCurrentIndex + 1 >= total ? 0 : 1).fadeOut(
            () => btns.eq(innerCurrentIndex + 1 < total ? 0 : 2).fadeIn({
                start: function() { $(this).css('display', 'flex') }
            })
        );
        return false;
    });

    $('.card-page .info-block .more-wrap').click(e => {
        let $this = $(e.currentTarget);
        $this.toggleClass('opened');
        $('.more-block').slideToggle();
        return false;
    });

    $('[data-popup]').click(function(e) {
        let $this = $(e.currentTarget),
            title = $this.data('title'),
            text = $this.data('text'),
            img = $this.data('img'),
            popup = $this.data('popup');

        $('.popup-name').text(title);
        $('.popup-text').text(text);
        $('.popup-img').html(`<img src="${img}" alt>`);
        if ( e.currentTarget === e.target || e.currentTarget.tagName == 'A' ) {
            TogglePopup(popup);
            return false;
        }
    });

    $('.popup-wrap .popup .categories-list .item').click(e => {
        let $this = $(e.currentTarget);
        $this.addClass('selected').siblings().removeClass('selected');
        $this.parent().addClass('selected');
        return false;
    });

    $('.calendar .days-grid .day').click(e => {
        let $this = $(e.currentTarget);
        $this.addClass('current').siblings().removeClass('current');
        return false;
    });

    let categoryPageSlider = new Swiper('.category-page .swiper', {
        speed: 600,
        slidesPerView: 'auto',
        spaceBetween: 30,
        mousewheel: {
            forceToAxis: true
        },
        scrollbar: {
            el: '.category-page .swiper-scrollbar'
        },
        navigation: {
            nextEl: '.category-page .round-btn.next',
            prevEl: '.category-page .round-btn.prev'
        }
    });

    $('.registry-page .tabs-wrap .tab').click(e => {
        let $this = $(e.currentTarget),
            index = $this.index();
        $this.addClass('current').siblings().removeClass('current');
        $('.tab-content').hide().eq(index).fadeIn();
        return false;
    });

    $('.user-page .bg-wrap .links-wrap .item .title').click(e => {
        let $this = $(e.currentTarget);
        $this.toggleClass('opened').next().slideToggle();
        return false;
    });

    $('.spec-projects-object-page .bg-wrap .links-wrap .btn').click(e => {
        let $this = $(e.currentTarget),
            tabIndex = $this.data('tab');
        if ( tabIndex !== null && tabIndex !== undefined ) {
            let tabContent = $('.tab-content'),
                currTab = tabContent.eq(tabIndex);
            tabContent.slideUp(600);
            if ( !currTab.is(':visible') )
                currTab.slideDown(600);
            return false;
        }
    });

    if ( navigator.userAgent.indexOf('Mac') > 0 )
        $('html').attr('data-os', 'mac-os');


    let regSwiper = new Swiper('.regs-area .swiper', {
        speed: 600,
        slidesPerView: 'auto',
        mousewheel: {
            forceToAxis: true
        },
        scrollbar: {
            el: '.reg-swiper'
        },
        navigation: {
            nextEl: '.reg-btn-next',
            prevEl: '.reg-btn-prev'
        }
    });
});


