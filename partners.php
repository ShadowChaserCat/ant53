<?php $pageTitle = 'Партнеры';

require 'header.php' ?>

<div class="partners-page">
    <div class="content-wrap">
        <div class="page-title">
            <p>Партнеры</p>
            <a href="#" class="round-btn back-btn">
                <?= SVG__ARROW_RIGHT ?>
            </a>
        </div>
        <div class="page-desc">
            В этом разделе присутствуют организации, когда-либо участвовавшие информационно
            или организационно в работе нашей цифровой библиотеки – они предоставляли сайту
            материалы, помогали организовывать мероприятия в поддержку библиотеки ant53.ru,
            консультировали в тематических проектах.
        </div>
        <div class="tabs-wrap">
            <a href="#" class="tab current">СМИ</a>
            <a href="#" class="tab">Культура</a>
            <a href="#" class="tab">Образование</a>
            <a href="#" class="tab">Некоммерческий сектор</a>
            <a href="#" class="tab">Отраслевые орагнизации</a>
            <a href="#" class="tab">Органы власти</a>
        </div>
        <div class="tab-content" style="display: block;">
            <div class="text">
                <p>Газета «Новгород»</p>
                <p>Новая Новгородская газета</p>
                <p>Газета «Новгородские ведомости»</p>
                <p>Сетевое издание «ВНовгороде.ру»</p>
                <p>Сетевое издание «53Новости»</p>
                <p>Новгородское областное телевидение</p>
            </div>
            <img src="img/partners-page/tab-icon-1.png" alt class="icon">
        </div>
        <div class="tab-content">
            <div class="row">
                <div class="title">Старорусская библиотечная система</div>
                <div class="desc">
                    / Муниципальное бюджетное учреждение культуры Старорусского муниципального района
                    «Межпоселенческая централизованная библиотечная система», Старорусский район
                    Новгородской области /
                </div>
            </div>
            <div class="row">
                <div class="title">Боровичская библиотечная система</div>
                <div class="desc">
                    / Муниципальное бюджетное учреждение культуры «Городская централизованная библиотечная
                    система», г. Боровичи Новгородской области /
                </div>
            </div>
            <div class="row">
                <div class="title">Шимская библиотечная система</div>
                <div class="desc">
                    / Муниципальное бюджетное учреждение культуры «Шимская межпоселенческая библиотечная
                    система», Шимский район Новгородской области /
                </div>
            </div>
            <div class="row">
                <div class="title">Культурный центр «Диалог»</div>
                <div class="desc">
                    / Муниципальное автономное учреждение культуры «Центр культуры, искусства и
                    общественных инициатив «Диалог», г. Великий Новгород /
                </div>
            </div>
            <div class="row">
                <div class="title">Дворец культуры «Город»</div>
                <div class="desc">
                    / Муниципальное автономное учреждение культуры «Дворец культуры и молодежи «Город», г.
                    Великий Новгород /
                </div>
            </div>
            <div class="row">
                <div class="title">Комиссия по культуре ОП НО</div>
                <div class="desc">
                    / Комиссия по вопросам развития культуры и сохранению культурного наследия Общественной
                    палаты Новгородской области /
                </div>
            </div>
            <div class="row">
                <div class="title">Поселковые - проект Код Памяти, Тверь</div>
            </div>
            <div class="pagination">
                <a href="#" class="arrow prev">
                    <?= SVG__ARROW_RIGHT ?>
                </a>
                <a href="#" class="page">1</a>
                <a href="#" class="page current">2</a>
                <a href="#" class="page">3</a>
                <a href="#" class="page">4</a>
                <a href="#" class="page">5</a>
                <div class="dots">...</div>
                <a href="#" class="page">9</a>
                <a href="#" class="arrow next">
                    <?= SVG__ARROW_RIGHT ?>
                </a>
            </div>
            <img src="img/partners-page/tab-icon-2.png" alt class="icon">
        </div>
        <div class="tab-content">
            <div class="row">
                <div class="title">Городские и районные школы – проект Код памяти, список</div>
            </div>
            <div class="row">
                <div class="title">Театральная студия «Софит»</div>
                <div class="desc">
                    / Театральная студия «Софит» при муниципальном автономном учреждении дополнительного
                    образования  «Дворец детского (юношеского) творчества имени Лени Голикова», г. Великий
                    Новгород /
                </div>
            </div>
            <div class="row">
                <div class="title">Детский технопарк «Кванториум»</div>
                <div class="desc">
                    / Государственное образовательное автономное учреждение «Новгородский Кванториум», г.
                    Великий Новгород /
                </div>
            </div>
            <img src="img/partners-page/tab-icon-3.png" alt class="icon">
        </div>
        <div class="tab-content">
            <div class="row">
                <div class="title">НОО ВООВ «Боевое братство»</div>
                <div class="desc">
                    / Новгородское областное отделение Всероссийской общественной организации ветеранов
                    «Боевое братство» /
                </div>
            </div>
            <div class="row">
                <div class="title">Юнармия Новгородской области</div>
                <div class="desc">
                    / Региональное отделение всероссийского детско-юношеского военно-патриотического
                    общественного движения «Юнармия» Новгородской области /
                </div>
            </div>
            <div class="row">
                <div class="title">Военно-спортивный центр «Русичи»</div>
                <div class="desc">
                    / АНО «Центр военно-спортивной подготовки «Русичи», Новгородская область /
                </div>
            </div>
            <div class="row">
                <div class="title">НОО ООО семей погибших солдат</div>
                <div class="desc">
                    / Новгородское областное отделение общероссийской общественной организации семей погибших
                    защитников Отечества /
                </div>
            </div>
            <div class="row">
                <div class="title">НРО ООО «Союз писателей России»</div>
                <div class="desc">
                    / Новгородское региональное отделение общероссийской общественной организации «Союз
                    писателей России» /
                </div>
            </div>
            <div class="row">
                <div class="title">Фонд «Благое дело – 53»</div>
                <div class="desc">
                    / Благотворительный фонд «Благое дело – 53», Великий Новгород /
                </div>
            </div>
            <div class="row">
                <div class="title">Географическое общество и др. – в проекте экологическом список</div>
            </div>
            <div class="row">
                <div class="title">Военно-историческое общество</div>
            </div>
            <img src="img/partners-page/tab-icon-4.png" alt class="icon">
        </div>
        <div class="tab-content">
            <div class="row">
                <div class="title">Дирекция по управлению ООПТ</div>
                <div class="desc">
                    / Областное государственное бюджетное учреждение «Дирекция по управлению особо
                    охраняемыми природными территориями», Новгородская область /
                </div>
            </div>
            <div class="row">
                <div class="title">Природный заповедник «Рдейский»</div>
                <div class="desc">
                    / Федеральное государственное бюджетное учреждение «Государственный природный
                    заповедник «Рдейский», Новгородская область /
                </div>
            </div>
            <div class="row">
                <div class="title">Военкомат, ОМОН 0 Код памяти, список</div>
            </div>
            <img src="img/partners-page/tab-icon-5.png" alt class="icon">
        </div>
        <div class="tab-content">
            <div class="row">
                <div class="title">Комитет культуры (Великий Новгород)</div>
                <div class="desc">
                    / Комитет культуры и молодежной политики Администрации Великого Новгорода /
                </div>
            </div>
            <div class="row">
                <div class="title">Новгородская областная Дума</div>
            </div>
            <img src="img/partners-page/tab-icon-6.png" alt class="icon">
        </div>
    </div>
</div>

<?php require 'footer.php' ?>