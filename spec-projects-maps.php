<?php $pageTitle = 'Спецпроекты - карты';

require 'header.php' ?>

<div class="spec-projects-maps-page">
    <div class="content-wrap">
        <div class="page-title">
            <p>Три имени,</p>
            <p>изменивших мир</p>
            <a href="#" class="round-btn back-btn">
                <?= SVG__ARROW_RIGHT ?>
            </a>
            <div class="round-btn age-limit">12+</div>
        </div>
        <div class="page-subtitle">Интерактивные карты-путешествия</div>
        <div class="page-desc">
            Три интерактивные карты-путешествия – по трем крупнейшим городам Новгородской
            области: Великому Новгороду, Боровичам и Старой Руссе. Проведут пользователей по
            объектам городской среды, связанным с именами Александра Невского, Ф.М.
            Достоевского и А.В. Суворова.
        </div>
    </div>
    <div class="map" id="map"></div>
</div>

<script src="js/map.js?v=<?= time() ?>"></script>

<?php require 'footer.php' ?>