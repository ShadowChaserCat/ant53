<?php $pageTitle = 'Тематические рубрики';

require 'header.php' ?>

<div class="category-page">
    <div class="content-wrap">
        <div class="page-title">
            <p>Тематические</p>
            <p>рубрики</p>
            <a href="#" class="round-btn back-btn">
                <?= SVG__ARROW_RIGHT ?>
            </a>
            <div class="round-btn age-limit">12+</div>
        </div>
    </div>
    <div class="img-block"></div>
    <div class="category-block">
        <div class="content-wrap">
            <div class="subcategories-wrap">
                <div class="title">Выберите рубрику:</div>
                <div class="list">
                    <a href="#" class="item">Memory</a>
                    <a href="#" class="item">Армия и флот</a>
                    <a href="#" class="item">Археология</a>
                    <a href="#" class="item">Архитектура</a>
                    <a href="#" class="item">Военная история</a>
                    <a href="#" class="item">Города и села</a>
                    <a href="#" class="item">Законность и правопорядок</a>
                    <a href="#" class="item">Искусство и Культура</a>
                    <a href="#" class="item">Космонавтика</a>
                    <a href="#" class="item">Литература</a>
                    <a href="#" class="item">Медицина и здравоохранение</a>
                    <a href="#" class="item">Наука</a>
                    <a href="#" class="item">Образование</a>
                    <a href="#" class="item">Общество и политика</a>
                    <a href="#" class="item">Открытия и изобретения</a>
                    <a href="#" class="item">Природа</a>
                    <a href="#" class="item">Промышленность</a>
                    <a href="#" class="item">Религия</a>
                    <a href="#" class="item">Связь и коммуникации</a>
                    <a href="#" class="item">Сельское и лесное хозяйство</a>
                    <a href="#" class="item">Средства массовой информации</a>
                    <a href="#" class="item">Страницы истории</a>
                    <a href="#" class="item">Техника</a>
                    <a href="#" class="item">Транспорт</a>
                    <a href="#" class="item">Физкультура и спорт</a>
                    <a href="#" class="item">Фольклор</a>
                    <a href="#" class="item">Чрезвычайные ситуации</a>
                </div>
            </div>
            <div class="block-title">
                <p>Последние</p>
                <p>обновления</p>
            </div>
            <div class="swiper">
                <div class="swiper-wrapper">
                    <div class="swiper-slide">
                        <img src="img/mainpage/block-5-img-1.png" alt>
                        <div class="title">Заголовок</div>
                        <div class="desc">Краткое описание текст примерно на одно-два предложения</div>
                    </div>
                    <div class="swiper-slide">
                        <img src="img/mainpage/block-5-img-2.png" alt>
                        <div class="title">Заголовок</div>
                        <div class="desc">Краткое описание текст примерно на одно-два предложения</div>
                    </div>
                    <div class="swiper-slide">
                        <img src="img/mainpage/block-5-img-3.png" alt>
                        <div class="title">Заголовок</div>
                        <div class="desc">Краткое описание текст примерно на одно-два предложения</div>
                    </div>
                    <div class="swiper-slide">
                        <img src="img/mainpage/block-5-img-1.png" alt>
                        <div class="title">Заголовок</div>
                        <div class="desc">Краткое описание текст примерно на одно-два предложения</div>
                    </div>
                    <div class="swiper-slide">
                        <img src="img/mainpage/block-5-img-2.png" alt>
                        <div class="title">Заголовок</div>
                        <div class="desc">Краткое описание текст примерно на одно-два предложения</div>
                    </div>
                    <div class="swiper-slide">
                        <img src="img/mainpage/block-5-img-3.png" alt>
                        <div class="title">Заголовок</div>
                        <div class="desc">Краткое описание текст примерно на одно-два предложения</div>
                    </div>
                </div>
                <div class="swiper-scrollbar"></div>
            </div>
            <div class="btns-wrap">
                <a href="#" class="round-btn prev">
                    <?= SVG__ARROW_RIGHT ?>
                </a>
                <a href="#" class="round-btn next">
                    <?= SVG__ARROW_RIGHT ?>
                </a>
            </div>
        </div>
    </div>
</div>

<?php require 'footer.php' ?>