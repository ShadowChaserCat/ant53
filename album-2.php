<?php $pageTitle = 'Новгородский альбом победителей';

require 'header.php' ?>

<div class="album-page" style="background-image: url(img/album-page/bg.png);">
    <div class="content-wrap">
        <div class="page-title">
            <p>Новгородский</p>
            <p>альбом победителей</p>
            <a href="#" class="round-btn back-btn">
                <?= SVG__ARROW_RIGHT ?>
            </a>
            <div class="round-btn age-limit">12+</div>
        </div>
        <div class="letters-wrap">
            <a href="#" class="letter">а</a>
            <a href="#" class="letter">б</a>
            <a href="#" class="letter">в</a>
            <a href="#" class="letter">г</a>
            <a href="#" class="letter">д</a>
            <a href="#" class="letter">е</a>
            <a href="#" class="letter">ё</a>
            <a href="#" class="letter">ж</a>
            <a href="#" class="letter">з</a>
            <a href="#" class="letter">и</a>
            <a href="#" class="letter">й</a>
            <a href="#" class="letter">к</a>
            <a href="#" class="letter">л</a>
            <a href="#" class="letter">м</a>
            <a href="#" class="letter">н</a>
            <a href="#" class="letter">о</a>
            <a href="#" class="letter">п</a>
            <a href="#" class="letter">р</a>
            <a href="#" class="letter">с</a>
            <a href="#" class="letter">т</a>
            <a href="#" class="letter">у</a>
            <a href="#" class="letter">ф</a>
            <a href="#" class="letter">х</a>
            <a href="#" class="letter">ц</a>
            <a href="#" class="letter">ч</a>
            <a href="#" class="letter">ш</a>
            <a href="#" class="letter">щ</a>
            <a href="#" class="letter">ы</a>
            <a href="#" class="letter">э</a>
            <a href="#" class="letter">ю</a>
            <a href="#" class="letter">я</a>
        </div>
        <div class="items-grid">
            <div class="item">
                <a href="#" class="img-wrap">
                    <img src="img/placeholder.png" alt>
                </a>
                <a href="#" class="title">Новгород</a>
                <div class="desc">Обзор книги: Кушнир И.И. Новгород. – Л.: Стройиздат, 1967.</div>
            </div>
            <div class="item">
                <a href="#" class="img-wrap">
                    <img src="img/placeholder.png" alt>
                </a>
                <a href="#" class="title">Новгород</a>
                <div class="desc">Обзор книги: Кушнир И.И. Новгород. – Л.: Стройиздат, 1967.</div>
            </div>
            <div class="item">
                <a href="#" class="img-wrap">
                    <img src="img/placeholder.png" alt>
                </a>
                <a href="#" class="title">Новгород</a>
                <div class="desc">Обзор книги: Кушнир И.И. Новгород. – Л.: Стройиздат, 1967.</div>
            </div>
            <div class="item">
                <a href="#" class="img-wrap">
                    <img src="img/placeholder.png" alt>
                </a>
                <a href="#" class="title">Новгород</a>
                <div class="desc">Обзор книги: Кушнир И.И. Новгород. – Л.: Стройиздат, 1967.</div>
            </div>
            <div class="item">
                <a href="#" class="img-wrap">
                    <img src="img/placeholder.png" alt>
                </a>
                <a href="#" class="title">Новгород</a>
                <div class="desc">Обзор книги: Кушнир И.И. Новгород. – Л.: Стройиздат, 1967.</div>
            </div>
            <div class="item">
                <a href="#" class="img-wrap">
                    <img src="img/placeholder.png" alt>
                </a>
                <a href="#" class="title">Новгород</a>
                <div class="desc">Обзор книги: Кушнир И.И. Новгород. – Л.: Стройиздат, 1967.</div>
            </div>
            <div class="item">
                <a href="#" class="img-wrap">
                    <img src="img/placeholder.png" alt>
                </a>
                <a href="#" class="title">Новгород</a>
                <div class="desc">Обзор книги: Кушнир И.И. Новгород. – Л.: Стройиздат, 1967.</div>
            </div>
            <div class="item">
                <a href="#" class="img-wrap">
                    <img src="img/placeholder.png" alt>
                </a>
                <a href="#" class="title">Новгород</a>
                <div class="desc">Обзор книги: Кушнир И.И. Новгород. – Л.: Стройиздат, 1967.</div>
            </div>
            <div class="item">
                <a href="#" class="img-wrap">
                    <img src="img/placeholder.png" alt>
                </a>
                <a href="#" class="title">Новгород</a>
                <div class="desc">Обзор книги: Кушнир И.И. Новгород. – Л.: Стройиздат, 1967.</div>
            </div>
            <div class="item">
                <a href="#" class="img-wrap">
                    <img src="img/placeholder.png" alt>
                </a>
                <a href="#" class="title">Новгород</a>
                <div class="desc">Обзор книги: Кушнир И.И. Новгород. – Л.: Стройиздат, 1967.</div>
            </div>
            <div class="item">
                <a href="#" class="img-wrap">
                    <img src="img/placeholder.png" alt>
                </a>
                <a href="#" class="title">Новгород</a>
                <div class="desc">Обзор книги: Кушнир И.И. Новгород. – Л.: Стройиздат, 1967.</div>
            </div>
            <div class="item">
                <a href="#" class="img-wrap">
                    <img src="img/placeholder.png" alt>
                </a>
                <a href="#" class="title">Новгород</a>
                <div class="desc">Обзор книги: Кушнир И.И. Новгород. – Л.: Стройиздат, 1967.</div>
            </div>
            <div class="item">
                <a href="#" class="img-wrap">
                    <img src="img/placeholder.png" alt>
                </a>
                <a href="#" class="title">Новгород</a>
                <div class="desc">Обзор книги: Кушнир И.И. Новгород. – Л.: Стройиздат, 1967.</div>
            </div>
            <div class="item">
                <a href="#" class="img-wrap">
                    <img src="img/placeholder.png" alt>
                </a>
                <a href="#" class="title">Новгород</a>
                <div class="desc">Обзор книги: Кушнир И.И. Новгород. – Л.: Стройиздат, 1967.</div>
            </div>
            <div class="item">
                <a href="#" class="img-wrap">
                    <img src="img/placeholder.png" alt>
                </a>
                <a href="#" class="title">Новгород</a>
                <div class="desc">Обзор книги: Кушнир И.И. Новгород. – Л.: Стройиздат, 1967.</div>
            </div>
            <div class="item">
                <a href="#" class="img-wrap">
                    <img src="img/placeholder.png" alt>
                </a>
                <a href="#" class="title">Новгород</a>
                <div class="desc">Обзор книги: Кушнир И.И. Новгород. – Л.: Стройиздат, 1967.</div>
            </div>
        </div>
        <div class="pagination">
            <a href="#" class="arrow prev">
                <?= SVG__ARROW_RIGHT ?>
            </a>
            <a href="#" class="page">1</a>
            <a href="#" class="page current">2</a>
            <a href="#" class="page">3</a>
            <a href="#" class="page">4</a>
            <a href="#" class="page">5</a>
            <div class="dots">...</div>
            <a href="#" class="page">9</a>
            <a href="#" class="arrow next">
                <?= SVG__ARROW_RIGHT ?>
            </a>
        </div>
    </div>
</div>

<?php require 'footer.php' ?>