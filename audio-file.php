<?php $pageTitle = 'Каталог аудио';

require 'header.php' ?>

    <div class="catalog-page file-page">
        <div class="content-wrap">
            <div class="page-title">
                <h1>Название блока</h1>
                <a href="#" class="round-btn back-btn">
                    <?= SVG__ARROW_RIGHT ?>
                </a>
            </div>
            <div class="items-list media-list">
                <div class="media-item audio">
                    <div class="desc-wrap">
                        <a href="#" class="title">Куличный луг</a>
                        <div class="desc">
                            <p>
                                Памятник природы регионального значения. Расположен в Старорусском районе Новгородской
                                области. Фото предоставлено краеведом г. Старая Русса Павлом Володиным, через социальную
                                сеть VK.
                            </p>
                            <p>
                                Дата съемки: 13.07.2011.
                            </p>
                        </div>
                        <div class="player-wrap">
                            <div class="time-wrap">
                                <div class="current">00:00</div>
                                <div class="total">04:30</div>
                            </div>
                            <div class="progress-bar"></div>
                        </div>
                        <div class="btns-wrap">
                            <a href="#" class="btn alt">Слушать</a>
                            <a href="#" class="round-btn fav-btn">
                                <?= SVG__HEART ?>
                            </a>
                            <div class="round-btn age-limit">12+</div>
                        </div>
                    </div>
                </div>
                <div class="media-item audio">
                    <div class="desc-wrap">
                        <a href="#" class="title">Куличный луг</a>
                        <div class="desc">
                            <p>
                                Памятник природы регионального значения. Расположен в Старорусском районе Новгородской
                                области. Фото предоставлено краеведом г. Старая Русса Павлом Володиным, через социальную
                                сеть VK.
                            </p>
                            <p>
                                Дата съемки: 13.07.2011.
                            </p>
                        </div>
                        <div class="player-wrap">
                            <div class="time-wrap">
                                <div class="current">00:00</div>
                                <div class="total">04:30</div>
                            </div>
                            <div class="progress-bar"></div>
                        </div>
                        <div class="btns-wrap">
                            <a href="#" class="btn alt">Слушать</a>
                            <a href="#" class="round-btn fav-btn">
                                <?= SVG__HEART ?>
                            </a>
                            <div class="round-btn age-limit">12+</div>
                        </div>
                    </div>
                </div>
                <div class="media-item audio">
                    <div class="desc-wrap">
                        <a href="#" class="title">Куличный луг</a>
                        <div class="desc">
                            <p>
                                Памятник природы регионального значения. Расположен в Старорусском районе Новгородской
                                области. Фото предоставлено краеведом г. Старая Русса Павлом Володиным, через социальную
                                сеть VK.
                            </p>
                            <p>
                                Дата съемки: 13.07.2011.
                            </p>
                        </div>
                        <div class="player-wrap">
                            <div class="time-wrap">
                                <div class="current">00:00</div>
                                <div class="total">04:30</div>
                            </div>
                            <div class="progress-bar"></div>
                        </div>
                        <div class="btns-wrap">
                            <a href="#" class="btn alt">Слушать</a>
                            <a href="#" class="round-btn fav-btn">
                                <?= SVG__HEART ?>
                            </a>
                            <div class="round-btn age-limit">12+</div>
                        </div>
                    </div>
                </div>
                <div class="media-item audio">
                    <div class="desc-wrap">
                        <a href="#" class="title">Куличный луг</a>
                        <div class="desc">
                            <p>
                                Памятник природы регионального значения. Расположен в Старорусском районе Новгородской
                                области. Фото предоставлено краеведом г. Старая Русса Павлом Володиным, через социальную
                                сеть VK.
                            </p>
                            <p>
                                Дата съемки: 13.07.2011.
                            </p>
                        </div>
                        <div class="player-wrap">
                            <div class="time-wrap">
                                <div class="current">00:00</div>
                                <div class="total">04:30</div>
                            </div>
                            <div class="progress-bar"></div>
                        </div>
                        <div class="btns-wrap">
                            <a href="#" class="btn alt">Слушать</a>
                            <a href="#" class="round-btn fav-btn">
                                <?= SVG__HEART ?>
                            </a>
                            <div class="round-btn age-limit">12+</div>
                        </div>
                    </div>
                </div>
                <div class="media-item audio">
                    <div class="desc-wrap">
                        <a href="#" class="title">Куличный луг</a>
                        <div class="desc">
                            <p>
                                Памятник природы регионального значения. Расположен в Старорусском районе Новгородской
                                области. Фото предоставлено краеведом г. Старая Русса Павлом Володиным, через социальную
                                сеть VK.
                            </p>
                            <p>
                                Дата съемки: 13.07.2011.
                            </p>
                        </div>
                        <div class="player-wrap">
                            <div class="time-wrap">
                                <div class="current">00:00</div>
                                <div class="total">04:30</div>
                            </div>
                            <div class="progress-bar"></div>
                        </div>
                        <div class="btns-wrap">
                            <a href="#" class="btn alt">Слушать</a>
                            <a href="#" class="round-btn fav-btn">
                                <?= SVG__HEART ?>
                            </a>
                            <div class="round-btn age-limit">12+</div>
                        </div>
                    </div>
                </div>
                <div class="media-item audio">
                    <div class="desc-wrap">
                        <a href="#" class="title">Куличный луг</a>
                        <div class="desc">
                            <p>
                                Памятник природы регионального значения. Расположен в Старорусском районе Новгородской
                                области. Фото предоставлено краеведом г. Старая Русса Павлом Володиным, через социальную
                                сеть VK.
                            </p>
                            <p>
                                Дата съемки: 13.07.2011.
                            </p>
                        </div>
                        <div class="player-wrap">
                            <div class="time-wrap">
                                <div class="current">00:00</div>
                                <div class="total">04:30</div>
                            </div>
                            <div class="progress-bar"></div>
                        </div>
                        <div class="btns-wrap">
                            <a href="#" class="btn alt">Слушать</a>
                            <a href="#" class="round-btn fav-btn">
                                <?= SVG__HEART ?>
                            </a>
                            <div class="round-btn age-limit">12+</div>
                        </div>
                    </div>
                </div>

            </div>
            <div class="pagination">
                <a href="#" class="arrow prev">
                    <?= SVG__ARROW_RIGHT ?>
                </a>
                <a href="#" class="page">1</a>
                <a href="#" class="page current">2</a>
                <a href="#" class="page">3</a>
                <a href="#" class="page">4</a>
                <a href="#" class="page">5</a>
                <div class="dots">...</div>
                <a href="#" class="page">9</a>
                <a href="#" class="arrow next">
                    <?= SVG__ARROW_RIGHT ?>
                </a>
            </div>
        </div>
    </div>

<?php require 'footer.php' ?>