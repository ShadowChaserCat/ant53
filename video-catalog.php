<?php $pageTitle = 'Каталог видео';

require 'header.php' ?>

<div class="catalog-page">
    <div class="content-wrap">
        <div class="page-title">
            <p>Каталог видео</p>
            <a href="#" class="round-btn back-btn">
                <?= SVG__ARROW_RIGHT ?>
            </a>
        </div>
        <div>
            <ul class="letters-list">
                <li class="letter">а</li>
                <li class="letter">б</li>
                <li class="letter">в</li>
                <li class="letter">г</li>
                <li class="letter">д</li>
                <li class="letter">е</li>
                <li class="letter">ё</li>
                <li class="letter">ж</li>
                <li class="letter">з</li>
                <li class="letter">и</li>
                <li class="letter">й</li>
                <li class="letter">к</li>
                <li class="letter">л</li>
                <li class="letter">м</li>
                <li class="letter">н</li>
                <li class="letter">о</li>
                <li class="letter">п</li>
                <li class="letter">р</li>
                <li class="letter">с</li>
                <li class="letter">т</li>
                <li class="letter">у</li>
                <li class="letter">ф</li>
                <li class="letter">х</li>
                <li class="letter">ц</li>
                <li class="letter">ч</li>
                <li class="letter">ш</li>
                <li class="letter">щ</li>
                <li class="letter">ы</li>
                <li class="letter">э</li>
                <li class="letter">ю</li>
                <li class="letter">я</li>
            </ul>
            <div class="regs-area">
                <div class="swiper">
                    <div class="swiper-wrapper">
                        <ul class="swiper-slide">
                            <li class="swiper-slide__list">Великий Новгород</li>
                            <li class="swiper-slide__list">Батецкий район</li>
                            <li class="swiper-slide__list">Боровичский район </li>
                            <li class="swiper-slide__list">Псковская</li>
                            <li class="swiper-slide__list">Щусева</li>
                            <li class="swiper-slide__list">Батецкий район</li>
                            <li class="swiper-slide__list">Великий Новгород</li>
                            <li class="swiper-slide__list">Батецкий район</li>
                            <li class="swiper-slide__list">Боровичский район </li>
                            <li class="swiper-slide__list">Псковская</li>
                            <li class="swiper-slide__list">Щусева</li>
                            <li class="swiper-slide__list">Батецкий район</li>
                        </ul>
                        <ul class="swiper-slide">
                            <li class="swiper-slide__list">Великий Новгород</li>
                            <li class="swiper-slide__list">Батецкий район</li>
                            <li class="swiper-slide__list">Боровичский район </li>
                            <li class="swiper-slide__list">Псковская</li>
                            <li class="swiper-slide__list">Щусева</li>
                            <li class="swiper-slide__list">Батецкий район</li>
                            <li class="swiper-slide__list">Великий Новгород</li>
                            <li class="swiper-slide__list">Батецкий район</li>
                            <li class="swiper-slide__list">Боровичский район </li>
                            <li class="swiper-slide__list">Псковская</li>
                            <li class="swiper-slide__list">Щусева</li>
                            <li class="swiper-slide__list">Батецкий район</li>
                        </ul>
                        <ul class="swiper-slide">
                            <li class="swiper-slide__list">Великий Новгород</li>
                            <li class="swiper-slide__list">Батецкий район</li>
                            <li class="swiper-slide__list">Боровичский район </li>
                            <li class="swiper-slide__list">Псковская</li>
                            <li class="swiper-slide__list">Щусева</li>
                            <li class="swiper-slide__list">Батецкий район</li>
                            <li class="swiper-slide__list">Великий Новгород</li>
                            <li class="swiper-slide__list">Батецкий район</li>
                            <li class="swiper-slide__list">Боровичский район </li>
                            <li class="swiper-slide__list">Псковская</li>
                            <li class="swiper-slide__list">Щусева</li>
                            <li class="swiper-slide__list">Батецкий район</li>
                        </ul>
                    </div>
                </div>
                <div class="slider-btns">
                    <a href="#" class="slider-btn reg-btn-prev">
                        <?= SVG__ARROW_RIGHT ?>
                    </a>
                    <a href="#" class="slider-btn reg-btn-next">
                        <?= SVG__ARROW_RIGHT ?>
                    </a>
                    <div class="reg-swiper swiper-scrollbar"></div>
                </div>
            </div>
        </div>
        <div class="items-list">
            <div class="item">
                <a href="#" class="img-wrap">
                    <img src="img/placeholder-295x205.png" alt>
                </a>
                <div class="desc-wrap">
                    <a href="#" class="title">Куличный луг</a>
                    <div class="desc">
                        Памятник природы регионального значения. Расположен в Старорусском районе Новгородской области. Фото предоставлено краеведом г. Старая Русса Павлом Володиным, через социальную сеть VK.<br>
                        Дата съемки: 13.07.2011.
                    </div>
                    <div class="btns-wrap">
                        <a href="#" class="btn alt">Смотреть</a>
                        <div class="round-btn age-limit">12+</div>
                    </div>
                </div>
            </div>
            <div class="item">
                <a href="#" class="img-wrap">
                    <img src="img/placeholder-295x205.png" alt>
                </a>
                <div class="desc-wrap">
                    <a href="#" class="title">Куличный луг</a>
                    <div class="desc">
                        Памятник природы регионального значения. Расположен в Старорусском районе Новгородской области. Фото предоставлено краеведом г. Старая Русса Павлом Володиным, через социальную сеть VK.<br>
                        Дата съемки: 13.07.2011.
                    </div>
                    <div class="btns-wrap">
                        <a href="#" class="btn alt">Смотреть</a>
                        <div class="round-btn age-limit">12+</div>
                    </div>
                </div>
            </div>
            <div class="item">
                <a href="#" class="img-wrap">
                    <img src="img/placeholder-295x205.png" alt>
                </a>
                <div class="desc-wrap">
                    <a href="#" class="title">Куличный луг</a>
                    <div class="desc">
                        Памятник природы регионального значения. Расположен в Старорусском районе Новгородской области. Фото предоставлено краеведом г. Старая Русса Павлом Володиным, через социальную сеть VK.<br>
                        Дата съемки: 13.07.2011.
                    </div>
                    <div class="btns-wrap">
                        <a href="#" class="btn alt">Смотреть</a>
                        <div class="round-btn age-limit">12+</div>
                    </div>
                </div>
            </div>
            <div class="item">
                <a href="#" class="img-wrap">
                    <img src="img/placeholder-295x205.png" alt>
                </a>
                <div class="desc-wrap">
                    <a href="#" class="title">Куличный луг</a>
                    <div class="desc">
                        Памятник природы регионального значения. Расположен в Старорусском районе Новгородской области. Фото предоставлено краеведом г. Старая Русса Павлом Володиным, через социальную сеть VK.<br>
                        Дата съемки: 13.07.2011.
                    </div>
                    <div class="btns-wrap">
                        <a href="#" class="btn alt">Смотреть</a>
                        <div class="round-btn age-limit">12+</div>
                    </div>
                </div>
            </div>
            <div class="item">
                <a href="#" class="img-wrap">
                    <img src="img/placeholder-295x205.png" alt>
                </a>
                <div class="desc-wrap">
                    <a href="#" class="title">Куличный луг</a>
                    <div class="desc">
                        Памятник природы регионального значения. Расположен в Старорусском районе Новгородской области. Фото предоставлено краеведом г. Старая Русса Павлом Володиным, через социальную сеть VK.<br>
                        Дата съемки: 13.07.2011.
                    </div>
                    <div class="btns-wrap">
                        <a href="#" class="btn alt">Смотреть</a>
                        <div class="round-btn age-limit">12+</div>
                    </div>
                </div>
            </div>
            <div class="item">
                <a href="#" class="img-wrap">
                    <img src="img/placeholder-295x205.png" alt>
                </a>
                <div class="desc-wrap">
                    <a href="#" class="title">Куличный луг</a>
                    <div class="desc">
                        Памятник природы регионального значения. Расположен в Старорусском районе Новгородской области. Фото предоставлено краеведом г. Старая Русса Павлом Володиным, через социальную сеть VK.<br>
                        Дата съемки: 13.07.2011.
                    </div>
                    <div class="btns-wrap">
                        <a href="#" class="btn alt">Смотреть</a>
                        <div class="round-btn age-limit">12+</div>
                    </div>
                </div>
            </div>
        </div>
        <div class="pagination">
            <a href="#" class="arrow prev">
                <?= SVG__ARROW_RIGHT ?>
            </a>
            <a href="#" class="page">1</a>
            <a href="#" class="page current">2</a>
            <a href="#" class="page">3</a>
            <a href="#" class="page">4</a>
            <a href="#" class="page">5</a>
            <div class="dots">...</div>
            <a href="#" class="page">9</a>
            <a href="#" class="arrow next">
                <?= SVG__ARROW_RIGHT ?>
            </a>
        </div>
    </div>
</div>

<?php require 'footer.php' ?>