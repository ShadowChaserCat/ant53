<?php $pageTitle = 'Спецпроекты - MEMORIAL';

require 'header.php' ?>

<div class="spec-project-single-page-3">
    <div class="content-wrap">
        <div class="page-title">
            <p>Проект “MEMORIAL”</p>
            <p>Мемориальные доски Великого Новгорода</p>
            <a href="#" class="round-btn back-btn">
                <?= SVG__ARROW_RIGHT ?>
            </a>
            <div class="round-btn age-limit">12+</div>
        </div>
    </div>
    <div class="img-block">
        <img src="img/spec-project-single-page-3/img-block.png" alt>
    </div>
    <div class="content-wrap">
        <div class="registry-wrap">
            <div class="count">153</div>
            <div class="name">мемориальных досок</div>
            <?= SVG__ARROW_TOP_RIGHT ?>
            <a href="#" class="btn alt">Реестр мемориальных досок</a>
        </div>
    </div>
    <div class="list-block">
        <div class="content-wrap">
            <div class="list-filters-wrap">
                <div class="title">Фильтры:</div>
                <div class="list-wrap">
                    <div class="list">
                        <a href="#" class="plus-btn">
                            <?= SVG__PLUS ?>
                        </a>
                    </div>
                    <div class="btns-wrap">
                        <a href="#" class="btn alt apply-btn">Применить</a>
                        <a href="#" class="btn gray reset-btn">Сбросить</a>
                    </div>
                </div>
            </div>
            <div class="list-wrap">
                <div class="item">
                    <a href="#" class="img-wrap">
                        <img src="img/placeholder.png" alt>
                    </a>
                    <div class="desc-wrap">
                        <a href="#" class="title">Памятная доска - Освобождение Новгорода</a>
                        <div class="icon-wrap">
                            <img src="img/svg/calendar.svg" alt>
                            20 Января 1969 года
                        </div>
                        <div class="icon-wrap">
                            <img src="img/svg/marker.svg" alt>
                            Великий Новгород, Новгородский кремль (восточная стена)
                        </div>
                        <div class="desc">
                            Памятная доска установлена на восточной стене Новгородского кремля, рядом со звонницей. Здесь 20 января 1944 года воинами 378-й стрелковой дивизии А.Р. Белова было водружено Красное знамя, как символ освобождения Новгорода от немецко-фашистских захватчиков.
                        </div>
                        <div class="btns-wrap">
                            <a href="#" class="btn alt">Смотреть на карте</a>
                            <a href="#" class="btn gray">Предыдущий вариант доски</a>
                        </div>
                    </div>
                </div>
                <div class="item">
                    <a href="#" class="img-wrap">
                        <img src="img/placeholder.png" alt>
                    </a>
                    <div class="desc-wrap">
                        <a href="#" class="title">Памятная доска - Освобождение Новгорода</a>
                        <div class="icon-wrap">
                            <img src="img/svg/calendar.svg" alt>
                            20 Января 1969 года
                        </div>
                        <div class="icon-wrap">
                            <img src="img/svg/marker.svg" alt>
                            Великий Новгород, Новгородский кремль (восточная стена)
                        </div>
                        <div class="desc">
                            Памятная доска установлена на восточной стене Новгородского кремля, рядом со звонницей. Здесь 20 января 1944 года воинами 378-й стрелковой дивизии А.Р. Белова было водружено Красное знамя, как символ освобождения Новгорода от немецко-фашистских захватчиков.
                        </div>
                        <div class="btns-wrap">
                            <a href="#" class="btn alt">Смотреть на карте</a>
                            <a href="#" class="btn gray">Предыдущий вариант доски</a>
                        </div>
                    </div>
                </div>
                <div class="item">
                    <a href="#" class="img-wrap">
                        <img src="img/placeholder.png" alt>
                    </a>
                    <div class="desc-wrap">
                        <a href="#" class="title">Памятная доска - Освобождение Новгорода</a>
                        <div class="icon-wrap">
                            <img src="img/svg/calendar.svg" alt>
                            20 Января 1969 года
                        </div>
                        <div class="icon-wrap">
                            <img src="img/svg/marker.svg" alt>
                            Великий Новгород, Новгородский кремль (восточная стена)
                        </div>
                        <div class="desc">
                            Памятная доска установлена на восточной стене Новгородского кремля, рядом со звонницей. Здесь 20 января 1944 года воинами 378-й стрелковой дивизии А.Р. Белова было водружено Красное знамя, как символ освобождения Новгорода от немецко-фашистских захватчиков.
                        </div>
                        <div class="btns-wrap">
                            <a href="#" class="btn alt">Смотреть на карте</a>
                            <a href="#" class="btn gray">Предыдущий вариант доски</a>
                        </div>
                    </div>
                </div>
                <div class="item">
                    <a href="#" class="img-wrap">
                        <img src="img/placeholder.png" alt>
                    </a>
                    <div class="desc-wrap">
                        <a href="#" class="title">Памятная доска - Освобождение Новгорода</a>
                        <div class="icon-wrap">
                            <img src="img/svg/calendar.svg" alt>
                            20 Января 1969 года
                        </div>
                        <div class="icon-wrap">
                            <img src="img/svg/marker.svg" alt>
                            Великий Новгород, Новгородский кремль (восточная стена)
                        </div>
                        <div class="desc">
                            Памятная доска установлена на восточной стене Новгородского кремля, рядом со звонницей. Здесь 20 января 1944 года воинами 378-й стрелковой дивизии А.Р. Белова было водружено Красное знамя, как символ освобождения Новгорода от немецко-фашистских захватчиков.
                        </div>
                        <div class="btns-wrap">
                            <a href="#" class="btn alt">Смотреть на карте</a>
                            <a href="#" class="btn gray">Предыдущий вариант доски</a>
                        </div>
                    </div>
                </div>
                <div class="item">
                    <a href="#" class="img-wrap">
                        <img src="img/placeholder.png" alt>
                    </a>
                    <div class="desc-wrap">
                        <a href="#" class="title">Памятная доска - Освобождение Новгорода</a>
                        <div class="icon-wrap">
                            <img src="img/svg/calendar.svg" alt>
                            20 Января 1969 года
                        </div>
                        <div class="icon-wrap">
                            <img src="img/svg/marker.svg" alt>
                            Великий Новгород, Новгородский кремль (восточная стена)
                        </div>
                        <div class="desc">
                            Памятная доска установлена на восточной стене Новгородского кремля, рядом со звонницей. Здесь 20 января 1944 года воинами 378-й стрелковой дивизии А.Р. Белова было водружено Красное знамя, как символ освобождения Новгорода от немецко-фашистских захватчиков.
                        </div>
                        <div class="btns-wrap">
                            <a href="#" class="btn alt">Смотреть на карте</a>
                            <a href="#" class="btn gray">Предыдущий вариант доски</a>
                        </div>
                    </div>
                </div>
                <div class="item">
                    <a href="#" class="img-wrap">
                        <img src="img/placeholder.png" alt>
                    </a>
                    <div class="desc-wrap">
                        <a href="#" class="title">Памятная доска - Освобождение Новгорода</a>
                        <div class="icon-wrap">
                            <img src="img/svg/calendar.svg" alt>
                            20 Января 1969 года
                        </div>
                        <div class="icon-wrap">
                            <img src="img/svg/marker.svg" alt>
                            Великий Новгород, Новгородский кремль (восточная стена)
                        </div>
                        <div class="desc">
                            Памятная доска установлена на восточной стене Новгородского кремля, рядом со звонницей. Здесь 20 января 1944 года воинами 378-й стрелковой дивизии А.Р. Белова было водружено Красное знамя, как символ освобождения Новгорода от немецко-фашистских захватчиков.
                        </div>
                        <div class="btns-wrap">
                            <a href="#" class="btn alt">Смотреть на карте</a>
                            <a href="#" class="btn gray">Предыдущий вариант доски</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="pagination">
                <a href="#" class="arrow prev">
                    <?= SVG__ARROW_RIGHT ?>
                </a>
                <a href="#" class="page">1</a>
                <a href="#" class="page current">2</a>
                <a href="#" class="page">3</a>
                <a href="#" class="page">4</a>
                <a href="#" class="page">5</a>
                <div class="dots">...</div>
                <a href="#" class="page">9</a>
                <a href="#" class="arrow next">
                    <?= SVG__ARROW_RIGHT ?>
                </a>
            </div>
        </div>
    </div>
</div>

<?php require 'footer.php' ?>