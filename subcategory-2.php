<?php $pageTitle = 'Географические рубрики - подрубрики';

require 'header.php' ?>

<div class="category-page">
    <div class="content-wrap">
        <div class="page-title">
            <p>Географические рубрики</p>
            <p>по районам</p>
            <a href="#" class="round-btn back-btn">
                <?= SVG__ARROW_RIGHT ?>
            </a>
            <div class="round-btn age-limit">12+</div>
        </div>
    </div>
    <div class="img-block"></div>
    <div class="category-block">
        <div class="content-wrap">
            <div class="subcategories-wrap">
                <div class="title">Выберите рубрику:</div>
                <div class="list">
                    <a href="" class="item">Новгородская область</a>
                    <a href="" class="item">Великий Новгород и Новгородский р-н</a>
                    <a href="" class="item">Батецкий и Батецкий р-н</a>
                    <a href="" class="item">Боровичи и Боровичский р-н</a>
                    <a href="" class="item">Валдай и Валдайский р-н</a>
                    <a href="" class="item">Волот и Волотовский р-н</a>
                    <a href="" class="item">Демянск и Демянский р-н</a>
                    <a href="" class="item">Крестцы и Крестецкий р-н</a>
                    <a href="" class="item">Любытино и Любытинский р-н</a>
                    <a href="" class="item">Малая Вишера и Маловишерский р-н</a>
                    <a href="" class="item">Марёво и Маревский р-н</a>
                    <a href="" class="item">Мошенское и Мошенской р-н</a>
                    <a href="" class="item">Окуловка и Окуловский р-н</a>
                    <a href="" class="item">Парфино и Парфинский р-н</a>
                    <a href="" class="item">Пестово и Пестовский р-н</a>
                    <a href="" class="item">Поддорье и Поддорский р-н</a>
                    <a href="" class="item">Сольцы и Солецкий р-н</a>
                    <a href="" class="item">Старая Русса и Старорусский р-н</a>
                    <a href="" class="item">Хвойная и Хвойнинский р-н</a>
                    <a href="" class="item">Холм и Холмский р-н</a>
                    <a href="" class="item">Чудово и Чудовский р-н</a>
                    <a href="" class="item">Шимск и Шимский р-н</a>
                </div>
            </div>
            <div class="items-grid">
                <div class="item">
                    <a href="#" class="img-wrap">
                        <img src="img/placeholder.png" alt>
                    </a>
                    <a href="#" class="title">Новгород</a>
                    <div class="desc">Обзор книги: Кушнир И.И. Новгород. – Л.: Стройиздат, 1967.</div>
                </div>
                <div class="item">
                    <a href="#" class="img-wrap">
                        <img src="img/placeholder.png" alt>
                    </a>
                    <a href="#" class="title">Новгород</a>
                    <div class="desc">Обзор книги: Кушнир И.И. Новгород. – Л.: Стройиздат, 1967.</div>
                </div>
                <div class="item">
                    <a href="#" class="img-wrap">
                        <img src="img/placeholder.png" alt>
                    </a>
                    <a href="#" class="title">Новгород</a>
                    <div class="desc">Обзор книги: Кушнир И.И. Новгород. – Л.: Стройиздат, 1967.</div>
                </div>
                <div class="item">
                    <a href="#" class="img-wrap">
                        <img src="img/placeholder.png" alt>
                    </a>
                    <a href="#" class="title">Новгород</a>
                    <div class="desc">Обзор книги: Кушнир И.И. Новгород. – Л.: Стройиздат, 1967.</div>
                </div>
                <div class="item">
                    <a href="#" class="img-wrap">
                        <img src="img/placeholder.png" alt>
                    </a>
                    <a href="#" class="title">Новгород</a>
                    <div class="desc">Обзор книги: Кушнир И.И. Новгород. – Л.: Стройиздат, 1967.</div>
                </div>
                <div class="item">
                    <a href="#" class="img-wrap">
                        <img src="img/placeholder.png" alt>
                    </a>
                    <a href="#" class="title">Новгород</a>
                    <div class="desc">Обзор книги: Кушнир И.И. Новгород. – Л.: Стройиздат, 1967.</div>
                </div>
                <div class="item">
                    <a href="#" class="img-wrap">
                        <img src="img/placeholder.png" alt>
                    </a>
                    <a href="#" class="title">Новгород</a>
                    <div class="desc">Обзор книги: Кушнир И.И. Новгород. – Л.: Стройиздат, 1967.</div>
                </div>
                <div class="item">
                    <a href="#" class="img-wrap">
                        <img src="img/placeholder.png" alt>
                    </a>
                    <a href="#" class="title">Новгород</a>
                    <div class="desc">Обзор книги: Кушнир И.И. Новгород. – Л.: Стройиздат, 1967.</div>
                </div>
                <div class="item">
                    <a href="#" class="img-wrap">
                        <img src="img/placeholder.png" alt>
                    </a>
                    <a href="#" class="title">Новгород</a>
                    <div class="desc">Обзор книги: Кушнир И.И. Новгород. – Л.: Стройиздат, 1967.</div>
                </div>
                <div class="item">
                    <a href="#" class="img-wrap">
                        <img src="img/placeholder.png" alt>
                    </a>
                    <a href="#" class="title">Новгород</a>
                    <div class="desc">Обзор книги: Кушнир И.И. Новгород. – Л.: Стройиздат, 1967.</div>
                </div>
                <div class="item">
                    <a href="#" class="img-wrap">
                        <img src="img/placeholder.png" alt>
                    </a>
                    <a href="#" class="title">Новгород</a>
                    <div class="desc">Обзор книги: Кушнир И.И. Новгород. – Л.: Стройиздат, 1967.</div>
                </div>
                <div class="item">
                    <a href="#" class="img-wrap">
                        <img src="img/placeholder.png" alt>
                    </a>
                    <a href="#" class="title">Новгород</a>
                    <div class="desc">Обзор книги: Кушнир И.И. Новгород. – Л.: Стройиздат, 1967.</div>
                </div>
                <div class="item">
                    <a href="#" class="img-wrap">
                        <img src="img/placeholder.png" alt>
                    </a>
                    <a href="#" class="title">Новгород</a>
                    <div class="desc">Обзор книги: Кушнир И.И. Новгород. – Л.: Стройиздат, 1967.</div>
                </div>
                <div class="item">
                    <a href="#" class="img-wrap">
                        <img src="img/placeholder.png" alt>
                    </a>
                    <a href="#" class="title">Новгород</a>
                    <div class="desc">Обзор книги: Кушнир И.И. Новгород. – Л.: Стройиздат, 1967.</div>
                </div>
                <div class="item">
                    <a href="#" class="img-wrap">
                        <img src="img/placeholder.png" alt>
                    </a>
                    <a href="#" class="title">Новгород</a>
                    <div class="desc">Обзор книги: Кушнир И.И. Новгород. – Л.: Стройиздат, 1967.</div>
                </div>
                <div class="item">
                    <a href="#" class="img-wrap">
                        <img src="img/placeholder.png" alt>
                    </a>
                    <a href="#" class="title">Новгород</a>
                    <div class="desc">Обзор книги: Кушнир И.И. Новгород. – Л.: Стройиздат, 1967.</div>
                </div>
            </div>
            <div class="pagination">
                <a href="#" class="arrow prev">
                    <?= SVG__ARROW_RIGHT ?>
                </a>
                <a href="#" class="page">1</a>
                <a href="#" class="page current">2</a>
                <a href="#" class="page">3</a>
                <a href="#" class="page">4</a>
                <a href="#" class="page">5</a>
                <div class="dots">...</div>
                <a href="#" class="page">9</a>
                <a href="#" class="arrow next">
                    <?= SVG__ARROW_RIGHT ?>
                </a>
            </div>
        </div>
    </div>
</div>

<?php require 'footer.php' ?>