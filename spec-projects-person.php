<?php $pageTitle = 'Спецпроекты - личность';

require 'header.php' ?>

<div class="spec-projects-person-page">
    <div class="content-wrap">
        <div class="page-title">
            <p>Александр</p>
            <p>князь новгородский</p>
            <a href="#" class="round-btn back-btn">
                <?= SVG__ARROW_RIGHT ?>
            </a>
            <div class="round-btn age-limit">12+</div>
        </div>
        <div class="page-subtitle">1220-1263 гг.</div>
    </div>
    <div class="map" id="map">
        <a href="#" class="objects-btn hide-lg show-sm-flex">Все объекты</a>
        <div class="objects-list">
            <a href="#" class="x hide-lg show-sm-flex">
                <?= SVG__X ?>
            </a>
            <div class="title">Великий Новгород</div>
            <div class="subtitle hide-sm">Список объектов:</div>
            <div class="links-wrap">
                <a href="#" class="item" data-object-id="1">
                    <div class="num">1.</div>
                    Барельеф / ул. Октябрьская, 5
                </a>
                <a href="#" class="item" data-object-id="2">
                    <div class="num">2.</div>
                    Бюст / Октябрьская ул, 5, привокзальная площадь
                </a>
                <a href="#" class="item" data-object-id="3">
                    <div class="num">3.</div>
                    Рельеф-панно / сквер у Киноцентра «Россия» / сквер Воинской Славы (бывшая площадь Карла Маркса)
                </a>
                <a href="#" class="item">
                    <div class="num">4.</div>
                    Горельеф / Кремль
                </a>
                <a href="#" class="item">
                    <div class="num">5.</div>
                    Памятная доска / Кремль, 11 (Софийский собор)
                </a>
                <a href="#" class="item">
                    <div class="num">6.</div>
                    Мост / ул. Федоровский Ручей
                </a>
                <a href="#" class="item">
                    <div class="num">7.</div>
                    Набережная
                </a>
                <a href="#" class="item">
                    <div class="num">8.</div>
                    Памятник / наб. Александра Невского
                </a>
                <a href="#" class="item">
                    <div class="num">9.</div>
                    Церковь / пр. А. Корсунова, 56
                </a>
                <a href="#" class="item" data-object-id="1">
                    <div class="num">10.</div>
                    Барельеф / ул. Октябрьская, 5
                </a>
                <a href="#" class="item" data-object-id="2">
                    <div class="num">11.</div>
                    Бюст / Октябрьская ул, 5, привокзальная площадь
                </a>
                <a href="#" class="item" data-object-id="3">
                    <div class="num">12.</div>
                    Рельеф-панно / сквер у Киноцентра «Россия» / сквер Воинской Славы (бывшая площадь Карла Маркса)
                </a>
                <a href="#" class="item">
                    <div class="num">13.</div>
                    Горельеф / Кремль
                </a>
                <a href="#" class="item">
                    <div class="num">14.</div>
                    Памятная доска / Кремль, 11 (Софийский собор)
                </a>
                <a href="#" class="item">
                    <div class="num">15.</div>
                    Мост / ул. Федоровский Ручей
                </a>
                <a href="#" class="item">
                    <div class="num">16.</div>
                    Набережная
                </a>
                <a href="#" class="item">
                    <div class="num">17.</div>
                    Памятник / наб. Александра Невского
                </a>
                <a href="#" class="item">
                    <div class="num">18.</div>
                    Церковь / пр. А. Корсунова, 56
                </a>
            </div>
        </div>
    </div>
    <div class="bg-wrap">
        <div class="content-wrap">
            <div class="page-title">
                <p>Суворов</p>
                <p>Александр Васильевич</p>
            </div>
            <div class="page-subtitle">1220-1263 гг.</div>
            <div class="img-block">
                <div class="img-wrap">
                    <img src="img/spec-projects-person-page/img-block.png" alt>
                </div>
                <div class="desc-wrap">
                    <p>
                        Великий русский полководец, не проигравший ни одной битвы, национальный герой России. Основоположник отечественной военной теории, автор «Науки побеждать».
                    </p>
                    <p>С его именем тесно связана история Боровичского края. В своем родовом имении в селе Кончанском (в 35 км от г. Боровичи) опальный полководец в 1797-99 гг. отбывал ссылку за саботаж прусских порядков в русской армии. Отсюда 7 февраля 1799 года Суворов отправился в знаменитый Итало-Швейцарский (Альпийский) поход.</p>
                    <p>Заложенный в 1913 году на Екатерининской площади в Боровичах памятник Суворову установить не успели: помешали Первая мировая война и революция. Памятник полководцу установили в Боровичах уже в наше время, а также назвали его именем одну из улиц.</p>
                    <p>В Кончанском-Суворовском действует Музей-усадьба полководца, филиал Новгородского музея-заповедника</p>
                    <a href="#" class="btn alt">Узнать больше</a>
                </div>
            </div>
            <div class="text-block">
                <div class="block-title">
                    <p>Основные</p>
                    <p>вехи биографии</p>
                </div>
                <div class="item">
                    <div class="title">Lorem ipsum dolor sit amet</div>
                    <div class="text">
                        Lorem <a href="#">ipsum</a> dolor sit amet, consectetur adipiscing elit. Nibh imperdiet velit aenean faucibus <a href="#">imperdiet</a> mauris in. Semper phasellus curabitur fringilla duis tincidunt potenti diam nibh nunc. Nisi dignissim scelerisque natoque semper in enim tincidunt tristique pretium. Sit nisl eleifend massa nulla faucibus risus odio habitant senectus. Pulvinar bibendum interdum mauris consequat pellentesque ultrices vestibulum.
                    </div>
                </div>
                <div class="item">
                    <div class="title">Lorem ipsum dolor sit amet</div>
                    <div class="text">
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nibh imperdiet velit aenean faucibus imperdiet mauris in. Semper phasellus curabitur fringilla duis tincidunt potenti diam nibh nunc. Nisi dignissim scelerisque natoque semper in enim tincidunt tristique pretium. Sit nisl eleifend massa nulla faucibus risus odio habitant senectus. Pulvinar bibendum interdum mauris consequat pellentesque ultrices vestibulum.
                    </div>
                </div>
                <div class="item">
                    <div class="title">Lorem ipsum dolor sit amet</div>
                    <div class="text">
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nibh imperdiet velit aenean faucibus imperdiet mauris in. Semper phasellus curabitur fringilla duis tincidunt potenti diam nibh nunc. Nisi dignissim scelerisque natoque semper in enim tincidunt tristique pretium. Sit nisl eleifend massa nulla faucibus risus odio habitant senectus. Pulvinar bibendum interdum mauris consequat pellentesque ultrices vestibulum.
                    </div>
                </div>
                <div class="item">
                    <div class="title">Lorem ipsum dolor sit amet</div>
                    <div class="text">
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nibh imperdiet velit aenean faucibus imperdiet mauris in. Semper phasellus curabitur fringilla duis tincidunt potenti diam nibh nunc. Nisi dignissim scelerisque natoque semper in enim tincidunt tristique pretium. Sit nisl eleifend massa nulla faucibus risus odio habitant senectus. Pulvinar bibendum interdum mauris consequat pellentesque ultrices vestibulum.
                    </div>
                </div>
                <div class="item">
                    <div class="title">Lorem ipsum dolor sit amet</div>
                    <div class="text">
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nibh imperdiet velit aenean faucibus imperdiet mauris in. Semper phasellus curabitur fringilla duis tincidunt potenti diam nibh nunc. Nisi dignissim scelerisque natoque semper in enim tincidunt tristique pretium. Sit nisl eleifend massa nulla faucibus risus odio habitant senectus. Pulvinar bibendum interdum mauris consequat pellentesque ultrices vestibulum.
                    </div>
                </div>
            </div>
            <div class="info-block">
                <div class="subtitle">
                    <p>Ответственный за контент карты г. Боровичи:</p>
                    <p>Яблочкина Людмила Анатольевна,</p>
                </div>
                <div class="text">
                    заведующая отделом информационной и справочно-библиографической работы<br>
                    МБУК «Боровичская городская централизованная библиотечная система»
                </div>
                <div class="subtitle">
                    <p>Модератор:</p>
                    <p>Карпова Татьяна Игоревна,</p>
                </div>
                <div class="text">
                    главный библиотекарь Краеведческой библиотеки<br>
                    МБУК БЦ «Читай-город», Великий Новгород
                </div>
            </div>
        </div>
    </div>
</div>

<script src="js/map.js?v=<?= time() ?>"></script>

<?php require 'footer.php' ?>