<?php $pageTitle = 'Карточка книги';

require 'header.php' ?>

<div class="book-single-page">
    <div class="content-wrap">
        <div class="page-title">
            <p>Cадко -</p>
            <p>народный ансамбль</p>
            <a href="#" class="round-btn back-btn">
                <?= SVG__ARROW_RIGHT ?>
            </a>
            <a href="#" class="round-btn fav-btn">
                <?= SVG__HEART ?>
            </a>
            <div class="round-btn age-limit">12+</div>
        </div>
        <div class="subtitle">
            Болвин П. «Садко» – народный ансамбль / П.А. Болвин, М.Н. Миронов. – Л.: Лениздат, 1983. – 77 с.:
        </div>
        <div class="swiper">
            <div class="swiper-wrapper">
                <div class="swiper-slide">
                    <img src="img/placeholder-darken.png" alt>
                    <div class="title">Подпись</div>
                </div>
                <div class="swiper-slide">
                    <img src="img/placeholder-darken.png" alt>
                    <div class="title">Подпись</div>
                </div>
                <div class="swiper-slide">
                    <img src="img/placeholder-darken.png" alt>
                    <div class="title">Подпись</div>
                </div>
                <div class="swiper-slide">
                    <img src="img/placeholder-darken.png" alt>
                    <div class="title">Подпись</div>
                </div>
                <div class="swiper-slide">
                    <img src="img/placeholder-darken.png" alt>
                    <div class="title">Подпись</div>
                </div>
                <div class="swiper-slide">
                    <img src="img/placeholder-darken.png" alt>
                    <div class="title">Подпись</div>
                </div>
                <div class="swiper-slide">
                    <img src="img/placeholder-darken.png" alt>
                    <div class="title">Подпись</div>
                </div>
                <div class="swiper-slide">
                    <img src="img/placeholder-darken.png" alt>
                    <div class="title">Подпись</div>
                </div>
                <div class="swiper-slide">
                    <img src="img/placeholder-darken.png" alt>
                    <div class="title">Подпись</div>
                </div>
            </div>
            <div class="swiper-scrollbar"></div>
        </div>
        <div class="slider-btns">
            <a href="#" class="round-btn slider-btn prev">
                <?= SVG__ARROW_RIGHT ?>
            </a>
            <a href="#" class="round-btn slider-btn next">
                <?= SVG__ARROW_RIGHT ?>
            </a>
        </div>
        <div class="more-wrap">
            <a href="#" class="dd-head">
                Скрыть сведения об авторе обзора
                <?= SVG__ARROW_TOP_RIGHT ?>
            </a>
            <div class="dd-content">
                <div class="columns-wrap">
                    <div class="column">
                        <div class="title">Автор материала:</div>
                        <div class="author">
                            <div class="img-wrap">
                                <img src="img/placeholder-darken.png" alt>
                            </div>
                            <div class="name">Татьяна Карпова</div>
                        </div>
                        <div class="text">МБУК "БЦ "Читай-город"</div>
                        <div class="text">2021-08-10 19:52:43 (Offline)</div>
                        <a href="#" class="profile-link">Смотреть профиль</a>
                    </div>
                    <div class="column">
                        <div class="title">Модератор:</div>
                        <div class="author">
                            <div class="img-wrap">
                                <img src="img/placeholder-darken.png" alt>
                            </div>
                            <div class="name">Татьяна Карпова</div>
                        </div>
                        <div class="text">МБУК "БЦ "Читай-город"</div>
                        <div class="text">2021-08-10 19:52:43 (Offline)</div>
                        <a href="#" class="profile-link">Смотреть профиль</a>
                    </div>
                    <div class="column">
                        <div class="title">Дата публикации:</div>
                        <div class="date">06.08.2021</div>
                    </div>
                    <div class="column">
                        <div class="title">Последнее обновление:</div>
                        <div class="date">06.08.2021</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="about-block">
        <div class="content-wrap">
            <div class="categories-wrap">
                <div class="block-title">Рубрики</div>
                <div class="categories-list">
                    <a href="#" class="item">История и культура</a>
                    <a href="#" class="item">Художественная самодеятельность</a>
                    <a href="#" class="item">Валдайская район</a>
                    <a href="#" class="item">Танец(хореография)</a>
                    <a href="#" class="item">Промышленные предприятия</a>
                    <a href="#" class="item">Промышленность</a>
                    <a href="#" class="item">Промышленность советского периода</a>
                    <a href="#" class="item">Культурно-досуговая деятельность</a>
                </div>
            </div>
            <div class="about-wrap">
                <div class="block-title">О книге <button class="btn alt">Полный текст книги</button></div>
                <div class="text">
                    Созданный в 1966 г. из рабочих новгородского завода им. Ленинского комсомола (затем ПО «Планета»), ансамбль в скором времени приобрел всесоюзную известность. Мастерству самодеятельных артистов, граничащему с профессионализмом, рукоплескали зрители Новгорода и области, городов центральной России, Сибири, Средней Азии, Венгрии, Дании, Румынии, Финляндии, далекой Африки. «Садко» официально представлял Новгород в культурных программах Олимпиады-80 и Международного фестиваля молодежи и студентов в Москве. Коллектив являлся лауреатом всесоюзных смотров, конкурсов и фестивалей народного творчества, участником ВДНХ СССР, лауреатом премии Ленинского комсомола.<br>
                    <br>
                    Страницы книги и многочисленные фотографии возвращают нас к временам громкой славы ансамбля «Садко», повествуют об участниках ансамбля, концертных программах, многочисленных гастролях по стране и за рубежом, о всемерной помощи, которую оказывал садковцам родной завод.<br>
                    <br>
                    А еще – о благодарной любви новгородцев-зрителей, гордящихся своим ансамблем. Как признавался один из новгородских зрителей, «наряды были такие яркие, что если бы и хор не пел, и танцоры не плясали, а только взяли бы и прошлись в своих изумительных костюмах перед нами – и то можно было бы благодарить «Садко за доставленную радость народного праздника!».<br>
                    <br>
                    Завершают рассказ о прославленном коллективе строки новгородского поэта Анатолия Бухвалова:<br>
                    <br>
                    От родного завода пошла далеко<br>
                    Слава песен и танцев ансамбля «Садко»…<br>
                    Это русская сила, и русский размах,<br>
                    Это русская нежность в душевных словах.<br>
                    Это русская пляска, русская удаль,<br>
                    Это русское сердце, раскрытое людям!
                </div>
            </div>
        </div>
    </div>
</div>

<?php require 'footer.php' ?>