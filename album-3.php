<?php $pageTitle = 'Новгородский альбом победителей';

require 'header.php' ?>

<div class="album-page">
    <div class="content-wrap">
        <div class="page-title">
            <p>Новгородский</p>
            <p>альбом победителей</p>
            <a href="#" class="round-btn back-btn">
                <?= SVG__ARROW_RIGHT ?>
            </a>
            <div class="round-btn age-limit">12+</div>
        </div>
    </div>
    <div class="desc-block">
        <img src="img/album-page/bg.png" alt class="bg">
        <div class="content-wrap">
            <div class="text">
                В Великом Новгороде периодически проходят акции памяти, в рамках которых люди делятся фотографиями своих родных – участников Великой Отечественной войны. Если снимки остаются у организаторов, то их передают в городскую Краеведческую библиотеку – где владельцы могут в любой момент получить их обратно. Забирая своего Героя домой, многие душевно рассказывают о нем: кем был до войны, где и как служил, о чем мечтал... Все, что помнят… Из воспоминаний и семейных альбомов новгородцев как-то сам собой сложился трогательный, один на всех «Новгородский альбом победителей».
            </div>
            <div class="swiper">
                <div class="swiper-wrapper">
                    <div class="swiper-slide">
                        <img src="img/placeholder.png" alt>
                        <div class="title">Аксеновы (семья)</div>
                    </div>
                    <div class="swiper-slide">
                        <img src="img/placeholder.png" alt>
                        <div class="title">Аксеновы (семья)</div>
                    </div>
                    <div class="swiper-slide">
                        <img src="img/placeholder.png" alt>
                        <div class="title">Аксеновы (семья)</div>
                    </div>
                    <div class="swiper-slide">
                        <img src="img/placeholder.png" alt>
                        <div class="title">Аксеновы (семья)</div>
                    </div>
                    <div class="swiper-slide">
                        <img src="img/placeholder.png" alt>
                        <div class="title">Аксеновы (семья)</div>
                    </div>
                    <div class="swiper-slide">
                        <img src="img/placeholder.png" alt>
                        <div class="title">Аксеновы (семья)</div>
                    </div>
                </div>
                <div class="swiper-scrollbar"></div>
            </div>
            <div class="btns-wrap">
                <a href="#" class="round-btn prev">
                    <?= SVG__ARROW_RIGHT ?>
                </a>
                <a href="#" class="round-btn next">
                    <?= SVG__ARROW_RIGHT ?>
                </a>
            </div>
            <a href="#" class="btn alt more-btn">Смотреть альбом</a>
        </div>
    </div>
    <div class="gallery-block">
        <div class="content-wrap">
            <div class="page-title">
                <p>галерея альбома</p>
            </div>
            <div class="text">
                В отдельной галерее мы разместили фото фронтовиков, по которым не было предоставлено дополнительных сведений… О них мы знаем только то, что было написано на самих снимках. Посмотрите – может, там есть и ваш герой? Если так, мы ждем вас – можно принести дополнительные фотографии и документы, рассказать известную вам информацию. И герой займет свое почетное место в народном «Новгородском альбоме победителей».
            </div>
            <div class="swiper">
                <div class="swiper-wrapper">
                    <div class="swiper-slide">
                        <img src="img/placeholder.png" alt>
                        <div class="title">Абакумов М.Н.</div>
                    </div>
                    <div class="swiper-slide">
                        <img src="img/placeholder.png" alt>
                        <div class="title">Абакумов М.Н.</div>
                    </div>
                    <div class="swiper-slide">
                        <img src="img/placeholder.png" alt>
                        <div class="title">Абакумов М.Н.</div>
                    </div>
                    <div class="swiper-slide">
                        <img src="img/placeholder.png" alt>
                        <div class="title">Абакумов М.Н.</div>
                    </div>
                    <div class="swiper-slide">
                        <img src="img/placeholder.png" alt>
                        <div class="title">Абакумов М.Н.</div>
                    </div>
                    <div class="swiper-slide">
                        <img src="img/placeholder.png" alt>
                        <div class="title">Абакумов М.Н.</div>
                    </div>
                </div>
                <div class="swiper-scrollbar"></div>
            </div>
            <div class="btns-wrap">
                <a href="#" class="round-btn prev">
                    <?= SVG__ARROW_RIGHT ?>
                </a>
                <a href="#" class="round-btn next">
                    <?= SVG__ARROW_RIGHT ?>
                </a>
            </div>
            <a href="#" class="btn alt more-btn">Смотреть галерею</a>
        </div>
    </div>
</div>

<?php require 'footer.php' ?>