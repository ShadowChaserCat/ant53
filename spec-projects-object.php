<?php $pageTitle = 'Спецпроекты - объект';

require 'header.php' ?>

<div class="spec-projects-object-page">
    <div class="content-wrap">
        <div class="page-title">
            <p>Бюст</p>
            <p>Александра Невского</p>
            <a href="#" class="round-btn back-btn">
                <?= SVG__ARROW_RIGHT ?>
            </a>
            <a href="#" class="round-btn fav-btn"><?= SVG__HEART ?></a>
            <div class="round-btn age-limit">12+</div>
        </div>
        <div class="page-subtitle">1220-1263 гг.</div>
    </div>
    <div class="map" id="map">
        <a href="#" class="objects-btn hide-lg show-sm-flex">Все объекты</a>
        <div class="objects-list">
            <a href="#" class="x hide-lg show-sm-flex">
                <?= SVG__X ?>
            </a>
            <div class="title">Великий Новгород</div>
            <div class="subtitle hide-sm">Список объектов:</div>
            <div class="links-wrap">
                <a href="#" class="item" data-object-id="1">
                    <div class="num">1.</div>
                    Барельеф / ул. Октябрьская, 5
                </a>
                <a href="#" class="item" data-object-id="2">
                    <div class="num">2.</div>
                    Бюст / Октябрьская ул, 5, привокзальная площадь
                </a>
                <a href="#" class="item" data-object-id="3">
                    <div class="num">3.</div>
                    Рельеф-панно / сквер у Киноцентра «Россия» / сквер Воинской Славы (бывшая площадь Карла Маркса)
                </a>
                <a href="#" class="item">
                    <div class="num">4.</div>
                    Горельеф / Кремль
                </a>
                <a href="#" class="item">
                    <div class="num">5.</div>
                    Памятная доска / Кремль, 11 (Софийский собор)
                </a>
                <a href="#" class="item">
                    <div class="num">6.</div>
                    Мост / ул. Федоровский Ручей
                </a>
                <a href="#" class="item">
                    <div class="num">7.</div>
                    Набережная
                </a>
                <a href="#" class="item">
                    <div class="num">8.</div>
                    Памятник / наб. Александра Невского
                </a>
                <a href="#" class="item">
                    <div class="num">9.</div>
                    Церковь / пр. А. Корсунова, 56
                </a>
                <a href="#" class="item" data-object-id="1">
                    <div class="num">10.</div>
                    Барельеф / ул. Октябрьская, 5
                </a>
                <a href="#" class="item" data-object-id="2">
                    <div class="num">11.</div>
                    Бюст / Октябрьская ул, 5, привокзальная площадь
                </a>
                <a href="#" class="item" data-object-id="3">
                    <div class="num">12.</div>
                    Рельеф-панно / сквер у Киноцентра «Россия» / сквер Воинской Славы (бывшая площадь Карла Маркса)
                </a>
                <a href="#" class="item">
                    <div class="num">13.</div>
                    Горельеф / Кремль
                </a>
                <a href="#" class="item">
                    <div class="num">14.</div>
                    Памятная доска / Кремль, 11 (Софийский собор)
                </a>
                <a href="#" class="item">
                    <div class="num">15.</div>
                    Мост / ул. Федоровский Ручей
                </a>
                <a href="#" class="item">
                    <div class="num">16.</div>
                    Набережная
                </a>
                <a href="#" class="item">
                    <div class="num">17.</div>
                    Памятник / наб. Александра Невского
                </a>
                <a href="#" class="item">
                    <div class="num">18.</div>
                    Церковь / пр. А. Корсунова, 56
                </a>
            </div>
        </div>
    </div>
    <div class="bg-wrap">
        <div class="content-wrap">
            <div class="links-wrap">
                <a href="#" class="btn alt" data-tab="0">Аудиогид</a>
                <a href="#" class="btn gray" data-tab="1">Панорама</a>
                <a href="#" class="btn dark">Историческая справка</a>
            </div>
            <div class="tab-content">
                <div class="item">
                    <a href="#" class="img-wrap">
                        <img src="img/placeholder-295x205.png" alt>
                    </a>
                    <div class="desc-wrap">
                        <a href="#" class="title">Куличный луг</a>
                        <div class="desc">
                            Памятник природы регионального значения. Расположен в Старорусском районе Новгородской области. Фото предоставлено краеведом г. Старая Русса Павлом Володиным, через социальную сеть VK.<br>
                            Дата съемки: 13.07.2011.
                        </div>
                        <div class="player-wrap">
                            <div class="time-wrap">
                                <div class="current">00:00</div>
                                <div class="total">04:30</div>
                            </div>
                            <div class="progress-bar"></div>
                        </div>
                        <div class="btns-wrap">
                            <a href="#" class="btn alt">Слушать</a>
                            <a href="#" class="round-btn fav-btn">
                                <?= SVG__HEART ?>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="tab-content">
                <div class="item">
                    <a href="#" class="img-wrap">
                        <img src="img/placeholder-295x205.png" alt>
                    </a>
                    <div class="desc-wrap">
                        <a href="#" class="title">Панорама</a>
                        <div class="desc">
                            Памятник природы регионального значения. Расположен в Старорусском районе Новгородской области. Фото предоставлено краеведом г. Старая Русса Павлом Володиным, через социальную сеть VK.<br>
                            Дата съемки: 13.07.2011.
                        </div>
                        <div class="btns-wrap">
                            <a href="#" class="btn alt">Смотреть</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="text">
                <p>Памятник Александру Васильевичу Суворову расположен в небольшом сквере на площади Володарского, недалеко от автомобильного моста через реку Мсту.</p>
                <p>Скульптура полководца выполнена из бронзового сплава и установлена на мраморном постаменте. Высота всей композиции составляет 4,5 метра.</p>
                <p class="italic">Памятник соответствует классическому образу полководца. Худощавый фельдмаршал, с императорским рескриптом в руке о прибытии в войска после Кончанской ссылки, стоит в окружении старинных зданий XIX века. Дорога рядом с площадью ведет к его усадьбе в селе Кончанском – туда устремлен взгляд бронзового Суворова.</p>
                <p>Первоначально памятник был установлен в парке 30-летия Октября. В 1998 году, в преддверии 200-летия суворовского Альпийского похода, парковая скульптура была отреставрирована в полноценный памятник и перенесена в центр города.</p>
            </div>
            <div class="items-wrap">
                <a href="#" class="item">
                    <div class="img-wrap">
                        <img src="img/spec-projects-object-page/img-1.png" alt>
                    </div>
                    <div class="title">Памятник А.В. Суворову</div>
                </a>
                <a href="#" class="item">
                    <div class="img-wrap">
                        <img src="img/spec-projects-object-page/img-2.png" alt>
                    </div>
                    <div class="title">Памятник А.В. Суворову</div>
                </a>
                <a href="#" class="item">
                    <div class="img-wrap">
                        <img src="img/spec-projects-object-page/img-3.png" alt>
                    </div>
                    <div class="title">Памятник А.В. Суворову</div>
                </a>
            </div>
        </div>
    </div>
</div>

<script src="js/map.js?v=<?= time() ?>"></script>

<?php require 'footer.php' ?>