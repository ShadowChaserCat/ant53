<?php $pageTitle = 'Тесты';

require 'header.php' ?>

<div class="tests-page">
    <div class="content-wrap">
        <div class="page-title">
            <a href="#" class="round-btn back-btn">
                <?= SVG__ARROW_RIGHT ?>
            </a>
            <p>Тесты</p>
        </div>
        <div class="items-wrap">
            <a href="#" class="item">
                <img src="img/spec-projects-page/item-1.png" alt>
                <div class="text-overlay">
                    <div class="title">Рюрик: правда или вымысел</div>
                </div>
            </a>
            <a href="#" class="item">
                <img src="img/spec-projects-page/item-2.png" alt>
                <div class="text-overlay">
                    <div class="title">Рюрик: правда или вымысел</div>
                </div>
            </a>
            <a href="#" class="item">
                <img src="img/spec-projects-page/item-3.png" alt>
                <div class="text-overlay">
                    <div class="title">Рюрик: правда или вымысел</div>
                </div>
            </a>
            <a href="#" class="item">
                <img src="img/spec-projects-page/item-4.png" alt>
                <div class="text-overlay">
                    <div class="title">Рюрик: правда или вымысел</div>
                </div>
            </a>
            <a href="#" class="item">
                <img src="img/spec-projects-page/item-1.png" alt>
                <div class="text-overlay">
                    <div class="title">Рюрик: правда или вымысел</div>
                </div>
            </a>
            <a href="#" class="item">
                <img src="img/spec-projects-page/item-2.png" alt>
                <div class="text-overlay">
                    <div class="title">Рюрик: правда или вымысел</div>
                </div>
            </a>
            <a href="#" class="item">
                <img src="img/spec-projects-page/item-3.png" alt>
                <div class="text-overlay">
                    <div class="title">Рюрик: правда или вымысел</div>
                </div>
            </a>
            <a href="#" class="item">
                <img src="img/spec-projects-page/item-4.png" alt>
                <div class="text-overlay">
                    <div class="title">Рюрик: правда или вымысел</div>
                </div>
            </a>
        </div>
        <div class="pagination">
            <a href="#" class="arrow prev">
                <?= SVG__ARROW_RIGHT ?>
            </a>
            <a href="#" class="page">1</a>
            <a href="#" class="page current">2</a>
            <a href="#" class="page">3</a>
            <a href="#" class="page">4</a>
            <a href="#" class="page">5</a>
            <div class="dots">...</div>
            <a href="#" class="page">9</a>
            <a href="#" class="arrow next">
                <?= SVG__ARROW_RIGHT ?>
            </a>
        </div>
    </div>
</div>

<?php require 'footer.php' ?>