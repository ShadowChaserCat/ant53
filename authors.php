<?php $pageTitle = 'Авторы';

require 'header.php' ?>

<div class="authors-page">
    <div class="content-wrap">
        <div class="page-title">
            <p>Авторы</p>
            <a href="#" class="round-btn back-btn">
                <?= SVG__ARROW_RIGHT ?>
            </a>
        </div>
        <div class="desc-wrap">
            <div class="text">
                Здесь можно увидеть всех, кто когда-либо добавлял на сайт свои материалы – передавал или самостоятельно заливал фотографии и другие файлы, создавал свои информационные коллекции. Мы уважаем и ценим вклад каждого участника проекта, и вспоминаем всех с благодарностью. Спасибо вам! За то, что сделали нашу общую цифровую библиотеку лучше.
            </div>
            <a href="#" class="btn alt">Стать автором</a>
        </div>
        <div class="items-grid">
            <a href="#" class="item">
                <div class="img-wrap">
                    <img src="img/placeholder-180x180.png" alt>
                </div>
                <div class="name">Антон Петров</div>
            </a>
            <a href="#" class="item">
                <div class="img-wrap">
                    <img src="img/placeholder-180x180.png" alt>
                </div>
                <div class="name">Антон Петров</div>
            </a>
            <a href="#" class="item">
                <div class="img-wrap">
                    <img src="img/placeholder-180x180.png" alt>
                </div>
                <div class="name">Антон Петров</div>
            </a>
            <a href="#" class="item">
                <div class="img-wrap">
                    <img src="img/placeholder-180x180.png" alt>
                </div>
                <div class="name">Антон Петров</div>
            </a>
            <a href="#" class="item">
                <div class="img-wrap">
                    <img src="img/placeholder-180x180.png" alt>
                </div>
                <div class="name">Антон Петров</div>
            </a>
            <a href="#" class="item">
                <div class="img-wrap">
                    <img src="img/placeholder-180x180.png" alt>
                </div>
                <div class="name">Антон Петров</div>
            </a>
            <a href="#" class="item">
                <div class="img-wrap">
                    <img src="img/placeholder-180x180.png" alt>
                </div>
                <div class="name">Антон Петров</div>
            </a>
            <a href="#" class="item">
                <div class="img-wrap">
                    <img src="img/placeholder-180x180.png" alt>
                </div>
                <div class="name">Антон Петров</div>
            </a>
            <a href="#" class="item">
                <div class="img-wrap">
                    <img src="img/placeholder-180x180.png" alt>
                </div>
                <div class="name">Антон Петров</div>
            </a>
            <a href="#" class="item">
                <div class="img-wrap">
                    <img src="img/placeholder-180x180.png" alt>
                </div>
                <div class="name">Антон Петров</div>
            </a>
            <a href="#" class="item">
                <div class="img-wrap">
                    <img src="img/placeholder-180x180.png" alt>
                </div>
                <div class="name">Антон Петров</div>
            </a>
            <a href="#" class="item">
                <div class="img-wrap">
                    <img src="img/placeholder-180x180.png" alt>
                </div>
                <div class="name">Антон Петров</div>
            </a>
            <a href="#" class="item">
                <div class="img-wrap">
                    <img src="img/placeholder-180x180.png" alt>
                </div>
                <div class="name">Антон Петров</div>
            </a>
            <a href="#" class="item">
                <div class="img-wrap">
                    <img src="img/placeholder-180x180.png" alt>
                </div>
                <div class="name">Антон Петров</div>
            </a>
            <a href="#" class="item">
                <div class="img-wrap">
                    <img src="img/placeholder-180x180.png" alt>
                </div>
                <div class="name">Антон Петров</div>
            </a>
            <a href="#" class="item">
                <div class="img-wrap">
                    <img src="img/placeholder-180x180.png" alt>
                </div>
                <div class="name">Антон Петров</div>
            </a>
            <a href="#" class="item">
                <div class="img-wrap">
                    <img src="img/placeholder-180x180.png" alt>
                </div>
                <div class="name">Антон Петров</div>
            </a>
            <a href="#" class="item">
                <div class="img-wrap">
                    <img src="img/placeholder-180x180.png" alt>
                </div>
                <div class="name">Антон Петров</div>
            </a>
            <a href="#" class="item">
                <div class="img-wrap">
                    <img src="img/placeholder-180x180.png" alt>
                </div>
                <div class="name">Антон Петров</div>
            </a>
            <a href="#" class="item">
                <div class="img-wrap">
                    <img src="img/placeholder-180x180.png" alt>
                </div>
                <div class="name">Антон Петров</div>
            </a>
        </div>
        <div class="pagination">
            <a href="#" class="arrow prev">
                <?= SVG__ARROW_RIGHT ?>
            </a>
            <a href="#" class="page">1</a>
            <a href="#" class="page current">2</a>
            <a href="#" class="page">3</a>
            <a href="#" class="page">4</a>
            <a href="#" class="page">5</a>
            <div class="dots">...</div>
            <a href="#" class="page">9</a>
            <a href="#" class="arrow next">
                <?= SVG__ARROW_RIGHT ?>
            </a>
        </div>
    </div>
</div>

<?php require 'footer.php' ?>