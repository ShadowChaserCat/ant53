<?php $pageTitle = 'Календарь знаменательных дат';

require 'header.php' ?>

<div class="calendar-page">
    <div class="content-wrap">
        <div class="page-title">
            <p>Календарь</p>
            <p>знаменательных дат</p>
            <a href="#" class="round-btn back-btn">
                <?= SVG__ARROW_RIGHT ?>
            </a>
        </div>
        <div class="calendar-wrap">
            <div class="calendar">
                <div class="month">Август</div>
                <div class="days-grid">
                    <a href="#" class="day inactive">26</a>
                    <a href="#" class="day inactive">27</a>
                    <a href="#" class="day inactive">28</a>
                    <a href="#" class="day inactive">29</a>
                    <a href="#" class="day inactive">30</a>
                    <a href="#" class="day inactive">31</a>
                    <a href="#" class="day">1</a>
                    <a href="#" class="day">2</a>
                    <a href="#" class="day">3</a>
                    <a href="#" class="day">4</a>
                    <a href="#" class="day">5</a>
                    <a href="#" class="day">6</a>
                    <a href="#" class="day">8</a>
                    <a href="#" class="day">9</a>
                    <a href="#" class="day">9</a>
                    <a href="#" class="day">10</a>
                    <a href="#" class="day">11</a>
                    <a href="#" class="day">12</a>
                    <a href="#" class="day">13</a>
                    <a href="#" class="day">14</a>
                    <a href="#" class="day">15</a>
                    <a href="#" class="day">16</a>
                    <a href="#" class="day">17</a>
                    <a href="#" class="day">18</a>
                    <a href="#" class="day">19</a>
                    <a href="#" class="day">20</a>
                    <a href="#" class="day">21</a>
                    <a href="#" class="day">22</a>
                    <a href="#" class="day">23</a>
                    <a href="#" class="day">24</a>
                    <a href="#" class="day">25</a>
                    <a href="#" class="day">26</a>
                    <a href="#" class="day">27</a>
                    <a href="#" class="day current">28</a>
                    <a href="#" class="day">29</a>
                    <a href="#" class="day">30</a>
                    <a href="#" class="day inactive">1</a>
                    <a href="#" class="day inactive">2</a>
                    <a href="#" class="day inactive">3</a>
                    <a href="#" class="day inactive">4</a>
                    <a href="#" class="day inactive">5</a>
                    <a href="#" class="day inactive">6</a>
                </div>
            </div>
            <div class="date-select">
                <div class="title">Отбор событий:</div>
                <div class="select-wrap">
                    <select name="year">
                        <option selected disabled>Год</option>
                        <option value="2015">2015</option>
                        <option value="2016">2016</option>
                        <option value="2017">2017</option>
                        <option value="2018">2018</option>
                        <option value="2019">2019</option>
                        <option value="2020">2020</option>
                        <option value="2021">2021</option>
                    </select>
                    <img src="img/svg/chevron-d.svg" alt>
                </div>
                <div class="select-wrap">
                    <select name="month">
                        <option selected disabled>Месяц</option>
                        <option value="1">Январь</option>
                        <option value="2">Февраль</option>
                        <option value="3">Март</option>
                        <option value="4">Апрель</option>
                        <option value="5">Май</option>
                        <option value="6">Июнь</option>
                        <option value="7">Июль</option>
                        <option value="8">Август</option>
                        <option value="9">Сентябрь</option>
                        <option value="10">Октябрь</option>
                        <option value="11">Ноябрь</option>
                        <option value="12">Декабрь</option>
                    </select>
                    <img src="img/svg/chevron-d.svg" alt>
                </div>
                <div class="select-wrap">
                    <select name="day">
                        <option selected disabled>День</option>
                        <?php for ( $i = 1; $i < 31; $i++ ) { ?>
                            <option value="<?= $i ?>"><?= $i ?></option>
                        <?php } ?>
                    </select>
                    <img src="img/svg/chevron-d.svg" alt>
                </div>
                <div class="btns-wrap">
                    <a href="#" class="btn alt">Применить</a>
                    <a href="#" class="btn gray">Сбросить</a>
                </div>
            </div>
        </div>
        <div class="items-list">
            <div class="item">
                <a href="#" class="img-wrap">
                    <img src="img/placeholder-295x270.png" alt>
                </a>
                <div class="desc-wrap">
                    <a href="#" class="title">01 января 2021</a>
                    <div class="desc">
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pretium eget cras a, eu egestas tristique. Pellentesque cras quam senectus gravida pretium vel neque. Velit vel blandit tempor ipsum dictum sit platea quis aliquam. Lacus rhoncus purus et cursus pellentesque lobortis ac. Egestas sit at porttitor elementum dictumst consectetur. Id tortor est vestibulum dui dignissim in nibh. Tellus viverra posuere arcu cursus urna, vitae. Quam eget diam etiam eu quis.
                    </div>
                    <div class="btns-wrap">
                        <a href="#" class="btn alt">Смотреть</a>
                        <div class="round-btn age-limit">12+</div>
                    </div>
                </div>
            </div>
            <div class="item">
                <a href="#" class="img-wrap">
                    <img src="img/placeholder-295x270.png" alt>
                </a>
                <div class="desc-wrap">
                    <a href="#" class="title">01 января 2021</a>
                    <div class="desc">
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pretium eget cras a, eu egestas tristique. Pellentesque cras quam senectus gravida pretium vel neque. Velit vel blandit tempor ipsum dictum sit platea quis aliquam. Lacus rhoncus purus et cursus pellentesque lobortis ac. Egestas sit at porttitor elementum dictumst consectetur. Id tortor est vestibulum dui dignissim in nibh. Tellus viverra posuere arcu cursus urna, vitae. Quam eget diam etiam eu quis.
                    </div>
                    <div class="btns-wrap">
                        <a href="#" class="btn alt">Смотреть</a>
                        <div class="round-btn age-limit">12+</div>
                    </div>
                </div>
            </div>
            <div class="item">
                <a href="#" class="img-wrap">
                    <img src="img/placeholder-295x270.png" alt>
                </a>
                <div class="desc-wrap">
                    <a href="#" class="title">01 января 2021</a>
                    <div class="desc">
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pretium eget cras a, eu egestas tristique. Pellentesque cras quam senectus gravida pretium vel neque. Velit vel blandit tempor ipsum dictum sit platea quis aliquam. Lacus rhoncus purus et cursus pellentesque lobortis ac. Egestas sit at porttitor elementum dictumst consectetur. Id tortor est vestibulum dui dignissim in nibh. Tellus viverra posuere arcu cursus urna, vitae. Quam eget diam etiam eu quis.
                    </div>
                    <div class="btns-wrap">
                        <a href="#" class="btn alt">Смотреть</a>
                        <div class="round-btn age-limit">12+</div>
                    </div>
                </div>
            </div>
            <div class="item">
                <a href="#" class="img-wrap">
                    <img src="img/placeholder-295x270.png" alt>
                </a>
                <div class="desc-wrap">
                    <a href="#" class="title">01 января 2021</a>
                    <div class="desc">
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pretium eget cras a, eu egestas tristique. Pellentesque cras quam senectus gravida pretium vel neque. Velit vel blandit tempor ipsum dictum sit platea quis aliquam. Lacus rhoncus purus et cursus pellentesque lobortis ac. Egestas sit at porttitor elementum dictumst consectetur. Id tortor est vestibulum dui dignissim in nibh. Tellus viverra posuere arcu cursus urna, vitae. Quam eget diam etiam eu quis.
                    </div>
                    <div class="btns-wrap">
                        <a href="#" class="btn alt">Смотреть</a>
                        <div class="round-btn age-limit">12+</div>
                    </div>
                </div>
            </div>
            <div class="item">
                <a href="#" class="img-wrap">
                    <img src="img/placeholder-295x270.png" alt>
                </a>
                <div class="desc-wrap">
                    <a href="#" class="title">01 января 2021</a>
                    <div class="desc">
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pretium eget cras a, eu egestas tristique. Pellentesque cras quam senectus gravida pretium vel neque. Velit vel blandit tempor ipsum dictum sit platea quis aliquam. Lacus rhoncus purus et cursus pellentesque lobortis ac. Egestas sit at porttitor elementum dictumst consectetur. Id tortor est vestibulum dui dignissim in nibh. Tellus viverra posuere arcu cursus urna, vitae. Quam eget diam etiam eu quis.
                    </div>
                    <div class="btns-wrap">
                        <a href="#" class="btn alt">Смотреть</a>
                        <div class="round-btn age-limit">12+</div>
                    </div>
                </div>
            </div>
            <div class="item">
                <a href="#" class="img-wrap">
                    <img src="img/placeholder-295x270.png" alt>
                </a>
                <div class="desc-wrap">
                    <a href="#" class="title">01 января 2021</a>
                    <div class="desc">
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pretium eget cras a, eu egestas tristique. Pellentesque cras quam senectus gravida pretium vel neque. Velit vel blandit tempor ipsum dictum sit platea quis aliquam. Lacus rhoncus purus et cursus pellentesque lobortis ac. Egestas sit at porttitor elementum dictumst consectetur. Id tortor est vestibulum dui dignissim in nibh. Tellus viverra posuere arcu cursus urna, vitae. Quam eget diam etiam eu quis.
                    </div>
                    <div class="btns-wrap">
                        <a href="#" class="btn alt">Смотреть</a>
                        <div class="round-btn age-limit">12+</div>
                    </div>
                </div>
            </div>
        </div>
        <div class="pagination">
            <a href="#" class="arrow prev">
                <?= SVG__ARROW_RIGHT ?>
            </a>
            <a href="#" class="page">1</a>
            <a href="#" class="page current">2</a>
            <a href="#" class="page">3</a>
            <a href="#" class="page">4</a>
            <a href="#" class="page">5</a>
            <div class="dots">...</div>
            <a href="#" class="page">9</a>
            <a href="#" class="arrow next">
                <?= SVG__ARROW_RIGHT ?>
            </a>
        </div>
    </div>
</div>

<?php require 'footer.php' ?>