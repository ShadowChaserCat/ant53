<?php $pageTitle = 'Реестр мемориальных досок';

require 'header.php' ?>

<div class="registry-page">
    <div class="content-wrap">
        <div class="page-title">
            <p>Реестр</p>
            <p>мемориальных досок</p>
            <a href="#" class="round-btn back-btn"><?= SVG__ARROW_RIGHT ?></a>
            <div class="round-btn age-limit">12+</div>
        </div>
        <div class="tabs-wrap">
            <a href="#" class="tab current">Люди</a>
            <a href="#" class="tab">События</a>
            <a href="#" class="tab">Информационные доски</a>
        </div>
    </div>
    <div class="table-wrap">
        <div class="content-wrap">
            <div class="tab-content">
                <div class="table-head">
                    <div class="cell">Наименование</div>
                    <div class="cell">Расположение</div>
                    <div class="cell">Дата открытия</div>
                </div>
                <div class="table-row">
                    <div class="cell">Групповая Мемориальная доска</div>
                    <div class="cell">Великий Новгород, район Антоново,1; старый корпус Гуманитарного институра НовГУ(внутри здания)</div>
                    <div class="cell"></div>
                </div>
                <div class="table-row">
                    <div class="cell">Мемориальная доска Абрамовой И.А.</div>
                    <div class="cell">Великий Новгород, Воскресенский бульвар, д. 1 </div>
                    <div class="cell">18 марта 2011 года</div>
                </div>
                <div class="table-row">
                    <div class="cell">Мемориальная доска Абрамовой И.А.</div>
                    <div class="cell">Великий Новгород, Воскресенский бульвар, д. 1 </div>
                    <div class="cell">18 марта 2011 года</div>
                </div>
                <div class="table-row">
                    <div class="cell">Мемориальная доска Абрамовой И.А.</div>
                    <div class="cell">Великий Новгород, Воскресенский бульвар, д. 1 </div>
                    <div class="cell">18 марта 2011 года</div>
                </div>
                <div class="table-row">
                    <div class="cell">Мемориальная доска Абрамовой И.А.</div>
                    <div class="cell">Великий Новгород, Воскресенский бульвар, д. 1 </div>
                    <div class="cell">18 марта 2011 года</div>
                </div>
                <div class="table-row">
                    <div class="cell">Мемориальная доска Абрамовой И.А.</div>
                    <div class="cell">Великий Новгород, Воскресенский бульвар, д. 1 </div>
                    <div class="cell">18 марта 2011 года</div>
                </div>
                <div class="table-row">
                    <div class="cell">Мемориальная доска Абрамовой И.А.</div>
                    <div class="cell">Великий Новгород, Воскресенский бульвар, д. 1 </div>
                    <div class="cell">18 марта 2011 года</div>
                </div>
                <div class="table-row">
                    <div class="cell">Мемориальная доска Абрамовой И.А.</div>
                    <div class="cell">Великий Новгород, Воскресенский бульвар, д. 1 </div>
                    <div class="cell">18 марта 2011 года</div>
                </div>
                <div class="table-row">
                    <div class="cell">Мемориальная доска Абрамовой И.А.</div>
                    <div class="cell">Великий Новгород, Воскресенский бульвар, д. 1 </div>
                    <div class="cell">18 марта 2011 года</div>
                </div>
                <div class="table-row">
                    <div class="cell">Мемориальная доска Абрамовой И.А.</div>
                    <div class="cell">Великий Новгород, Воскресенский бульвар, д. 1 </div>
                    <div class="cell">18 марта 2011 года</div>
                </div>
                <div class="table-row">
                    <div class="cell">Мемориальная доска Абрамовой И.А.</div>
                    <div class="cell">Великий Новгород, Воскресенский бульвар, д. 1 </div>
                    <div class="cell">18 марта 2011 года</div>
                </div>
                <div class="table-row">
                    <div class="cell">Мемориальная доска Абрамовой И.А.</div>
                    <div class="cell">Великий Новгород, Воскресенский бульвар, д. 1 </div>
                    <div class="cell">18 марта 2011 года</div>
                </div>
                <div class="table-row">
                    <div class="cell">Мемориальная доска Абрамовой И.А.</div>
                    <div class="cell">Великий Новгород, Воскресенский бульвар, д. 1 </div>
                    <div class="cell">18 марта 2011 года</div>
                </div>
                <div class="table-row">
                    <div class="cell">Мемориальная доска Абрамовой И.А.</div>
                    <div class="cell">Великий Новгород, Воскресенский бульвар, д. 1 </div>
                    <div class="cell">18 марта 2011 года</div>
                </div>
                <div class="table-row">
                    <div class="cell">Мемориальная доска Абрамовой И.А.</div>
                    <div class="cell">Великий Новгород, Воскресенский бульвар, д. 1 </div>
                    <div class="cell">18 марта 2011 года</div>
                </div>
                <div class="table-row">
                    <div class="cell">Мемориальная доска Абрамовой И.А.</div>
                    <div class="cell">Великий Новгород, Воскресенский бульвар, д. 1 </div>
                    <div class="cell">18 марта 2011 года</div>
                </div>
                <div class="table-row">
                    <div class="cell">Мемориальная доска Абрамовой И.А.</div>
                    <div class="cell">Великий Новгород, Воскресенский бульвар, д. 1 </div>
                    <div class="cell">18 марта 2011 года</div>
                </div>
                <div class="pagination">
                    <a href="#" class="arrow prev">
                        <?= SVG__ARROW_RIGHT ?>
                    </a>
                    <a href="#" class="page">1</a>
                    <a href="#" class="page current">2</a>
                    <a href="#" class="page">3</a>
                    <a href="#" class="page">4</a>
                    <a href="#" class="page">5</a>
                    <div class="dots">...</div>
                    <a href="#" class="page">9</a>
                    <a href="#" class="arrow next">
                        <?= SVG__ARROW_RIGHT ?>
                    </a>
                </div>
            </div>
            <div class="tab-content">
                <div class="table-head">
                    <div class="cell">Наименование</div>
                    <div class="cell">Расположение</div>
                    <div class="cell">Дата открытия</div>
                </div>
                <div class="table-row">
                    <div class="cell">tab2</div>
                    <div class="cell"></div>
                    <div class="cell"></div>
                </div>
                <div class="pagination">
                    <a href="#" class="arrow prev">
                        <?= SVG__ARROW_RIGHT ?>
                    </a>
                    <a href="#" class="page">1</a>
                    <a href="#" class="page current">2</a>
                    <a href="#" class="page">3</a>
                    <a href="#" class="page">4</a>
                    <a href="#" class="page">5</a>
                    <div class="dots">...</div>
                    <a href="#" class="page">9</a>
                    <a href="#" class="arrow next">
                        <?= SVG__ARROW_RIGHT ?>
                    </a>
                </div>
            </div>
            <div class="tab-content">
                <div class="table-head">
                    <div class="cell">Наименование</div>
                    <div class="cell">Расположение</div>
                    <div class="cell">Дата открытия</div>
                </div>
                <div class="table-row">
                    <div class="cell">tab3</div>
                    <div class="cell"></div>
                    <div class="cell"></div>
                </div>
                <div class="pagination">
                    <a href="#" class="arrow prev">
                        <?= SVG__ARROW_RIGHT ?>
                    </a>
                    <a href="#" class="page">1</a>
                    <a href="#" class="page current">2</a>
                    <a href="#" class="page">3</a>
                    <a href="#" class="page">4</a>
                    <a href="#" class="page">5</a>
                    <div class="dots">...</div>
                    <a href="#" class="page">9</a>
                    <a href="#" class="arrow next">
                        <?= SVG__ARROW_RIGHT ?>
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>

<?php require 'footer.php' ?>