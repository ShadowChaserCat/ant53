<?php $pageTitle = 'Географические рубрики - по странам';

require 'header.php' ?>

<div class="category-page">
    <div class="content-wrap">
        <div class="page-title">
            <p>Географические рубрики</p>
            <p>по странам</p>
            <a href="#" class="round-btn back-btn">
                <?= SVG__ARROW_RIGHT ?>
            </a>
            <div class="round-btn age-limit">12+</div>
        </div>
    </div>
    <div class="img-block"></div>
    <div class="category-block">
        <div class="content-wrap">
            <div class="subcategories-wrap">
                <div class="title">Выберите рубрику:</div>
                <div class="list">
                    <a href="#" class="item">Абхазия</a>
                    <a href="#" class="item">Армения</a>
                    <a href="#" class="item">Беларусь</a>
                    <a href="#" class="item">Болгария</a>
                    <a href="#" class="item">Венгрия</a>
                    <a href="#" class="item">Германия</a>
                    <a href="#" class="item">Грузия</a>
                    <a href="#" class="item">Италия</a>
                    <a href="#" class="item">Казахстан</a>
                    <a href="#" class="item">Киргизия</a>
                    <a href="#" class="item">Латвия</a>
                    <a href="#" class="item">Литва</a>
                    <a href="#" class="item">Молдавия</a>
                    <a href="#" class="item">Польша</a>
                    <a href="#" class="item">Сингапур</a>
                    <a href="#" class="item">Судан</a>
                    <a href="#" class="item">США</a>
                    <a href="#" class="item">Таджикистан</a>
                    <a href="#" class="item">Тайланд</a>
                    <a href="#" class="item">Туркмения</a>
                    <a href="#" class="item">Узбекистан</a>
                    <a href="#" class="item">Украина</a>
                    <a href="#" class="item">Финляндия</a>
                    <a href="#" class="item">Франция</a>
                    <a href="#" class="item">Чехия</a>
                    <a href="#" class="item">Швеция</a>
                    <a href="#" class="item">Эстония</a>
                    <a href="#" class="item">Япония</a>
                </div>
            </div>
            <div class="block-title">
                <p>Последние</p>
                <p>обновления</p>
            </div>
            <div class="swiper">
                <div class="swiper-wrapper">
                    <div class="swiper-slide">
                        <img src="img/mainpage/block-5-img-1.png" alt>
                        <div class="title">Заголовок</div>
                        <div class="desc">Краткое описание текст примерно на одно-два предложения</div>
                    </div>
                    <div class="swiper-slide">
                        <img src="img/mainpage/block-5-img-2.png" alt>
                        <div class="title">Заголовок</div>
                        <div class="desc">Краткое описание текст примерно на одно-два предложения</div>
                    </div>
                    <div class="swiper-slide">
                        <img src="img/mainpage/block-5-img-3.png" alt>
                        <div class="title">Заголовок</div>
                        <div class="desc">Краткое описание текст примерно на одно-два предложения</div>
                    </div>
                    <div class="swiper-slide">
                        <img src="img/mainpage/block-5-img-1.png" alt>
                        <div class="title">Заголовок</div>
                        <div class="desc">Краткое описание текст примерно на одно-два предложения</div>
                    </div>
                    <div class="swiper-slide">
                        <img src="img/mainpage/block-5-img-2.png" alt>
                        <div class="title">Заголовок</div>
                        <div class="desc">Краткое описание текст примерно на одно-два предложения</div>
                    </div>
                    <div class="swiper-slide">
                        <img src="img/mainpage/block-5-img-3.png" alt>
                        <div class="title">Заголовок</div>
                        <div class="desc">Краткое описание текст примерно на одно-два предложения</div>
                    </div>
                </div>
                <div class="swiper-scrollbar"></div>
            </div>
            <div class="btns-wrap">
                <a href="#" class="round-btn prev">
                    <?= SVG__ARROW_RIGHT ?>
                </a>
                <a href="#" class="round-btn next">
                    <?= SVG__ARROW_RIGHT ?>
                </a>
            </div>
        </div>
    </div>
</div>

<?php require 'footer.php' ?>