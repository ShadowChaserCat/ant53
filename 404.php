<?php $pageTitle = 'Страница 404';

require 'header.php' ?>

<div class="page-404">
    <div class="content-wrap">
        <div class="code">404</div>
        <div class="text">
            К сожалению, запрашиваемая Вами<br>
            страница не найдена...
        </div>
        <div class="btns-wrap">
            <a href="/" class="btn alt">На главную</a>
            <a href="#" class="btn">Смотреть все материалы</a>
        </div>
        <div class="updates-block">
            <div class="block-title">Последние обновления:</div>
            <div class="items-grid">
                <div class="item">
                    <a href="#" class="img-wrap">
                        <img src="img/placeholder.png" alt>
                    </a>
                    <a href="#" class="title">Новгород</a>
                    <div class="desc">Обзор книги: Кушнир И.И. Новгород. – Л.: Стройиздат, 1967.</div>
                </div>
                <div class="item">
                    <a href="#" class="img-wrap">
                        <img src="img/placeholder.png" alt>
                    </a>
                    <a href="#" class="title">Новгород</a>
                    <div class="desc">Обзор книги: Кушнир И.И. Новгород. – Л.: Стройиздат, 1967.</div>
                </div>
                <div class="item">
                    <a href="#" class="img-wrap">
                        <img src="img/placeholder.png" alt>
                    </a>
                    <a href="#" class="title">Новгород</a>
                    <div class="desc">Обзор книги: Кушнир И.И. Новгород. – Л.: Стройиздат, 1967.</div>
                </div>
                <div class="item">
                    <a href="#" class="img-wrap">
                        <img src="img/placeholder.png" alt>
                    </a>
                    <a href="#" class="title">Новгород</a>
                    <div class="desc">Обзор книги: Кушнир И.И. Новгород. – Л.: Стройиздат, 1967.</div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php require 'footer.php' ?>