<?php $pageTitle = 'Модерация';

require 'header.php' ?>

<div class="moderation-page">
    <div class="content-wrap">
        <div class="page-title">
            Редактирование всех материалов
            <div class="count">3000</div>
        </div>
        <form method="post" class="search-wrap">
            <div class="btn-wrap">
                <input type="text" name="search" placeholder="Введите свой запрос">
                <button type="submit" class="btn alt">Найти</button>
            </div>
            <label class="check-wrap">
                <input type="checkbox" name="personal" required>
                <span class="check">
                    <img src="img/svg/check.svg" alt>
                </span>
                <span class="text">
                    Материалы на модерации
                </span>
            </label>
        </form>
        <div class="results-title">Результаты поиска по запросу ...:</div> <!-- Выводится только при поиске -->
        <div class="items-grid">
            <div class="item">
                <a href="#" class="img-wrap">
                    <img src="img/placeholder.png" alt>
                </a>
                <a href="#" class="title">Новгород</a>
                <div class="desc">Обзор книги: Кушнир И.И. Новгород. – Л.: Стройиздат, 1967.</div>
                <div class="btns-wrap">
                    <a href="#" class="btn alt">Снять</a>
                    <a href="#" class="btn">Вернуть</a>
                </div>
            </div>
            <div class="item">
                <a href="#" class="img-wrap">
                    <img src="img/placeholder.png" alt>
                </a>
                <a href="#" class="title">Новгород</a>
                <div class="desc">Обзор книги: Кушнир И.И. Новгород. – Л.: Стройиздат, 1967.</div>
                <div class="btns-wrap">
                    <a href="#" class="btn alt">Снять</a>
                    <a href="#" class="btn">Вернуть</a>
                </div>
            </div>
            <div class="item">
                <a href="#" class="img-wrap">
                    <img src="img/placeholder.png" alt>
                </a>
                <a href="#" class="title">Новгород</a>
                <div class="desc">Обзор книги: Кушнир И.И. Новгород. – Л.: Стройиздат, 1967.</div>
                <div class="btns-wrap">
                    <a href="#" class="btn alt">Снять</a>
                    <a href="#" class="btn">Вернуть</a>
                </div>
            </div>
            <div class="item">
                <a href="#" class="img-wrap">
                    <img src="img/placeholder.png" alt>
                </a>
                <a href="#" class="title">Новгород</a>
                <div class="desc">Обзор книги: Кушнир И.И. Новгород. – Л.: Стройиздат, 1967.</div>
                <div class="btns-wrap">
                    <a href="#" class="btn alt">Снять</a>
                    <a href="#" class="btn">Вернуть</a>
                </div>
            </div>
            <div class="item">
                <a href="#" class="img-wrap">
                    <img src="img/placeholder.png" alt>
                </a>
                <a href="#" class="title">Новгород</a>
                <div class="desc">Обзор книги: Кушнир И.И. Новгород. – Л.: Стройиздат, 1967.</div>
                <div class="btns-wrap">
                    <a href="#" class="btn alt">Снять</a>
                    <a href="#" class="btn">Вернуть</a>
                </div>
            </div>
            <div class="item">
                <a href="#" class="img-wrap">
                    <img src="img/placeholder.png" alt>
                </a>
                <a href="#" class="title">Новгород</a>
                <div class="desc">Обзор книги: Кушнир И.И. Новгород. – Л.: Стройиздат, 1967.</div>
                <div class="btns-wrap">
                    <a href="#" class="btn alt">Снять</a>
                    <a href="#" class="btn">Вернуть</a>
                </div>
            </div>
            <div class="item">
                <a href="#" class="img-wrap">
                    <img src="img/placeholder.png" alt>
                </a>
                <a href="#" class="title">Новгород</a>
                <div class="desc">Обзор книги: Кушнир И.И. Новгород. – Л.: Стройиздат, 1967.</div>
                <div class="btns-wrap">
                    <a href="#" class="btn alt">Снять</a>
                    <a href="#" class="btn">Вернуть</a>
                </div>
            </div>
            <div class="item">
                <a href="#" class="img-wrap">
                    <img src="img/placeholder.png" alt>
                </a>
                <a href="#" class="title">Новгород</a>
                <div class="desc">Обзор книги: Кушнир И.И. Новгород. – Л.: Стройиздат, 1967.</div>
                <div class="btns-wrap">
                    <a href="#" class="btn alt">Снять</a>
                    <a href="#" class="btn">Вернуть</a>
                </div>
            </div>
            <div class="item">
                <a href="#" class="img-wrap">
                    <img src="img/placeholder.png" alt>
                </a>
                <a href="#" class="title">Новгород</a>
                <div class="desc">Обзор книги: Кушнир И.И. Новгород. – Л.: Стройиздат, 1967.</div>
                <div class="btns-wrap">
                    <a href="#" class="btn alt">Снять</a>
                    <a href="#" class="btn">Вернуть</a>
                </div>
            </div>
            <div class="item">
                <a href="#" class="img-wrap">
                    <img src="img/placeholder.png" alt>
                </a>
                <a href="#" class="title">Новгород</a>
                <div class="desc">Обзор книги: Кушнир И.И. Новгород. – Л.: Стройиздат, 1967.</div>
                <div class="btns-wrap">
                    <a href="#" class="btn alt">Снять</a>
                    <a href="#" class="btn">Вернуть</a>
                </div>
            </div>
            <div class="item">
                <a href="#" class="img-wrap">
                    <img src="img/placeholder.png" alt>
                </a>
                <a href="#" class="title">Новгород</a>
                <div class="desc">Обзор книги: Кушнир И.И. Новгород. – Л.: Стройиздат, 1967.</div>
                <div class="btns-wrap">
                    <a href="#" class="btn alt">Снять</a>
                    <a href="#" class="btn">Вернуть</a>
                </div>
            </div>
            <div class="item">
                <a href="#" class="img-wrap">
                    <img src="img/placeholder.png" alt>
                </a>
                <a href="#" class="title">Новгород</a>
                <div class="desc">Обзор книги: Кушнир И.И. Новгород. – Л.: Стройиздат, 1967.</div>
                <div class="btns-wrap">
                    <a href="#" class="btn alt">Снять</a>
                    <a href="#" class="btn">Вернуть</a>
                </div>
            </div>
            <div class="item">
                <a href="#" class="img-wrap">
                    <img src="img/placeholder.png" alt>
                </a>
                <a href="#" class="title">Новгород</a>
                <div class="desc">Обзор книги: Кушнир И.И. Новгород. – Л.: Стройиздат, 1967.</div>
                <div class="btns-wrap">
                    <a href="#" class="btn alt">Снять</a>
                    <a href="#" class="btn">Вернуть</a>
                </div>
            </div>
            <div class="item">
                <a href="#" class="img-wrap">
                    <img src="img/placeholder.png" alt>
                </a>
                <a href="#" class="title">Новгород</a>
                <div class="desc">Обзор книги: Кушнир И.И. Новгород. – Л.: Стройиздат, 1967.</div>
                <div class="btns-wrap">
                    <a href="#" class="btn alt">Снять</a>
                    <a href="#" class="btn">Вернуть</a>
                </div>
            </div>
            <div class="item">
                <a href="#" class="img-wrap">
                    <img src="img/placeholder.png" alt>
                </a>
                <a href="#" class="title">Новгород</a>
                <div class="desc">Обзор книги: Кушнир И.И. Новгород. – Л.: Стройиздат, 1967.</div>
                <div class="btns-wrap">
                    <a href="#" class="btn alt">Снять</a>
                    <a href="#" class="btn">Вернуть</a>
                </div>
            </div>
            <div class="item">
                <a href="#" class="img-wrap">
                    <img src="img/placeholder.png" alt>
                </a>
                <a href="#" class="title">Новгород</a>
                <div class="desc">Обзор книги: Кушнир И.И. Новгород. – Л.: Стройиздат, 1967.</div>
                <div class="btns-wrap">
                    <a href="#" class="btn alt">Снять</a>
                    <a href="#" class="btn">Вернуть</a>
                </div>
            </div>
        </div>
        <div class="pagination">
            <a href="#" class="arrow prev">
                <?= SVG__ARROW_RIGHT ?>
            </a>
            <a href="#" class="page">1</a>
            <a href="#" class="page current">2</a>
            <a href="#" class="page">3</a>
            <a href="#" class="page">4</a>
            <a href="#" class="page">5</a>
            <div class="dots">...</div>
            <a href="#" class="page">9</a>
            <a href="#" class="arrow next">
                <?= SVG__ARROW_RIGHT ?>
            </a>
        </div>
    </div>
</div>

<?php require 'footer.php' ?>