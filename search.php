<?php $pageTitle = 'Поиск';

require 'header.php' ?>

<div class="search-page">
    <div class="content-wrap">
        <div class="page-title">Свой поиск</div>
        <form method="post" class="search-wrap">
            <input type="text" name="search" placeholder="Введите свой запрос">
            <button type="submit" class="btn alt">Найти</button>
        </form>
        <section class="filters">
            <article>
                <a href="#" class="filters__link">Отбор по темам</a>
            </article>
            <article>
                <a href="#" class="filters__link">Отбор по районам Новгородской области</a>
            </article>
            <article>
                <a href="#" class="filters__link">Отбор по регионам Российской Федерации</a>
            </article>
            <article>
                <a href="#" class="filters__link">Отбор по странам мира</a>
            </article>
        </section>
        <h2 class="results-title">Последние обновления</h2>
    </div>
</div>

<?php require 'footer.php' ?>