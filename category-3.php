<?php $pageTitle = 'Географические рубрики - по регионам';

require 'header.php' ?>

<div class="category-page">
    <div class="content-wrap">
        <div class="page-title">
            <p>Географические рубрики</p>
            <p>по регионам</p>
            <a href="#" class="round-btn back-btn">
                <?= SVG__ARROW_RIGHT ?>
            </a>
            <div class="round-btn age-limit">12+</div>
        </div>
    </div>
    <div class="img-block"></div>
    <div class="category-block">
        <div class="content-wrap">
            <div class="subcategories-wrap">
                <div class="title">Выберите рубрику:</div>
                <div class="list">
                    <a href="#" class="item">Алтайский край</a>
                    <a href="#" class="item">Архангельская область</a>
                    <a href="#" class="item">Белгородская область</a>
                    <a href="#" class="item">Волгоградская область</a>
                    <a href="#" class="item">Вологодская область</a>
                    <a href="#" class="item">Воронежская область</a>
                    <a href="#" class="item">Забайкальский край</a>
                    <a href="#" class="item">Калининградская область</a>
                    <a href="#" class="item">Калужская область</a>
                    <a href="#" class="item">Карелия</a>
                    <a href="#" class="item">Кировская область</a>
                    <a href="#" class="item">Краснодарский край</a>
                    <a href="#" class="item">Красноярский край</a>
                    <a href="#" class="item">Курская область</a>
                    <a href="#" class="item">Ленинградская область</a>
                    <a href="#" class="item">Липецкая область</a>
                    <a href="#" class="item">Магаданская область</a>
                    <a href="#" class="item">Москва</a>
                    <a href="#" class="item">Московская область</a>
                    <a href="#" class="item">Мурманская область</a>
                    <a href="#" class="item">Нижегородская область</a>
                    <a href="#" class="item">Омская область</a>
                    <a href="#" class="item">Оренбургская область</a>
                    <a href="#" class="item">Орловская область</a>
                    <a href="#" class="item">Псковская область</a>
                    <a href="#" class="item">Республика Дагестан</a>
                    <a href="#" class="item">Республика Коми</a>
                    <a href="#" class="item">Республика Крым</a>
                    <a href="#" class="item">Республика Татарстан</a>
                    <a href="#" class="item">Ростовская область</a>
                    <a href="#" class="item">Рязанская область</a>
                    <a href="#" class="item">Санкт-Петербург</a>
                    <a href="#" class="item">Саратовская область</a>
                    <a href="#" class="item">Сахалинская область</a>
                    <a href="#" class="item">Свердловская область</a>
                    <a href="#" class="item">Северная Осетия</a>
                    <a href="#" class="item">Смоленская область</a>
                    <a href="#" class="item">Тамбовская область</a>
                    <a href="#" class="item">Тверская область</a>
                    <a href="#" class="item">Томская область</a>
                    <a href="#" class="item">Тюменская область</a>
                    <a href="#" class="item">Удмуртская Республика</a>
                    <a href="#" class="item">Хабаровский край</a>
                    <a href="#" class="item">Челябинская область</a>
                    <a href="#" class="item">Чувашская Республика</a>
                </div>
            </div>
            <div class="block-title">
                <p>Последние</p>
                <p>обновления</p>
            </div>
            <div class="swiper">
                <div class="swiper-wrapper">
                    <div class="swiper-slide">
                        <img src="img/mainpage/block-5-img-1.png" alt>
                        <div class="title">Заголовок</div>
                        <div class="desc">Краткое описание текст примерно на одно-два предложения</div>
                    </div>
                    <div class="swiper-slide">
                        <img src="img/mainpage/block-5-img-2.png" alt>
                        <div class="title">Заголовок</div>
                        <div class="desc">Краткое описание текст примерно на одно-два предложения</div>
                    </div>
                    <div class="swiper-slide">
                        <img src="img/mainpage/block-5-img-3.png" alt>
                        <div class="title">Заголовок</div>
                        <div class="desc">Краткое описание текст примерно на одно-два предложения</div>
                    </div>
                    <div class="swiper-slide">
                        <img src="img/mainpage/block-5-img-1.png" alt>
                        <div class="title">Заголовок</div>
                        <div class="desc">Краткое описание текст примерно на одно-два предложения</div>
                    </div>
                    <div class="swiper-slide">
                        <img src="img/mainpage/block-5-img-2.png" alt>
                        <div class="title">Заголовок</div>
                        <div class="desc">Краткое описание текст примерно на одно-два предложения</div>
                    </div>
                    <div class="swiper-slide">
                        <img src="img/mainpage/block-5-img-3.png" alt>
                        <div class="title">Заголовок</div>
                        <div class="desc">Краткое описание текст примерно на одно-два предложения</div>
                    </div>
                </div>
                <div class="swiper-scrollbar"></div>
            </div>
            <div class="btns-wrap">
                <a href="#" class="round-btn prev">
                    <?= SVG__ARROW_RIGHT ?>
                </a>
                <a href="#" class="round-btn next">
                    <?= SVG__ARROW_RIGHT ?>
                </a>
            </div>
        </div>
    </div>
</div>

<?php require 'footer.php' ?>